namespace tmapi4net.Providers.ActiveRecord.Index {
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;
	using tmapi4net.Index.Core;

	class ActiveRecordVariantsIndex : ActiveRecordIndex , IVariantsIndex {
		public ActiveRecordVariantsIndex(ITopicMap topicMap)
			: base(topicMap) {
			
		}
		#region IVariantsIndex Members

		ISet<IVariant> IVariantsIndex.GetVariantsByResource(ILocator loc) {
			
			throw new System.Exception("The method or operation is not implemented.");
		}

		ISet<IVariant> IVariantsIndex.GetVariantsByValue(string value) {
			throw new System.Exception("The method or operation is not implemented.");
		}

		#endregion

	}
}