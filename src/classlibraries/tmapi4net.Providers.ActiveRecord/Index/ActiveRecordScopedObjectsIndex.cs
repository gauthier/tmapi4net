namespace tmapi4net.Providers.ActiveRecord.Index {
	using System.Collections.Generic;
	using Castle.ActiveRecord;
	using Castle.ActiveRecord.Queries;
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;
	using tmapi4net.Index.Core;
	using tmapi4net.Providers.ActiveRecord.Implementations;
	using tmapi4net.Providers.ActiveRecord.Repositories;
	using ScopedObject=tmapi4net.Providers.ActiveRecord.Model.ScopedObject;
	using Topic=tmapi4net.Providers.ActiveRecord.Model.Topic;

	class ActiveRecordScopedObjectsIndex : ActiveRecordIndex, IScopedObjectsIndex {
		public  ActiveRecordScopedObjectsIndex(ITopicMap topicMap) : base(topicMap) {
			
		}


		#region IScopedObjectsIndex Members

		IReadOnlySet<ITopic> IScopedObjectsIndex.GetScopingTopics() {
			using (IDisposableLoggerScope scope = GetDisposableLoggerScope("GetScopingTopics")) {
				string hqlquery =
					@"
select distinct t from Topic t, Association scoped where t in elements (scoped.Scope) and scoped.TopicMap.Key = :topicMapKey
			";
				/*union
				select t from Topic t Occurrence where scoped.TopicMap.Key = :topicMapKey
				union
				select distinct scoped.Scope.Topic from TopicName where scoped.TopicMap.Key = :topicMapKey
				union
				select distinct scoped.Scope.Topic from Variant where scoped.TopicMap.Key = :topicMapKey
				*/
				SimpleQuery<Topic> scopingTopicsQuery = new SimpleQuery<Topic>(hqlquery);
				scopingTopicsQuery.SetParameter("topicMapKey", ((Implementations.TopicMap) topicMap_).topicMap_.Key);

				IList<Topic> scopingtopics =
					new List<Topic>((IEnumerable<Topic>) ActiveRecordMediator<Topic>.ExecuteQuery(scopingTopicsQuery));
				ReadOnlySet<ITopic> result = new ReadOnlySet<ITopic>();
				foreach (Topic unnamedtopic in scopingtopics) {
					result.Add(new Implementations.Topic(topicMap_, unnamedtopic));
				}
				return result;
			}
		}

		IReadOnlySet<IScopedObject> IScopedObjectsIndex.GetScopedObjectsByScopingTopic(ITopic scopingTopic) {
			using (IDisposableLoggerScope scope = GetDisposableLoggerScope("GetScopedObjectsByScopingTopic")) {
				string query;
				SimpleQuery<ScopedObject> q;
				if (scopingTopic == null) {
					query =
						string.Format("from {0} scopedobject where scopedobject.Scope.Count = 0 and scopedobject.TopicMap = :topicMap",
						              typeof (ScopedObject).Name);
				}
				else
					query =
						string.Format(
							"from {0} scopedobject where :scopingTopic in (scopedobject.Scope) and scopedobject.TopicMap = :topicMap",
							typeof (ScopedObject).Name);
				q = new SimpleQuery<ScopedObject>(query);
				if (scopingTopic != null)
					q.SetParameter("scopingTopic", scopingTopic);
				q.SetParameter("topicMap", scopingTopic.TopicMap);
				return new Implementations.ScopedObjectSet(topicMap_, q.Execute());
			}
		}

		ISet<ITopic> IScopedObjectsIndex.GetScopedObjectsByScopingTopics(ITopic[] scopingTopics, bool matchAll) {
			using (IDisposableLoggerScope scope = GetDisposableLoggerScope("GetScopedObjectsByScopingTopics")) {
				throw new System.Exception("The method or operation is not implemented.");
			}
		}

		#endregion


	}
}