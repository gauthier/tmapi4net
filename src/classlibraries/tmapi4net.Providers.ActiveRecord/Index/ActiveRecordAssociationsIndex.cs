namespace tmapi4net.Providers.ActiveRecord.Index {
	using Castle.ActiveRecord.Queries;
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;
	using tmapi4net.Index.Core;
	using tmapi4net.Providers.ActiveRecord.Model;
	using tmapi4net.Providers.ActiveRecord.Repositories;

	class ActiveRecordAssociationsIndex : ActiveRecordIndex, IAssociationsIndex {


		public ActiveRecordAssociationsIndex(ITopicMap topicMap)
			: base(topicMap) {
	

		}

		#region IAssociationsIndex Members

		ISet<ITopic> IAssociationsIndex.GetAssociationTypes() {
			using (IDisposableLoggerScope scope = GetDisposableLoggerScope("GetAssociationTypes")) {
				return getTypedObjectTypes<Association>();
			}
		}

		ISet<IAssociation> IAssociationsIndex.GetAssociationsByType(ITopic type) {
			using (IDisposableLoggerScope scope = GetDisposableLoggerScope("GetAssociationsByType")) {
				return new Implementations.AssociationSet(topicMap_, getTypedObjectsByType<Model.Association>(type));
			}
		}

		#endregion

	}
}