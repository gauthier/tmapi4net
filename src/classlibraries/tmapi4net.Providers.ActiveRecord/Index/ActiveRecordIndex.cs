
namespace tmapi4net.Providers.ActiveRecord.Index {
	using System;
	using System.Collections.Generic;
	using System.Collections.Specialized;
	using Castle.ActiveRecord.Queries;
	using log4net;
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;
	using tmapi4net.Index;
	using tmapi4net.Providers.ActiveRecord.Repositories;

	internal class ActiveRecordIndex : IIndex {
		
		
		public ActiveRecordIndex(ITopicMap topicMap) {
			this.topicMap_ = topicMap;
		}

		protected ILog logger { get { return LogManager.GetLogger(this.GetType()); } }

			protected ITopicMap topicMap_;

		protected StringCollection supportedMethodNames;
		IDictionary<string, IList<IList<Type>>> supportedMethodParameters;
		private bool isOpen_;

		protected IDisposableLoggerScope GetDisposableLoggerScope(string methodName) {
			return new DisposableLoggerXmlScope(this.GetType().Name, methodName, logger);
		}
		#region IIndex Members

		void IIndex.Open() {
			isOpen_ = true;
		}

		void IIndex.Close() {
			isOpen_ = false;
		}

		bool IIndex.IsOpen {
			get { return isOpen_; }
		}

		void IIndex.Reindex() {
			
		}

		IIndexFlags IIndex.Flags {
			get { throw new System.Exception("The method or operation is not implemented."); }
		}

		bool IIndex.SupportMethod(string methodName) {
			return supportedMethodNames.Contains(methodName);
		}

		bool IIndex.SupportMethod(string methodName, params Type[] parameterTypes) {
			if (supportedMethodNames.Contains(methodName)) {
				IList<IList<Type>> parametertype = supportedMethodParameters[methodName];
				foreach (IList<Type> types in parametertype) {
					
					if (types.Count != parameterTypes.Length) 
						continue;
					int paramIndex = 0;
					int paramlength = parameterTypes.Length;
					bool areequals =true;
					for (paramIndex = 0; paramIndex < paramlength; ++paramIndex) {
						areequals = areequals && parameterTypes[paramIndex] == types[paramIndex];
						if (!areequals)
							break;

					}
					if(!areequals) 
						continue;
					else
						return true;
				}
				return false;
			}
			else 
				return false;
		}

		#endregion
		
		
		
		
		
		protected ISet<ITopic> getTypedObjectTypes<TTypedObject>() {
			string query =
			string.Format("select distinct typed.Type from {0} typed where typed.TopicMap = :topicMap", typeof(TTypedObject).Name);
			SimpleQuery<Model.Topic> q = new SimpleQuery<Model.Topic>(query);
			q.SetParameter("topicMap", ((Implementations.TopicMap)topicMap_).topicMap_);
			Model.Topic[] types = q.Execute();
			return new Implementations.TopicSet(topicMap_, types);
		}

		protected IEnumerable<TTypedObject> getTypedObjectsByType<TTypedObject>(ITopic type) {
			string query;

			if (typeof(TTypedObject) == typeof(Model.Topic)) {
					query =
						(type != null)	
						? @"from Topic t 
where	t.TopicMap = :topicMap 
and		t.Key in (
			select	rel.Key.TopicKey	
			from	TopicTypesRelation rel 
			where	rel.Type = :typeTopic
		)"				
						: @"from Topic t 
where	t.TopicMap = :topicMap 
and		t.Key not in (
			select	rel.Key.TopicKey
			from	TopicTypesRelation rel";
			}
			else
			{
				
					query = 
						String.Format(
						(type != null)	? "from {0} typed where typed.Type = :typeTopic and typed.TopicMap = :topicMap"
										: "from {0} typed where typed.Type is null and typed.TopicMap = :topicMap",
						              typeof (TTypedObject).Name);
			}


			SimpleQuery<TTypedObject> q = new SimpleQuery<TTypedObject>(query);
			if (type != null)
				q.SetParameter("typeTopic", ((Implementations.Topic)type).topic_);
			q.SetParameter("topicMap", ((Implementations.TopicMap)topicMap_).topicMap_);
			return q.Execute();
		}

	}
}
