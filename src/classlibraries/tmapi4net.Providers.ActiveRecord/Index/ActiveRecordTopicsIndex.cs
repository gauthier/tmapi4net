namespace tmapi4net.Providers.ActiveRecord.Index {
	using System.Collections.Generic;
	using Castle.ActiveRecord;
	using Castle.ActiveRecord.Queries;
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;
	using tmapi4net.Index.Core;
	using tmapi4net.Providers.ActiveRecord.Implementations;
	using tmapi4net.Providers.ActiveRecord.Repositories;
	using Topic=tmapi4net.Providers.ActiveRecord.Model.Topic;

	class ActiveRecordTopicsIndex : ActiveRecordIndex, ITopicsIndex {
		
public ActiveRecordTopicsIndex(ITopicMap topicMap)
			: base(topicMap) {
			
		}

	

		#region ITopicsIndex Members

		IReadOnlySet<ITopic> ITopicsIndex.GetTopicTypes() {
			using (IDisposableLoggerScope scope = GetDisposableLoggerScope("GetTopicTypes")) {
				string query =
					"select t from Topic t where t.Key in (select distinct rel.Key.TypeKey from TopicTypesRelation rel) and t.TopicMap.Key = :topicMapKey";


				/*string query = "select distinct t from Topic t, Topic types where types.TopicMap = t.TopicMap and t.TopicMap = :topicMap and t in elements(types.Types)";*/
				SimpleQuery<Topic> q = new SimpleQuery<Topic>(query);
				q.SetParameter("topicMapKey", ((Implementations.TopicMap) topicMap_).topicMap_.Key);
				return new Implementations.TopicSet(topicMap_, q.Execute());
			}
		}

		IReadOnlySet<ITopic> ITopicsIndex.GetTopicsByType(ITopic type) {
			using (IDisposableLoggerScope scope = GetDisposableLoggerScope("GetTopicsByType")) {
				string query;
				if (type != null)
					query =
						@"
from Topic t 
where t.TopicMap = :topicMap 
and t.Key in (
	select rel.Key.TopicKey
	from TopicTypesRelation rel
	where rel.Key.TypeKey = :typeKey
)";
				else
					query =
						@"
from Topic t 
where t.TopicMap = :topicMap 
and t.Key not in (
	select rel.Key.TopicKey
	from TopicTypesRelation rel
)";
				SimpleQuery q = new SimpleQuery(typeof (Topic), query);

				q.SetParameter("topicMap", ((Implementations.TopicMap) topicMap_).topicMap_);
				if (type != null)
					q.SetParameter("typeKey", ((Implementations.Topic) type).topic_.Key);

				object o = Castle.ActiveRecord.ActiveRecordMediator.ExecuteQuery(q);

				return new Implementations.TopicSet(topicMap_, o as Topic[]);
			}
		}

		IReadOnlySet<ITopic> ITopicsIndex.GetTopicsByTypes(ITopic[] types, bool matchAll) {
			using (IDisposableLoggerScope scope = GetDisposableLoggerScope("GetTopicsByTypes")) {
				if (types == null)
					return ((ITopicsIndex) this).GetTopicsByType(null);
				else {
					string query;
					query =
						@"
from Topic t 
where t.TopicMap = :topicMap 
and t.Key in (
	select rel.Key.TopicKey
	from TopicTypesRelation rel
	where rel.TypeKey in elements(:types)
)";
					SimpleQuery q = new SimpleQuery(typeof (Topic), query);

					q.SetParameter("topicMap", ((Implementations.TopicMap) topicMap_).topicMap_);
					List<Topic> typemodels = new List<Topic>();
					foreach (ITopic topic in types) {
						typemodels.Add(((Implementations.Topic) topic).topic_);
					}
					q.SetParameter("types", typemodels);

					object o = Castle.ActiveRecord.ActiveRecordMediator.ExecuteQuery(q);

					return new Implementations.TopicSet(topicMap_, o as Topic[]);
				}
			}
		}

		ITopic ITopicsIndex.GetTopicBySubjectLocator(ILocator subjectLocator) {
			using (IDisposableLoggerScope scope = GetDisposableLoggerScope("GetTopicBySubjectLocator")) {
				string query = "from Topic t where :subjectLocator in elements(t.SubjectLocators) and t.TopicMap = :topicMap";
				SimpleQuery<Topic> q = new SimpleQuery<Topic>(query);
				q.SetParameter("subjectLocator", ((Implementations.Locator) subjectLocator).locator_);
				q.SetParameter("topicMap", ((Implementations.TopicMap) topicMap_).topicMap_);
				Topic[] topics = q.Execute();
				if (topics.Length == 0)
					return null;
				return new Implementations.Topic(topicMap_, topics[0]);
			}
		}

		ITopic ITopicsIndex.GetTopicBySubjectIdentifier(ILocator subjectIdentifier) {
			using (IDisposableLoggerScope scope = GetDisposableLoggerScope("GetTopicBySubjectIdentifier")) {
				string query = "from Topic t where :subjectIdentifier in elements(t.SubjectIdentifiers) and t.TopicMap = :topicMap";
				SimpleQuery<Topic> q = new SimpleQuery<Topic>(query);
				q.SetParameter("subjectIdentifier", ((Implementations.Locator) subjectIdentifier).locator_);
				q.SetParameter("topicMap", ((Implementations.TopicMap) topicMap_).topicMap_);
				Topic[] topics = q.Execute();
				if (topics.Length == 0)
					return null;
				return new Implementations.Topic(topicMap_, topics[0]);
			}
		}

		IReadOnlySet<ITopic> ITopicsIndex.GetNonTypingTopics(ITopicMap topicMap) {
			using (IDisposableLoggerScope scope = GetDisposableLoggerScope("GetNonTypingTopics")) {
				#region sql

				string sqlquery =
					@"
select	typed.type_key
from 	associationroles typed
,	topicmapobjects o 
where 	typed.key = o.key
and 	o.topicmap_key = :topicMapKey
union
select	typed.type_key 
from 	associations typed
,	topicmapobjects o
where 	typed.key = o.key
and 	o.topicmap_key = :topicMapKey
union
select	typed.type_key 
from 	occurrences typed
,	topicmapobjects o
where 	typed.key = o.key
and 	o.topicmap_key = :topicMapKey
union
select	typed.type_key
from 	topicnames typed
,	topicmapobjects o
where 	typed.key = o.key
and 	o.topicmap_key = :topicMapKey
union
select	typed.type_key
from 	topic_types typed
,	topicmapobjects o
where 	typed.topic_key = o.key
and 	o.topicmap_key = :topicMapKey
";

				#endregion

				string hqlquery =
					@"
select	typed1.Type
from 	AssociationRole typed1
where 	typed1.TopicMap.Key = :topicMapKey

";
					/*
union

select	typed2.Type
from 	Association typed2
where 	typed2.TopicMap.Key = :topicMapKey

union

select	typed3.Type 
from 	Occurrence.Type typed3
where 	typed3.TopicMap.Key = :topicMapKey

union

select	typed4.Type
from 	TopicName typed4
where 	typed4.TopicMap.Key = :topicMapKey

union

select	typed5.Type
from 	TopicTypesRelation typed5
where	typed5.Topic.TopicMap.Key = :topicMapKey
   */

				SimpleQuery<Topic> typingtopicsQuery = new SimpleQuery<Topic>(hqlquery);

				typingtopicsQuery.SetParameter("topicMapKey", ((Implementations.TopicMap) topicMap).topicMap_.Key);


				IList<Topic> typingtopics =
					new List<Topic>((IEnumerable<Topic>) ActiveRecordMediator<Topic>.ExecuteQuery(typingtopicsQuery));

				IReadOnlySet<ITopic> nontypingtopics = new ReadOnlySet<ITopic>();
				foreach (ITopic topic in topicMap.Topics) {
					if (!typingtopics.Contains(((Implementations.Topic) topic).topic_))
						nontypingtopics.Add(topic);
				}
				return nontypingtopics;
			}
		}


		IReadOnlySet<ITopic> ITopicsIndex.GetUnnamedTopics(ITopicMap topicMap) {
			using (IDisposableLoggerScope scope = GetDisposableLoggerScope("GetUnnamedTopics")) {
				string hqlquery =
					@"

from	Topic unnameds
where	unnameds not in (
	select tn.Topic
	from	TopicName tn
	where	tn.TopicMap.Key = :topicMapKey
)
and unnameds.TopicMap.Key = :topicMapKey
";
				SimpleQuery<Topic> unnamedTopicsQuery = new SimpleQuery<Topic>(hqlquery);
				unnamedTopicsQuery.SetParameter("topicMapKey", ((Implementations.TopicMap) topicMap).topicMap_.Key);

				IList<Topic> unnamedtopics =
					new List<Topic>((IEnumerable<Topic>) ActiveRecordMediator<Topic>.ExecuteQuery(unnamedTopicsQuery));
				ReadOnlySet<ITopic> result = new ReadOnlySet<ITopic>();
				foreach (Topic unnamedtopic in unnamedtopics) {
					result.Add(new Implementations.Topic(topicMap, unnamedtopic));
				}
				return result;
			}
		}

		#endregion

	}
}