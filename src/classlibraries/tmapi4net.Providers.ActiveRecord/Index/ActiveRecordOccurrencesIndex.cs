namespace tmapi4net.Providers.ActiveRecord.Index {
	using Castle.ActiveRecord.Queries;
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;
	using tmapi4net.Providers.ActiveRecord;
	using tmapi4net.Index.Core;
	using tmapi4net.Providers.ActiveRecord.Model;
	using tmapi4net.Providers.ActiveRecord.Repositories;


	class ActiveRecordOccurrencesIndex : ActiveRecordIndex, IOccurrencesIndex {

		public ActiveRecordOccurrencesIndex(ITopicMap topicMap)
			: base(topicMap) {
		}

		#region IOccurrencesIndex Members

		ISet<ITopic> IOccurrencesIndex.GetOccurrenceTypes() {
			using (IDisposableLoggerScope scope = GetDisposableLoggerScope("GetOccurrenceTypes")) {
				return getTypedObjectTypes<Occurrence>();
			}
		}

		ISet<IOccurrence> IOccurrencesIndex.GetOccurrencesByType(ITopic type) {
			using (IDisposableLoggerScope scope = GetDisposableLoggerScope("GetOccurrencesByType")) {
				return new Implementations.OccurrenceSet(topicMap_, getTypedObjectsByType<Occurrence>(type));
			}
		}

		ISet<IOccurrence> IOccurrencesIndex.GetOccurrencesByResource(ILocator loc) {
			using (IDisposableLoggerScope scope = GetDisposableLoggerScope("GetOccurrencesByResource")) {
				string query;
				SimpleQuery<Occurrence> q;
				if (loc == null)
					query =
						string.Format("from {0} located where located.Locator is null and located.TopicMap = :topicMap",
						              typeof (Occurrence).Name);
				else
					query =
						string.Format("from {0} located where located.Locator = :locator and located.TopicMap = :topicMap",
						              typeof (Occurrence).Name);
				q = new SimpleQuery<Occurrence>(query);
				if (loc != null)
					q.SetParameter("locator", ((Implementations.Locator) loc).locator_);
				q.SetParameter("topicMap", ((Implementations.TopicMap) topicMap_).topicMap_);
				return new Implementations.OccurrenceSet(topicMap_, q.Execute());
			}
		}

		ISet<IOccurrence> IOccurrencesIndex.GetOccurrencesByValue(string value) {
			using (IDisposableLoggerScope scope = GetDisposableLoggerScope("GetOccurrencesByValue")) {
				string query;
				if (value != null)
					query =
						string.Format("from {0} valued where valued.Value = :value and valued.TopicMap = :topicMap", typeof (Occurrence));
				else
					query = string.Format("from {0} valued where value is null and valued.TopicMap = :topicMap", typeof (Occurrence));
				SimpleQuery<Occurrence> q = new SimpleQuery<Occurrence>(query);
				if (value != null)
					q.SetParameter("value", value);
				q.SetParameter("topicMap", ((Implementations.TopicMap) topicMap_).topicMap_);
				return new Implementations.OccurrenceSet(topicMap_, q.Execute());
			}
		}

		#endregion

	}
}