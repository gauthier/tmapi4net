namespace tmapi4net.Providers.ActiveRecord.Index {
	using System;
	using Castle.ActiveRecord;
	using Castle.ActiveRecord.Queries;
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;
	using tmapi4net.Index.Core;
	using tmapi4net.Providers.ActiveRecord.Model;
	using tmapi4net.Providers.ActiveRecord.Repositories;

	class ActiveRecordTopicNamesIndex : ActiveRecordIndex, ITopicNamesIndex {
		public ActiveRecordTopicNamesIndex(ITopicMap topicMap)
			: base(topicMap) {
			
		}
		#region ITopicNamesIndex Members

		ISet<ITopicName> ITopicNamesIndex.GetTopicNamesByValue(string value) {

			throw new System.Exception("The method or operation is not implemented.");
		}

		ISet<ITopic> ITopicNamesIndex.GetTopicNameTypes() {
			using (IDisposableLoggerScope scope = GetDisposableLoggerScope("GetTopicNameTypes")) {
				return getTypedObjectTypes<Model.TopicName>();
			}
		}

		ISet<ITopicName> ITopicNamesIndex.GetTopicNamesByType(ITopic type) {
			using (IDisposableLoggerScope scope = GetDisposableLoggerScope("GetTopicNamesByType")) {
				return new Implementations.TopicNameSet(topicMap_, getTypedObjectsByType<TopicName>(type));
			}
		}

		#endregion

	}
	class ActiveRecordTopicMapObjectsIndex : ActiveRecordIndex, ITopicMapObjectsIndex {
		public ActiveRecordTopicMapObjectsIndex(ITopicMap topicMap)
			: base(topicMap) {
			
		}

#region ITopicMapObjectsIndex Members

		ITopicMapObject ITopicMapObjectsIndex.GetTopicMapObjectBySourceLocator(ILocator sourceLocator) {
			using (IDisposableLoggerScope scope = GetDisposableLoggerScope("GetTopicMapObjectBySourceLocator")) {
				string query;
				query = "from TopicMapObject o where :sourceLocator in elements(o.SourceLocators)";
				SimpleQuery<TopicMapObject> q = new SimpleQuery<TopicMapObject>(query);
				q.SetParameter("sourceLocator", ((Implementations.Locator) sourceLocator).locator_);
				TopicMapObject[] objects = q.Execute();


				if (objects != null && objects.Length > 0) {
					TopicMapObject o = objects[0];
					if (o is Topic)
						return new Implementations.Topic(topicMap_, (Topic) o);
					else if (o is TopicName)
						return new Implementations.TopicName(topicMap_, (TopicName) o);
					else
						throw new NotImplementedException(o.GetType().Name);
				}
				else return null;
			}
		}

		#endregion

	}
}