namespace tmapi4net.Providers.ActiveRecord.Index {
	using Castle.ActiveRecord.Queries;
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;
	using tmapi4net.Index.Core;
	using tmapi4net.Providers.ActiveRecord.Model;
	using tmapi4net.Providers.ActiveRecord.Repositories;

	class ActiveRecordAssociationRolesIndex : ActiveRecordIndex, IAssociationRolesIndex {

		public ActiveRecordAssociationRolesIndex(ITopicMap topicMap) : base(topicMap) {
	

		}

		#region IAssociationRolesIndex Members

		ISet<tmapi4net.Core.ITopic> IAssociationRolesIndex.GetAssociationRoleTypes() {
			using (IDisposableLoggerScope scope = GetDisposableLoggerScope("GetAssociationRoleTypes")) {
				return getTypedObjectTypes<AssociationRole>();
			}
		}

		ISet<tmapi4net.Core.IAssociationRole> IAssociationRolesIndex.GetAssociationRolesByType(tmapi4net.Core.ITopic type) {
			using (IDisposableLoggerScope scope = GetDisposableLoggerScope("GetAssociationRolesByType")) {
				return new Implementations.AssociationRoleSet(topicMap_, getTypedObjectsByType<Model.AssociationRole>(type));
			}
		}

		#endregion

	}
}