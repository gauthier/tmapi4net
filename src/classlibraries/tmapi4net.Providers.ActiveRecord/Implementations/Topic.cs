using System;
using System.Collections.Generic;
using System.Text;

namespace tmapi4net.Providers.ActiveRecord.Implementations {
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;


	class Variant : ScopedObject, IVariant {
		private Model.Variant variant_;
		public Variant(ITopicMap topicMap, Model.Variant variant) 
		:base(topicMap, variant) {
			variant_ = variant;
		}
#region IVariant Members

		ITopicName IVariant.TopicName {
			get {
				if (variant_.TopicName == null) return null;
				return new TopicName(topicMap_, variant_.TopicName);
			}
		}

		string IVariant.Value {
			get {
				return variant_.Value;
			}
			set {
				variant_.Value = value;
			}
		}

		ILocator IVariant.Resource {
			get {
				if(variant_.Locator == null) return null;
				return new Locator(variant_.Locator);
			}
			set {
				if(value  == null) variant_.Locator = null;
				else variant_.Locator = ((Locator) value).locator_;
			}
		}

		void IVariant.Remove() {
			topicMapRepository_.RemoveVariant(this.variant_);
		}

		IEnumerable<ITopic> IVariant.Scope {
			get { throw new Exception("The method or operation is not implemented."); }
		}

		#endregion
	}

	class Topic : TopicMapObject,ITopic {
		internal Model.Topic topic_;
		private Repositories.ITopicMapRepository topicMapRepository_ = new Repositories.TopicMapRepository();
		internal Topic(ITopicMap topicMap, Model.Topic topic):base(topicMap, topic) { 
			topic_ = topic;
		}

		ITopic selfAsITopic{get { return this;}}
		#region ITopic Members

		IReadOnlySet<ITopicName> ITopic.TopicNames {
			get {
				if (topic_.TopicNames == null) return new TopicNameSet();
				return new TopicNameSet(topicMap_, topic_.TopicNames);
			}
		}

		ITopicName ITopic.CreateTopicName(string topicName, IEnumerable<ITopic> scope) {
			return selfAsITopic.CreateTopicName(topicName, null, scope);
		
		}

		ITopicName ITopic.CreateTopicName(string topicName, ITopic type, IEnumerable<ITopic> scope) {
IList<Model.Topic> scopemodel = new List<Model.Topic>();
			if (scope != null) {
				foreach (Topic t in scope)
					scopemodel.Add(t.topic_);
			}
			Model.Topic topicnametype = null;
			if(type != null)
				 topicnametype = ((Topic) type).topic_;
			Model.TopicName topicname = topicMapRepository_.CreateTopicName(this.topic_, topicName, topicnametype, scopemodel);
			ITopicName tn = new TopicName(topicMap_, topicname);
			tn.Type = type;

			return tn;

		}

		IOccurrence ITopic.CreateOccurrence(string occurrenceData, ITopic type, IEnumerable<ITopic> scope) {
			return createOccurrence(null, occurrenceData, type, scope);
		}

		IOccurrence ITopic.CreateOccurrence(ILocator resource, ITopic type, IEnumerable<ITopic> scope) {
			return createOccurrence(resource, null, type, scope);
		}



		IReadOnlySet<IOccurrence> ITopic.Occurrences {
			get {
				if (topic_.Occurrences == null) return new OccurrenceSet();
				return new OccurrenceSet(topicMap_, topic_.Occurrences);
				
				throw new Exception("The method or operation is not implemented.");
			}
		}

		IReadOnlySet<ILocator> ITopic.SubjectLocators {
			get {
				return new LocatorSet(topic_.SubjectLocators);
			}
		}

		void ITopic.AddSubjectLocator(ILocator subjectLocator) {
			topic_.SubjectLocators.Add(((Implementations.Locator)subjectLocator).locator_);
			topicMapRepository_.Save(topic_);
		}

		void ITopic.RemoveSubjectLocator(ILocator subjectLocator) {
			topic_.SubjectLocators.Remove(((Implementations.Locator)subjectLocator).locator_);
			topicMapRepository_.Save(topic_);
		}

		IReadOnlySet<ILocator> ITopic.SubjectIdentifiers {
			get { return new LocatorSet(topic_.SubjectIdentifiers); }
		}

		void ITopic.AddSubjectIdentifier(ILocator subjectIdentifier) {
			topicMapRepository_.AddSubjectIdentifier(
				topic_, 
				((Locator)subjectIdentifier).locator_);
		}

		void ITopic.RemoveSubjectIdentifier(ILocator subjectIdentifier) {
			topic_.SubjectIdentifiers.Remove(((Locator)subjectIdentifier).locator_);
			topicMapRepository_.Save(topic_);
		}

		IReadOnlySet<ITopic> ITopic.Types {
			get {
				return new TopicSet(topicMap_, topic_.Types);
			}
		}

		void ITopic.AddType(ITopic type) {
			topicMapRepository_.AddTypeToTopic(topic_, ((Implementations.Topic)type).topic_);
		}

		void ITopic.RemoveType(ITopic type) {
			topicMapRepository_.RemoveTypeToTopic(topic_, ((Implementations.Topic)type).topic_);
		}

		IReadOnlySet<IAssociationRole> ITopic.RolesPlayed {
			get {
				if (topic_.RolesPlayed == null) return null;
				return new AssociationRoleSet(topicMap_, topic_.RolesPlayed);
			}
		}

		void ITopic.Remove() {
			topicMapRepository_.RemoveTopic(topic_);
		}

		void ITopic.MergeIn(ITopic other) {
			throw new Exception("The method or operation is not implemented.");
		}

		IReadOnlySet<ITopicMapObject> ITopic.Reified {
			get {
				return new TopicMapObjectSet(topicMap_, topicMapRepository_.GetTopicMapObjectsReifiedBy(topic_));
				throw new Exception("The method or operation is not implemented.");
			}
		}

		void ITopic.AddSourceLocator(ILocator sourceLocator) {
			selfAsITopicMapObject.AddSourceLocator(sourceLocator);
		}

		#endregion

		IOccurrence createOccurrence(ILocator resource, string occurrenceData, ITopic type, IEnumerable<ITopic> scope) {
			Model.Topic occurrencetype = null;
			Model.TopicSet occurrencescope = new Model.TopicSet();
			Model.Locator occurrenceresource = null;
			if(resource != null) {
				occurrenceresource = ((Implementations.Locator) resource).locator_;
			}
			if (scope != null) {
				foreach (ITopic topic in scope) {
					if (topic is Implementations.Topic) {
						occurrencescope.Add(((Implementations.Topic)topic).topic_);
					}
				}
			}
			if (type != null)
				occurrencetype = ((Implementations.Topic)type).topic_;
			Model.Occurrence occurrence = topicMapRepository_.CreateOccurrence(topic_, occurrenceresource, occurrenceData, occurrencetype, occurrencescope);
			return new Implementations.Occurrence(topicMap_, occurrence);
		}
/*
		public override int GetHashCode() {
			return topic_.Key.GetHashCode();
			
		}

		public override bool Equals(object obj) {
			if (obj is Topic) {
				Topic t = (Topic)obj;
				return t.topic_.Key == topic_.Key;
			}
			return false;
		}

		public static bool operator !=(Topic emp1, Topic emp2) {

			return emp1.GetHashCode() != emp2.GetHashCode();

		}
		public static bool operator ==(Topic emp1, Topic emp2) {

			return emp1.GetHashCode() ==  emp2.GetHashCode();

		}*/
	}
}
