namespace tmapi4net.Providers.ActiveRecord.Implementations {
	using System;
	using System.Globalization;
	using tmapi4net.Core;

	class Locator : ILocator {
		internal Model.Locator locator_;
		internal Locator(Model.Locator locatorModel) {
			locator_ = locatorModel;
		}

		#region ILocator Members

		string ILocator.Reference {
			get { return locator_.Address; }
		}

		string ILocator.Notation {
			get { return locator_.Notation; }
		}

		ILocator ILocator.ResolveRelative(string relative) {
			throw new Exception("The method or operation is not implemented.");
		}

		string ILocator.ToExternalForm() {
			throw new Exception("The method or operation is not implemented.");
		}

		#endregion



		public override bool Equals(object obj) {
			if (this.GetType() == obj.GetType()) {
				return this.locator_.Key.GetHashCode() ==
					   ((Locator)obj).locator_.Key.GetHashCode();
			}
			return false;
		}

		public override int GetHashCode() {
			return locator_.Key.GetHashCode();
		}


		public static bool operator !=(Locator emp1, Locator emp2) {

			return emp1.GetHashCode() != emp2.GetHashCode();
		}
		public static bool operator ==(Locator emp1, Locator emp2) {

			return emp1.GetHashCode() == emp2.GetHashCode();


		}

		#region IComparable<ILocator> Members

		int IComparable<ILocator>.CompareTo(ILocator other) {
			if(other == null)
				return 1;
			int notationdif = CultureInfo.CurrentCulture.CompareInfo.Compare(locator_.Notation, other.Notation);
			if(notationdif != 0) return notationdif;
			return CultureInfo.CurrentCulture.CompareInfo.Compare(locator_.Address, other.Reference);
		}

		#endregion

		#region IComparable Members

		int IComparable.CompareTo(object obj) {
			if (obj is ILocator) return ((IComparable<ILocator>)this).CompareTo((ILocator)obj);
			return 1;
		}

		#endregion
	}
}