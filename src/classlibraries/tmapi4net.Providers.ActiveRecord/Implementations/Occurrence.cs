namespace tmapi4net.Providers.ActiveRecord.Implementations {
	using tmapi4net.Core;

	class Occurrence : ScopedObject, IOccurrence {
		internal Occurrence	(ITopicMap topicMap, Model.Occurrence occurrence):base(topicMap, occurrence) {
			occurrence_ = occurrence;
		}

		private Model.Occurrence occurrence_;

		#region IOccurrence Members

		ITopic IOccurrence.Topic {
			get {
				if(occurrence_.Topic == null) return null;
				return new Topic(topicMap_, occurrence_.Topic);
			}
		}

		public ITopic Type {
			get {
				if (occurrence_.Type == null)
					return null;
				return new Topic(topicMap_, occurrence_.Type);
			}
			set {
				if (value != occurrence_.Type) {
					if (value == null)
						occurrence_.Type = null;
					else
						occurrence_.Type = ((Topic)value).topic_;
					topicMapRepository_.Save(occurrence_);
				}
			}
		}

		string IOccurrence.Value {
			get {
				return occurrence_.Value;
			}
			set {
				occurrence_.Locator = null;
				occurrence_.Value = value;
				topicMapRepository_.Save(occurrence_);
			}
		}

		ILocator IOccurrence.Resource {
			get {
				if (occurrence_.Locator == null) return null;
				return new Locator(occurrence_.Locator);
			}
			set {
				occurrence_.Value = null;
				if (value == null) occurrence_.Locator = null;
				else
					occurrence_.Locator = ((Locator) value).locator_;
				topicMapRepository_.Save(occurrence_);
			}
		}

		void IOccurrence.Remove() {
			topicMapRepository_.RemoveOccurrence(occurrence_);
		}

		#endregion
	}
}