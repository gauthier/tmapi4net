namespace tmapi4net.Providers.ActiveRecord.Implementations {
	using System;
	using tmapi4net.Core;
	using System.Collections.Generic;
	using tmapi4net.Core.Collections;

	class ScopedObjectSet : List<IScopedObject>, ISet<IScopedObject> {
		internal ScopedObjectSet() { }
		internal ScopedObjectSet(ITopicMap topicMap, IEnumerable<Model.ScopedObject> scopedObjects) {
			if (scopedObjects != null) {
				foreach (Model.ScopedObject scopedobject in scopedObjects) {

					this.Add(new ScopedObject(topicMap, scopedobject));
				}
			}
		}
	}

	class TopicSet: List<ITopic> , ISet<ITopic> {
		internal TopicSet(){}
		internal TopicSet(ITopicMap topicMap, IEnumerable<Model.Topic> topics) { 
			if (topics != null) {
				foreach (Model.Topic topic in topics) {
					
					this.Add(new Topic(topicMap, topic));
				}
			}
		}

	}
	
	class TopicMapObjectSet : List<ITopicMapObject> , IReadOnlySet<ITopicMapObject> {
		internal TopicMapObjectSet(){}
		internal TopicMapObjectSet(ITopicMap topicMap, IEnumerable<Model.TopicMapObject> topicmapobjects) { 
			if (topicmapobjects != null) {
				foreach (Model.TopicMapObject o in topicmapobjects) {
					if(o.GetType() == typeof(Model.TopicMap)) {
						this.Add(new TopicMap(topicMap.TopicMapSystem, o as Model.TopicMap));
					}else if(o.GetType() == typeof(Model.Topic)) {
						this.Add(new Topic(topicMap, o as Model.Topic));
					} else if (o.GetType() == typeof(Model.Association)) {
						this.Add(new Association(topicMap, o as Model.Association));
					}
					 else if (o.GetType() == typeof(Model.AssociationRole)) {
					 	this.Add(new AssociationRole(topicMap, o as Model.AssociationRole));
					 }
					 else if (o.GetType() == typeof(Model.Occurrence)) {
					 	this.Add(new Occurrence(topicMap, o as Model.Occurrence));
					 }
					 else if (o.GetType() == typeof(Model.TopicName)) {
					 	this.Add(new TopicName(topicMap, o as Model.TopicName));
					 }
					 else if (o.GetType() == typeof(Model.Variant)) {
					 	this.Add(new Variant(topicMap, o as Model.Variant));
					 }

				}
			}
		}
	}

	class AssociationRoleSet : List<IAssociationRole>, ISet<IAssociationRole> {
		internal AssociationRoleSet() { }
		internal AssociationRoleSet(ITopicMap topicMap, IEnumerable<Model.AssociationRole> associationRoles) {
			if (associationRoles != null) {
				Implementations.TopicMap map = null;
				foreach (Model.AssociationRole role in associationRoles) {
					this.Add(new AssociationRole(topicMap, role));
				}
			}
		}
	}

	internal class VariantSet :
		List<IVariant>, IReadOnlySet<IVariant> {
		internal VariantSet() : base() { }
		internal VariantSet(ITopicMap topicMap, IEnumerable<Model.Variant> variants) {


			foreach (Model.Variant variant in variants) {
				this.Add(new Variant(topicMap, variant));
			}
		}

	}

	class AssociationSet : List<IAssociation>, ISet<IAssociation> {
		internal AssociationSet() { }
		internal AssociationSet(ITopicMap topicMap, IEnumerable<Model.Association> associations) {

			foreach (Model.Association association in associations) {

				this.Add(new Association(topicMap, association));

			}
		}

	}

	class OccurrenceSet : List<IOccurrence>, ISet<IOccurrence> {
		internal OccurrenceSet() { }
		internal OccurrenceSet(ITopicMap topicMap, IEnumerable<Model.Occurrence> occurrences) {

			foreach (Model.Occurrence occurrence in occurrences) {
				this.Add(new Occurrence(topicMap, occurrence));

			}
		}
	}



	class TopicNameSet : List<ITopicName>, ISet<ITopicName> {
		internal TopicNameSet() { }
		internal TopicNameSet(ITopicMap topicMap, IEnumerable<Model.TopicName> topicNames) {
			
			foreach (Model.TopicName topicname in topicNames) {
				this.Add(new TopicName(topicMap, topicname));

			}
		}
	}

	class Association :ScopedObject, IAssociation {
		private ITopic type_;
		private Model.Association association_;
		
		public Association():base(){}
		internal Association(ITopicMap topicMap, Model.Association association) :base(topicMap, association) {
			association_ = association;
		}

		#region IAssociation Members

		public ITopic Type {
			get {
				if (association_.Type == null)
					return null;
				return new Topic(topicMap_, association_.Type);
			}
			set {
				if (value != association_.Type) {
					if (value == null)
						association_.Type = null;
					else
						association_.Type = ((Topic)value).topic_;
					topicMapRepository_.Save(association_);
				}
			}
		}

		IAssociationRole IAssociation.CreateAssociationRole(ITopic player, ITopic roleSpec) {
			
			Model.Topic p = null;
			Model.Topic t = null; 
			if(player != null) p = ((Topic) player).topic_;
			if (roleSpec != null) t = ((Topic) roleSpec).topic_;
			Model.AssociationRole associationrole = topicMapRepository_.CreateAssociationRole(association_, p, t);
			return new Implementations.AssociationRole (this.topicMap_, associationrole);
		}

		IReadOnlySet<IAssociationRole> IAssociation.AssociationRoles {
			get {
				AssociationRoleSet associations = new AssociationRoleSet();
				if (association_.Roles != null) {
					foreach (Model.AssociationRole role in association_.Roles) {
						associations.Add(new Implementations.AssociationRole(this.topicMap_, role));
					}
				}
				return associations;
			}
		}

		void IAssociation.Remove() {
			topicMapRepository_.RemoveAssociation(this.association_);
		}

		#endregion
	}
}