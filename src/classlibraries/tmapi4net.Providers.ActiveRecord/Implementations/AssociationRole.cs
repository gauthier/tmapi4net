namespace tmapi4net.Providers.ActiveRecord.Implementations {
	using System;
	using tmapi4net.Core;

	class AssociationRole : ReifiedObject,IAssociationRole {

		private Model.AssociationRole associationRole_;
		internal AssociationRole (ITopicMap topicMap, Model.AssociationRole associationRole) : base(topicMap, associationRole) {
			associationRole_ = associationRole;
			association_ = associationRole.Association;
		}

		private Model.Association association_;

		#region IAssociationRole Members

		IAssociation IAssociationRole.Association {
			get { return new Association(this.topicMap_, association_); }
		}

		ITopic IAssociationRole.Player {
			get {
				if (associationRole_.Player == null)
					throw new Exception("The method or operation is not implemented.");
				return new Topic(topicMap_, associationRole_.Player);
			}
			set {
				if(value == null)
					associationRole_.Player = null;
				associationRole_.Player = ((Topic) value).topic_;
				topicMapRepository_.Save(this.associationRole_);
			}
		}

		public ITopic Type {
			get {
				if (associationRole_.Type == null)
					return null;
				return new Topic(topicMap_, associationRole_.Type);
			}
			set {
				if(value != associationRole_.Type)
				{
					if(value == null)
						associationRole_.Type = null;
					else
						associationRole_.Type = ((Topic) value).topic_;
					topicMapRepository_.Save(associationRole_);
				}
			}
		}
		void IAssociationRole.Remove() {
			topicMapRepository_.RemoveAssociationRole(associationRole_);
		}

		#endregion

		public override int GetHashCode() {
			return associationRole_.Key.GetHashCode();
		}
		public override bool Equals(object o) {
			if(o is AssociationRole) {
				return ((object)o).GetHashCode() == ((object)this).GetHashCode();
			}
			return false;
		}

		ITopic IAssociationRole.Reifier {
			get {
				if (base.reifiedObject_.Reifier == null)
					return null;
				return new Topic(topicMap_, base.reifiedObject_.Reifier);
			}
		}
	}
}