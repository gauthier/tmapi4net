namespace tmapi4net.Providers.ActiveRecord.Implementations {
	using Castle.ActiveRecord;
	using tmapi4net.Core;



	class ReifiedObject : TopicMapObject, IReifiable {
		internal ReifiedObject(){}
		internal ReifiedObject(ITopicMap topicMap, Model.ReifiedObject reifiedObject)
			: base(topicMap, reifiedObject) {

			reifiedObject_ = reifiedObject;

		}

		internal Model.ReifiedObject reifiedObject_;


		#region IHasReifier Members

		ITopic IReifiable.Reifier {
			get {

				Model.Topic t = reifiedObject_.Reifier;
				if (t == null) t = topicMapRepository_.GetReifier(reifiedObject_);
				if (t == null) return null;
				return new Implementations.Topic(this.topicMap_, t);
			}
		}

		#endregion
	}
}
