namespace tmapi4net.Providers.ActiveRecord.Implementations {
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;

	class ScopedObject : ReifiedObject, IScopedObject {
		internal ScopedObject():base(){}
		internal Model.ScopedObject scopedObject_;
		internal ScopedObject(ITopicMap topicMap, Model.ScopedObject scopedObject)  : base(topicMap, scopedObject) {
			scopedObject_ = scopedObject;

		}
		
		
		#region IScopedObject Members

		IReadOnlySet<ITopic> IScopedObject.Scope {
			get {
				IReadOnlySet<ITopic> topics;
				if (scopedObject_.Scope != null) {
					topics = new Implementations.TopicSet(topicMap_, scopedObject_.Scope);
				}
				else
					topics = new Implementations.TopicSet();
				return topics;
			}
		}

		void IScopedObject.AddScopingTopic(ITopic topic) {
			topicMapRepository_.AddScopingTopic(this.scopedObject_, ((Topic)topic).topic_);
		}

		void IScopedObject.RemoveScopingTopic(ITopic topic) {
			topicMapRepository_.RemoveScopingTopic(this.scopedObject_, ((Topic)topic).topic_);
		}


		#endregion

	

	}
}