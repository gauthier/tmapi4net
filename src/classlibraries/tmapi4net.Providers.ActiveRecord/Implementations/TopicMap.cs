namespace tmapi4net.Providers.ActiveRecord.Implementations {
	using System;
	using System.Collections.Generic;
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;
	using tmapi4net.Core.Exceptions;
	using tmapi4net.Index.Core;
	using tmapi4net.Providers.ActiveRecord.Index;

	class TopicMap : ReifiedObject,ITopicMap {
		internal Model.TopicMap topicMap_;
		private Repositories.ITopicMapRepository topicMapRepository_ = new Repositories.TopicMapRepository();
		private ITopicMapSystem topicMapSystem_;

		internal TopicMap(ITopicMapSystem topicMapSystem, Model.TopicMap topicMap) : base(null, topicMap) {
			topicMap_ = topicMap;
			topicMapSystem_ = topicMapSystem;
			helperObjects_.Add(typeof(IAssociationRolesIndex), typeof(ActiveRecordAssociationRolesIndex));
			helperObjects_.Add(typeof(IAssociationsIndex), typeof(ActiveRecordAssociationsIndex));
			helperObjects_.Add(typeof(IOccurrencesIndex), typeof(ActiveRecordOccurrencesIndex));
			helperObjects_.Add(typeof(IScopedObjectsIndex), typeof(ActiveRecordScopedObjectsIndex));
			helperObjects_.Add(typeof(ITopicMapObjectsIndex), typeof(ActiveRecordTopicMapObjectsIndex));
			helperObjects_.Add(typeof(ITopicNamesIndex), typeof(ActiveRecordTopicNamesIndex));
			helperObjects_.Add(typeof(IVariantsIndex), typeof(ActiveRecordVariantsIndex));
			helperObjects_.Add(typeof(ITopicsIndex), typeof(ActiveRecordTopicsIndex));
		}

		#region ITopicMap Members

		ITopicMapSystem ITopicMap.TopicMapSystem {
			get { return topicMapSystem_; }
		}

		ILocator ITopicMap.BaseLocator {
			get { return new Locator(topicMap_.BaseLocator); }
		}

		IReadOnlySet<IAssociation> ITopicMap.Associations {
			get {

				return new AssociationSet(this, topicMapRepository_.GetAllAssociations(topicMap_));
			}
		}

		IAssociation ITopicMap.CreateAssociation() {
			Model.Association association = topicMapRepository_.CreateAssociation(topicMap_);
			return new Implementations.Association(this, association);
		}

		IReadOnlySet<ITopic> ITopicMap.Topics {
			get {
				return new TopicSet(this, topicMapRepository_.GetAllTopics(topicMap_));
			}
		}

		ITopic ITopicMap.CreateTopic() {
			Model.Topic topic = topicMapRepository_.CreateTopic(topicMap_);
			if (topic == null)
				throw new NotImplementedException();
			
			return new Implementations.Topic(this, topic);
				
		}

		ILocator ITopicMap.CreateLocator(string address, string notation) {
			return new Implementations.Locator(topicMapRepository_.CreateLocator(notation, address));
		}

		ILocator ITopicMap.CreateLocator(string address) {
			return new Implementations.Locator(topicMapRepository_.CreateLocator("URI", address));
		}


/*

		ITopic ITopicMap.Reifier {
			get {
				foreach (ILocator locator in sourceLocators_) {
					ITopic topic = getTopicBySubjectIdentifier(locator);
					if (topic != null)
						return topic;
				}
				return null;
			}
		}*/
		/*
		ITopic ITopicMap.Reifier {
			get {
				Model.Topic t = topicMap_.Reifier;
				if (t == null) topicMapRepository_.GetReifier(topicMap_);
				if (t == null) return null;
				return new Implementations.Topic(this, t);
			}
		}*/

		void ITopicMap.Remove() {
			topicMapRepository_.RemoveTopicMap(topicMap_);
		}

		ITopicMapObject ITopicMap.GetObjectById(string objectId) {
			Model.TopicMapObject o = topicMapRepository_.GetObjectById(topicMap_, objectId);
			if(o == null)
				return null;
			ITopicMapObject topicmapobject = null;
			switch(o.Class) {
				case"Topic":
					topicmapobject = new Implementations.Topic(this, (Model.Topic) o);
					break;
				case "TopicName":
					topicmapobject = new Implementations.TopicName(this, (Model.TopicName)o);
					break;
				case "Association":
					topicmapobject = new Implementations.Association(this, (Model.Association)o);
					break;
				case "AssociationRole":
					topicmapobject = new Implementations.AssociationRole(this, (Model.AssociationRole)o);
					break;
				case "Occurrence":
					topicmapobject = new Implementations.Occurrence(this, (Model.Occurrence) o);
					break;

				default:
					throw new NotImplementedException();
			}
			return topicmapobject;
		}

		void ITopicMap.MergeIn(ITopicMap other) {
			throw new Exception("The method or operation is not implemented.");
		}

		TInterface ITopicMap.GetHelperObject<TInterface>() {
			if (helperObjects_.ContainsKey(typeof(TInterface)))
				return
					(TInterface) Activator.CreateInstance(helperObjects_[typeof (TInterface)], this);
			else
				throw TMAPIException.CreateUnsupportedHelperException(typeof (TInterface));
		}

		void ITopicMap.Close() {
			throw new Exception("The method or operation is not implemented.");
		}

		#endregion

		private IDictionary<Type, Type> helperObjects_ = new Dictionary<Type, Type>();
	
	}
}