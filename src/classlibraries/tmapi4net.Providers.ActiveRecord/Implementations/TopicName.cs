namespace tmapi4net.Providers.ActiveRecord.Implementations {
	using System;
	using System.Collections.Generic;
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;


	class TopicName : ScopedObject,ITopicName {
		
		private Model.TopicName topicName_;
		private Repositories.ITopicMapRepository topicMapRepository_ = new Repositories.TopicMapRepository();
		private ITopic type_;
		internal TopicName(ITopicMap topicMap, Model.TopicName topicName) : base(topicMap, topicName) {
			topicName_ = topicName;
			if(topicName.Type != null)
				type_ = new Topic(topicMap, topicName_.Type);
		}


		#region ITopicName Members

		ITopic ITopicName.Topic {
			get {
				if (topicName_.Topic != null)			
				return new Topic(topicMap_, topicName_.Topic);
				return null;
			}
		}

		string ITopicName.Value {
			get {
				return topicName_.Value ;
			}
			set {
				if (value != topicName_.Value) {
					topicName_.Value = value;
					topicMapRepository_.Save(topicName_);
				}
			}
		}

		IReadOnlySet<IVariant> ITopicName.Variants {
			get {
				if(topicName_.Variants != null) {
					return new VariantSet(topicMap_, topicName_.Variants);
				}
				
				return new VariantSet(); }
		}

		IVariant ITopicName.CreateVariant(string resourceData, IEnumerable<ITopic> scope) {
		
			Model.TopicSet variantscope = new Model.TopicSet();
			if(scope != null)
				foreach (ITopic topic in scope) {
					variantscope.Add(((Implementations.Topic)topic).topic_);
				}
			Model.Variant variant = topicMapRepository_.CreateVariant(topicName_, null, resourceData, variantscope);
			return new Implementations.Variant(topicMap_, variant);
		}

		IVariant ITopicName.CreateVariant(ILocator resource, IEnumerable<ITopic> scope) {
			Model.Locator variantlocator = null;
			if(resource!=null)
				variantlocator = ((Locator) resource).locator_;
			
			Model.TopicSet variantscope = new Model.TopicSet();
			if(scope != null)
				foreach (ITopic topic in scope) {
					variantscope.Add(((Implementations.Topic)topic).topic_);
				}

			Model.Variant variant = topicMapRepository_.CreateVariant(topicName_, variantlocator, null, variantscope);
			return new Implementations.Variant(topicMap_, variant);
		}

		public ITopic Type {
			get {
				return type_;
			}
			set {
				if (value == null) {
					if (topicName_.Type != null) {
						topicName_.Type = null;
						type_ = null;
						topicMapRepository_.Save(topicName_);
					}
				}
				else {
					if (topicName_.Type != ((Topic)value).topic_) {
						topicName_.Type = ((Topic) value).topic_;
						type_ = value;
						topicMapRepository_.Save(topicName_);
					}
				}
			}
		}

		void ITopicName.Remove() {
			topicMapRepository_.RemoveTopicName(this.topicName_);

		}

		void ITopicName.AddScopingTopic(ITopic topic) {
			topicMapRepository_.AddScopingTopic(this.topicName_, ((Implementations.Topic)topic).topic_);
		}

		void ITopicName.RemoveScopingTopic(ITopic topic) {
			topicMapRepository_.RemoveScopingTopic(this.topicName_, ((Implementations.Topic)topic).topic_);
		}

		#endregion

	}
}