namespace tmapi4net.Providers.ActiveRecord.Implementations {
	using System;
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;

	class TopicMapObject : ITopicMapObject {
		protected Model.TopicMapObject topicMapObject_;
		protected  Repositories.ITopicMapRepository topicMapRepository_ = new Repositories.TopicMapRepository();
		internal TopicMapObject() {
		}

		protected ITopicMapObject selfAsITopicMapObject { get { return this; } }
		
		internal TopicMapObject(ITopicMap topicMap, Model.TopicMapObject topicMapObject){
			topicMap_ = topicMap;
			topicMapObject_ = topicMapObject;
		}
		
		
		protected ITopicMap topicMap_;


		public override bool Equals(object obj) {
			if(this.GetType() == obj.GetType())
			{
				return this.topicMapObject_.Key.GetHashCode() ==
				       ((TopicMapObject) obj).topicMapObject_.Key.GetHashCode();
			}
			return false;
		}

		public override int GetHashCode() {
			return base.GetHashCode();
		}
		#region ITopicMapObject Members

		ITopicMap ITopicMapObject.TopicMap {
			get { return topicMap_; }
		}

		IReadOnlySet<ILocator> ITopicMapObject.SourceLocators {
			get { return new LocatorSet(topicMapObject_.SourceLocators); }
		}

		void ITopicMapObject.AddSourceLocator(ILocator sourceLocator) {

			topicMapRepository_.AddSourceLocator(topicMapObject_, ((Locator)sourceLocator).locator_);

		}

		void ITopicMapObject.RemoveSourceLocator(ILocator sourceLocator) {
			topicMapObject_.SourceLocators.Remove(((Locator)sourceLocator).locator_);
			topicMapRepository_.Save(topicMapObject_);
		}

		void ITopicMapObject.Remove() {
			throw new Exception("The method or operation is not implemented.");
		}

		string ITopicMapObject.ObjectId {
			get {
				return topicMapObject_.Id;
			}
		}

		#endregion

		#region IEquatable<ITopicMapObject> Members

		bool IEquatable<ITopicMapObject>.Equals(ITopicMapObject other) {
			return this.Equals((object) other);
		}

		#endregion




		#region IComparable<ITopicMapObject> Members

		int IComparable<ITopicMapObject>.CompareTo(ITopicMapObject other) {
			return topicMapObject_.Id.CompareTo(other.ObjectId);
		}

		#endregion


		#region IComparable Members

		int IComparable.CompareTo(object obj) {
			if (obj == null) return 1;
			else if (obj is ITopicMapObject) return topicMapObject_.Id.CompareTo(((ITopicMapObject)obj).ObjectId);
			else return 1;
		}

		#endregion
	}
}