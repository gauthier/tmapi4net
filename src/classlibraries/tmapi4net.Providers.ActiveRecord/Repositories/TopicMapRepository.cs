namespace tmapi4net.Providers.ActiveRecord.Repositories {
	using System;
	using System.Collections.Generic;
	using System.Collections;
	using Castle.ActiveRecord;
	using Castle.ActiveRecord.Framework;
	using Castle.ActiveRecord.Queries;
	using log4net;
	using NHibernate;
	using NHibernate.Criterion;
	using NHibernate.Type;
	using tmapi4net.Providers.ActiveRecord.Index;
	using tmapi4net.Providers.ActiveRecord.Model;


	public interface IDisposableLoggerScope : IDisposable { }
	class DisposableLoggerXmlScope : IDisposableLoggerScope {
		DateTime scopetime;
		internal DisposableLoggerXmlScope(string tagName, string title, ILog logger) {
			scopetime = DateTime.Now;
			logger_ = logger;
			tagName_ = tagName;
			if (logger_.IsDebugEnabled) {
				logger_.DebugFormat(@"<{0} title=""{1}"">", tagName_, title);
			}
		}


		string tagName_;
		ILog logger_;


		#region IDisposable Members

		void IDisposable.Dispose() {

			if (logger_.IsDebugEnabled) {
				TimeSpan span = DateTime.Now - scopetime;
				logger_.DebugFormat(@"<timespan totalms=""{0}""/>",span.TotalMilliseconds);
				logger_.DebugFormat("</{0}>", tagName_);
			}
		}

		#endregion
	}
	

	internal class TopicMapRepository : ITopicMapRepository {

		static readonly ILog logger = LogManager.GetLogger(typeof(TopicMapRepository));
		
		
		ITopicMapRepository selfAsITopicMapRepository {
			get { return this;}

		}

		private TransactionScope currentTransaction_;

		#region ILocatorRepository Members

		

		IEnumerable<Locator> ILocatorRepository.GetTopicMapBaseLocators() {
			using (IDisposableLoggerScope scope = new DisposableLoggerXmlScope("topicmaprepository", "GetTopicMapBaseLocators", logger)) {
				SimpleQuery<Model.Locator> baseLocatorsQuery =
					new SimpleQuery<Model.Locator>(@"select l from Locator l, TopicMap m where m.BaseLocator = l");

				IList<Model.Locator> models = (IList<Model.Locator>)
				                              ActiveRecordMediator<Model.Locator>.ExecuteQuery(baseLocatorsQuery);

				return models;
			}
		}


		Locator ILocatorRepository.GetLocator(string notation, string address) {
			using (IDisposableLoggerScope scope = new DisposableLoggerXmlScope("topicmaprepository", "GetLocator", logger)) {
				IList<Model.Locator> locators =
					ActiveRecordMediator<Model.Locator>.FindAll(
						NHibernate.Criterion.Restrictions.Eq("Notation", notation),
						NHibernate.Criterion.Restrictions.Eq("Address", address)
						);

				Model.Locator locator;
				if (locators.Count == 0) {
					return null;
				}
				else
					return locators[0];
			}
		}

		#endregion

		#region ITopicMapRepository Members

		#region *topicmap

		TopicMap ITopicMapRepository.CreateTopicMap(string baseLocator) {
			using (IDisposableLoggerScope scope = new DisposableLoggerXmlScope("topicmaprepository", "CreateTopicMap", logger)) {
				Locator locator = selfAsITopicMapRepository.CreateLocator("URI", baseLocator);
				TopicMap topicMap = new TopicMap();
				topicMap.BaseLocator = locator;
				topicMap.Id = generateObjectId();
				ActiveRecordMediator<TopicMap>.Save(topicMap);
				return topicMap;
			}
		}

		TopicMap ITopicMapRepository.GetTopicMap(Locator baseLocator) {
			using (IDisposableLoggerScope scope = new DisposableLoggerXmlScope("topicmaprepository", "GetTopicMap", logger)) {
				Model.TopicMap map = ActiveRecordMediator<Model.TopicMap>.FindOne(
					NHibernate.Criterion.Restrictions.Eq("BaseLocator", baseLocator)
					);
				return map;
			}
		}


		void ITopicMapRepository.RemoveTopicMap(TopicMap topicMap) {
			using (IDisposableLoggerScope scope = new DisposableLoggerXmlScope("topicmaprepository", "RemoveTopicMap", logger)) {
				foreach (Association association in selfAsITopicMapRepository.GetAllAssociations(topicMap)) {
					selfAsITopicMapRepository.RemoveAssociation(association);
				}
				List<Topic> topics = new List<Topic>(selfAsITopicMapRepository.GetAllTopics(topicMap));
				foreach (Topic topic in topics) {
					selfAsITopicMapRepository.RemoveTopic(topic);
				}
				removeTopicMapObjects(topicMap);
				ActiveRecordMediator<Model.TopicMap>.Delete(topicMap);
			}
		}

		#endregion

		#region *topic

		IEnumerable<Topic> ITopicMapRepository.GetAllTopics(TopicMap topicMap) {
			using (IDisposableLoggerScope scope = new DisposableLoggerXmlScope("topicmaprepository", "GetAllTopics", logger)) {
				object result = ActiveRecordMediator<Model.Topic>.Execute(typeof (Topic),
				  delegate(ISession session, object instance) {
              		IQuery query =
              			session.CreateQuery(
              				"from Topic t where t.TopicMap.Key = :topicMapKey");
              		query.SetParameter("topicMapKey", topicMap.Key);
              		IList<Topic> list = query.List<Topic>();
              		return list;
				  }, topicMap.Key);

				return (IEnumerable<Topic>) result;
			}
		}

		Model.Topic ITopicMapRepository.CreateTopic(Model.TopicMap topicMap) {
			using (IDisposableLoggerScope scope = new DisposableLoggerXmlScope("topicmaprepository", "CreateTopic", logger)) {
				Model.Topic topic = new Topic(topicMap);
				topic.Id = generateObjectId();
				ActiveRecordMediator<Model.Topic>.Save(topic);
				return topic;
			}
		}

		void ITopicMapRepository.RemoveTopic(Model.Topic topic) {
			using (IDisposableLoggerScope scope = new DisposableLoggerXmlScope("topicmaprepository", "RemoveTopic", logger)) {
				Guid typeKey = topic.Key.Value;
				ISessionFactoryHolder sessionHolder = ActiveRecordMediator.GetSessionFactoryHolder();
				using (ISession session = sessionHolder.CreateSession(typeof (TopicTypesRelation))) {
					string[] queries
						= {
						  	"delete from topic_types where type_key = :typeKey",
						  	"update occurrences set type_key = null where type_key = :typeKey",
						  	"update topicnames set type_key = null where type_key = :typeKey",
						  	"update topics set reifier_key = null where reifier_key = :typeKey",
						  	"update topicmaps set reifier_key = null where reifier_key = :typeKey", 
							"delete from topicmapobject_locators where topicmapobject_key in (select key from occurrences where topic_key = :typeKey)"
						  	,
						  	"delete from occurrences where topic_key = :typeKey",
						  	"delete from variants where topicname_key in (select key from topicnames where topic_key = :typeKey)",
						  	"delete from topicmapobject_locators where topicmapobject_key = :typeKey",
						  	"delete from topicnames where topic_key = :typeKey",
						  	"delete from topics where key = :typeKey",
						  	"delete from topicmapobjects where key = :typeKey"
						  };
					foreach (string query in queries) {
						ISQLQuery q = session.CreateSQLQuery(query);
						q.SetGuid("typeKey", typeKey);
						q.AddEntity(typeof (TopicTypesRelation));
						try {
							object o = q.List();
						}
						catch {
#if RELEASE
					throw;
#endif
						}
					}
				}
			}
		}

		#region type

		void ITopicMapRepository.RemoveTypeToTopic(Topic topic, Topic type) {
			using (IDisposableLoggerScope scope = new DisposableLoggerXmlScope("topicmaprepository", "RemoveTypeFromTopic", logger)) {
				topic.Types.Remove(type);
				TopicTypesRelation rel;
				rel =
					ActiveRecordMediator<TopicTypesRelation>.FindOne(
					NHibernate.Criterion.Restrictions.Eq("Key.TopicKey", topic.Key)
					, NHibernate.Criterion.Restrictions.Eq("Key.TypeKey", type.Key));
				if (rel != null)
					ActiveRecordMediator<TopicTypesRelation>.Delete(rel);
			}
		}

		void ITopicMapRepository.AddTypeToTopic(Topic topic, Topic type) {
			using (IDisposableLoggerScope scope = new DisposableLoggerXmlScope("topicmaprepository", "AddTypeToTopic", logger)) {
				topic.Types.Add(type);
				TopicTypesRelation rel;
				rel =
					ActiveRecordMediator<TopicTypesRelation>.FindOne(NHibernate.Criterion.Restrictions.Eq("Key.TopicKey", topic.Key), NHibernate.Criterion.Restrictions.Eq("Key.TypeKey", type.Key));
				if (rel == null) {
					rel = new TopicTypesRelation();
					rel.Key = new TopicTypesKey(topic.Key, type.Key);
					rel.Key.TopicKey = topic.Key;
					rel.Key.TypeKey = type.Key;
					ActiveRecordMediator<TopicTypesRelation>.Create(rel);
				}
			}
		}

		#endregion

		#region topicnames

		#region variants

		Variant ITopicMapRepository.CreateVariant(TopicName topicName, Locator resource, string resourceData, TopicSet scope) {
			using (IDisposableLoggerScope logscope = new DisposableLoggerXmlScope("topicmaprepository", "CreateVariant", logger)) {
				Model.Variant variant = new Variant(topicName);
				variant.TopicName = topicName;
				variant.Locator = resource;
				variant.Value = resourceData;
				variant.Scope = scope;
				variant.TopicMap = topicName.TopicMap;
				topicName.Variants.Add(variant);
				variant.Id = generateObjectId();
				ActiveRecordMediator<Model.Variant>.Save(variant);
				createDefaultSourceLocator(variant);
				return variant;
			}
		}

		void ITopicMapRepository.RemoveVariant(Variant variant) {
			using (IDisposableLoggerScope scope = new DisposableLoggerXmlScope("topicmaprepository", "RemoveVariant", logger)) {
				variant.TopicName.Variants.Remove(variant);
				ActiveRecordMediator<Model.Variant>.Delete(variant);
			}
		}

		#endregion

		TopicName ITopicMapRepository.CreateTopicName(Topic topic, string topicName, IList<Topic> scope) {
			using (IDisposableLoggerScope loggerscope = new DisposableLoggerXmlScope("topicmaprepository", "CreateTopicName", logger)) {
				return selfAsITopicMapRepository.CreateTopicName(topic, topicName, null, scope);
			}
		}

		TopicName ITopicMapRepository.CreateTopicName(Topic topic, string topicName, Topic type, IList<Topic> scope) {
			using (IDisposableLoggerScope loggerscope = new DisposableLoggerXmlScope("topicmaprepository", "CreateTopicName", logger)) {
				Model.TopicName topicname = new Model.TopicName(topic);
				topicname.Value = topicName;
				topicname.Topic = topic;
				topicname.Type = type;
				topicname.Scope = scope;
				topicname.TopicMap = topic.TopicMap;
				topic.TopicNames.Add(topicname);
				topicname.Id = generateObjectId();
				ActiveRecordMediator<TopicName>.Save(topicname);
				createDefaultSourceLocator(topicname);
				return topicname;
			}
		}

		void ITopicMapRepository.RemoveTopicName(Model.TopicName topicName) {
			using (IDisposableLoggerScope scope = new DisposableLoggerXmlScope("topicmaprepository", "RemoveTopicName", logger)) {
				IEnumerable<Variant> variants = new List<Variant>(topicName.Variants);
				foreach (Variant variant in variants) {
					ActiveRecordMediator<Variant>.Delete(variant);
				}
				topicName.Topic.TopicNames.Remove(topicName);
				topicName.Topic = null;
				ActiveRecordMediator<Model.TopicName>.Delete(topicName);
				
			}
		}

		#endregion

		#region occurrences

		Occurrence ITopicMapRepository.CreateOccurrence(Topic topic, Locator resource, string occurrenceData, Topic occurrenceType, TopicSet occurrenceScope) {
			using (IDisposableLoggerScope scope = new DisposableLoggerXmlScope("topicmaprepository", "CreateOccurrence", logger)) {
				Model.Occurrence occurrence = new Model.Occurrence(topic);
				occurrence.Type = occurrenceType;
				occurrence.Scope = occurrenceScope;
				occurrence.Value = occurrenceData;
				occurrence.Locator = resource;
				occurrence.TopicMap = topic.TopicMap;
				occurrence.Topic = topic;
				topic.Occurrences.Add(occurrence);
				occurrence.Id = generateObjectId();
				ActiveRecordMediator<Model.Occurrence>.Save(occurrence);
				createDefaultSourceLocator(occurrence);
				return occurrence;
			}
		}

		void ITopicMapRepository.RemoveOccurrence(Occurrence occurrence) {
			using (IDisposableLoggerScope scope = new DisposableLoggerXmlScope("topicmaprepository", "RemoveOccurrence", logger)) {
				occurrence.Topic.Occurrences.Remove(occurrence);
				occurrence.Topic = null;
				ActiveRecordMediator<Model.Occurrence>.Delete(occurrence);
			}
		}

		#endregion

		#region roles

		AssociationRole ITopicMapRepository.CreateAssociationRole(Association association, Topic player, Topic roleSpec) {
			Model.AssociationRole associationrole = new AssociationRole(association.TopicMap);
			associationrole.Id = generateObjectId();
			associationrole.Association = association;

			associationrole.Player = player;
			associationrole.Type = roleSpec;
			associationrole.TopicMap = association.TopicMap;
			association.Roles.Add(associationrole);
			if (associationrole.Player != null)
				associationrole.Player.RolesPlayed.Add(associationrole);			
			ActiveRecordMediator<Model.AssociationRole>.Save(associationrole);
			createDefaultSourceLocator(associationrole);
			return associationrole;
		}

		void ITopicMapRepository.RemoveAssociationRole(Model.AssociationRole associationRole) {
			if(associationRole.Player != null)
				associationRole.Player.RolesPlayed.Remove(associationRole);
			
			associationRole.association_.Roles.Remove(associationRole);
			
			ActiveRecordMediator<Model.Association>.Delete(associationRole);
		}

		#endregion

		#endregion

		Association ITopicMapRepository.CreateAssociation(TopicMap topicMap) {
			Model.Association association = new Association(topicMap);
			association.Id = generateObjectId();
			ActiveRecordMediator<Model.Association>.Create(association);
			createDefaultSourceLocator(association);
			return association;
		}

		#region *scopedobject

		void ITopicMapRepository.AddScopingTopic(ScopedObject scopedObject, Model.Topic topic) {
			
			scopedObject.Scope.Add(topic);
			ActiveRecordMediator<Model.ScopedObject>.Save(scopedObject);
		}

		void ITopicMapRepository.RemoveScopingTopic(Model.ScopedObject scopedObject, Model.Topic topic)
	{
			scopedObject.Scope.Remove(topic);
			ActiveRecordMediator<Model.ScopedObject>.Save(scopedObject);
	}

		#endregion

		#region *association

		IEnumerable<Association> ITopicMapRepository.GetAllAssociations(TopicMap topicMap) {
			SimpleQuery<Model.Association> associationsQuery =
				new SimpleQuery<Model.Association>(
					@"
from Association a
where a.TopicMap = :topicMap
");
			
			associationsQuery.SetParameter("topicMap", topicMap);
			
			return ActiveRecordMediator<Model.Association>.ExecuteQuery2(associationsQuery);
		}

		void ITopicMapRepository.RemoveAssociation(Model.Association association) {
			IEnumerable<AssociationRole> roles = new List<AssociationRole>(association.Roles);
			foreach (AssociationRole role in roles) {
				selfAsITopicMapRepository.RemoveAssociationRole(role);
			}
			association.Roles.Clear();
			ActiveRecordMediator<Association>.Delete(association);
		}

		#endregion

		#region *topicmapobject

		void ITopicMapRepository.Save(TopicMapObject topicMapObject) {
			using (IDisposableLoggerScope scope = new DisposableLoggerXmlScope("topicmaprepository", "Save", logger)) {
				ActiveRecordMediator<TopicMapObject>.Save(topicMapObject);
			}
		}

		IEnumerable<TopicMapObject> ITopicMapRepository.GetTopicMapObjectsReifiedBy(Topic topic) {
			string[] queries = new string[] {
			                                	"select m from TopicMap m where m.Reifier.Key = :topicKey",
			                                	"select a from Association a where a.Reifier.Key = :topicKey",
			                                	"select r from AssociationRole r where r.Reifier.Key = :topicKey",
			                                	"select o from Occurrence o where o.Reifier.Key = :topicKey",
			                                	"select tn from TopicName tn where tn.Reifier.Key = :topicKey",
			                                	"select v from Variant v where v.Reifier.Key = :topicKey",
			                                };
			List<TopicMapObject> reified = new List<TopicMapObject>();
			SimpleQuery<TopicMapObject> q = new SimpleQuery<TopicMapObject>("");
			q.SetParameter("topicKey", topic.Key);
			foreach (string query in queries) {
				q.Query = query;
				reified.AddRange(q.Execute());
			}
			return reified;
		}

		TopicMapObject ITopicMapRepository.GetObjectById(TopicMap topicMap, string objectId) {
			using (IDisposableLoggerScope scope = new DisposableLoggerXmlScope("topicmaprepository", "GetObjectById", logger)) {
				object result = ActiveRecordMediator.Execute(typeof (TopicMapObject), delegate(ISession session, object instance) {
//select o.Key, o.Class 
          		IQuery query =
          			session.CreateQuery(
          				"from TopicMapObject o where o.Id = :objectId and o.TopicMap.Key = :topicMapKey");
          		query.SetString("objectId", objectId);
          		query.SetParameter("topicMapKey",
          						   topicMap.Key);
          		object o = query.UniqueResult();
          		return o;
			  }, objectId);
				return (TopicMapObject) result;
			}
			throw new NotImplementedException();
		}

		#endregion

		Locator ILocatorRepository.CreateLocator(string notation, string reference ) {
			Model.Locator locator = selfAsITopicMapRepository.GetLocator(notation, reference);
			if(locator == null) {
				locator = new Locator();
				locator.Address = reference;
				locator.Notation = notation;
				ActiveRecordMediator<Model.Locator>.Create(locator);
			}
			return locator;
		}

		void ITopicMapRepository.AddSubjectIdentifier(Topic topic, Locator locator) {
		
			locator = selfAsITopicMapRepository.CreateLocator(locator.Notation, locator.Address);
			topic.SubjectIdentifiers.Add(locator);
			ensureReifiersAddSubjectIdentifier(topic, locator);
			selfAsITopicMapRepository.Save(topic);
		}

		Topic ITopicMapRepository.GetReifier<TReified>(TReified reified) {
			using (IDisposableLoggerScope scope = new DisposableLoggerXmlScope("topicmaprepository", "GetReifier", logger)) {
				SimpleQuery<TopicMapObject> q = new SimpleQuery<TopicMapObject>(
					"from TopicMapObject reified where reified.Id = :reifiedId");
				q.SetParameter("reifiedId", ((ReifiedObject)reified).Id);
				TopicMapObject[] r = q.Execute();
				if (r.Length > 0)
					return ((ReifiedObject)r[0]).Reifier;
				return null;
			}
		}

		void ITopicMapRepository.AddSourceLocator(TopicMapObject topicMapObject, Locator locator) {
			topicMapObject.SourceLocators.Add(locator);
			if (topicMapObject.SourceLocators.Count == 1) {
				topicMapObject.Id = locator.Address;

				/*if (locator.Address.StartsWith("#")) {
					id = locator.Address.Substring(1);
				}
				else {
					string tmid = null;
					if (topicMapObject is TopicMap) tmid = topicMapObject.Id; 
					else tmid = topicMapObject.TopicMap.BaseLocator.Address;
					string baselocatorrefuri = String.Format( "{0}{1}",
						tmid
						,"#");
					if (locator.Address.StartsWith(baselocatorrefuri)) {
						topicMapObject.Id = locator.Address.Substring(baselocatorrefuri.Length);
					}
				}*/
			}
			if(topicMapObject is ReifiedObject)
				ensureReifierAddSourceLocator(((ReifiedObject)topicMapObject), locator);
			if (topicMapObject is Topic)
				ensureMergingTopicAddSourceLocator((Topic)topicMapObject, locator);
			selfAsITopicMapRepository.Save(topicMapObject);
		}

		private void ensureMergingTopicAddSourceLocator(Topic topic, Locator locator) {
			string query = "from Topic t where :locator in elements(t.SourceLocators)";
			SimpleQuery<Topic> q = new SimpleQuery<Topic>(query);
			q.SetParameter("locator", locator);
			// todo: optimize by specifying topicmapkey
			Topic[] topics = q.Execute();
			if (topics.Length > 0) {
				foreach (Topic t in topics) {
					if (t.Id != topic.Id) {
						Topic merged = topic;
						mergeTopicIn(t, merged);
					}
				}
			}
		}

		private void mergeTopicIn(Topic t, Topic merged) {
			string[] q = {
				
				// topicnames/variants
				"update topicnames set topic_key = :topicKey where topic_key = :mergedKey",
				"update topicnames set type_key = :topicKey where type_key = :mergedKey",
				"update topicnames set reifier_key = :topicKey where reifier_key = :mergedKey",
				"update variants set reifier_key = :topicKey where reifier_key = :mergedKey",
				"update occurrences set topic_key = :topicKey where topic_key = :mergedKey",
				"update occurrences set type_key = :topicKey where type_key = :mergedKey",
				"update occurrences set reifier_key = :topicKey where reifier_key = :mergedKey",
				"update associationroles set player_key = :topicKey where player_key = :mergedKey",
				"update associationroles set type_key = :topicKey where type_key = :mergedKey",
				"update associationroles set reifier_key = :topicKey where reifier_key = :mergedKey",
				"update associations set type_key = :topicKey where type_key = :mergedKey",
				"update associations set reifier_key = :topicKey where reifier_key = :mergedKey",
				
				// !!! this hacky from theses
				"update topic_types set topic_key = :topicKey where topic_key = :mergedKey and type_key != :topicKey",
				"delete from topic_types where topic_key = :mergedKey",
				"update topic_types set type_key = :topicKey where topic_key = :mergedKey and type_key != :topicKey",
				// need to sort out sourcelocators/SubjectIdentifiers/SubjectLocators
				// !!! need to optimize out subselects
				
				};

				// associations/roles

			
		}

		#endregion

		void ensureReifiersAddSubjectIdentifier(Topic reifier, Locator subjectIdentifier) {
			ensureReifiersAddSubjectIdentifier<TopicMap>(reifier, subjectIdentifier);
			ensureReifiersAddSubjectIdentifier<Association>(reifier, subjectIdentifier);
			ensureReifiersAddSubjectIdentifier<AssociationRole>(reifier, subjectIdentifier);
			ensureReifiersAddSubjectIdentifier<Occurrence>(reifier, subjectIdentifier);
			ensureReifiersAddSubjectIdentifier<TopicName>(reifier, subjectIdentifier);
			ensureReifiersAddSubjectIdentifier<Variant>(reifier, subjectIdentifier);

		}

		void ensureReifierAddSourceLocator(ReifiedObject reifiedObject, Locator sourceLocator) {

			if (reifiedObject.Reifier == null) {
				string query = "from Topic t where :locator in elements(t.SubjectIdentifiers) and t.TopicMap = :topicMap";
				SimpleQuery<Topic> q = new SimpleQuery<Topic>(query);
				q.SetParameter("locator", sourceLocator);
				q.SetParameter("topicMap", reifiedObject.TopicMap);
				Topic[] t = q.Execute();
				if (t.Length > 0) {
					reifiedObject.Reifier = t[0];
					selfAsITopicMapRepository.Save(reifiedObject);
				}
			}
		}
		void ensureReifiersAddSubjectIdentifier<TReified>(Topic reifier, Locator subjectIdentifier) where TReified :TopicMapObject, IReified {
			
			/*
			object result = ActiveRecordMediator.Execute(typeof(TReified), 
				delegate(ISession session, object instance) {
					//select o.Key, o.Class 
					IQuery query =
						session.CreateQuery(
							String.Format(@"
update {0} reified 
set reified.Reifier = :reifier 
where reifier.Key in (
	select Key from {0} o, o.SourceLocators l, Topic reifier
	where '#' + l.Address in (reifier.SubjectIdentifiers.Address)
	and l in elemenst(o.SourceLocators)
	and o.Reifier is null
	and o.TopicMap.Key = :topicMapKey
)",
					typeof(TReified))
					)
					.SetParameter("reifier", reifier).SetParameter("topicMapKey",
									   reifier.TopicMap.Key);

					object o = query.();
					return o;
				}
			, null);
			//return (TReified)result;









			*/


			SimpleQuery<TReified> q = new SimpleQuery<TReified>(
				String.Format(@"select o from {0} o where :locator in elements(o.SourceLocators)", typeof(TReified)));
			q.SetParameter("locator", subjectIdentifier);
			IEnumerable<TReified> result = q.Execute();
			foreach (TReified reified in result) {
				if (reified.Reifier == null) {
					reified.Reifier = reifier;
					ActiveRecordMediator<TReified>.Save(reified);
				}
			}


			
			/*

			ScalarQuery<TReified> q = new ScalarQuery<TReified>(typeof(TReified),
				
			String.Format(@"
update {0} reified 
set reified.Reifier = :reifier 
where reifier.Key in (
	select Key from {0} o, o.SourceLocators l, Topic reifier
	where l in elements(reifier.SubjectIdentifiers)
	and l in elemenst(o.SourceLocators)
	and o.Reifier is null
)", typeof(TReified)));
			q. SetParameter("reifier", reifier);
			try {
				
			}
			catch(Exception e) {
				logger.Error(
					string.Format("error while setting reifier by adding subject identifier to topic {0}", reifier.Id), e);
			}
			//ActiveRecordMediator<TReified>.(q);
		
			*/



			
		} 
		void ensureReifierAddSubjectIdentifier<TReified>(TReified reified, Locator subjectIdentifier) where TReified : TopicMapObject, IReified  { 
			SimpleQuery<Topic> q = new SimpleQuery<Topic>(String.Format(@"
select t
from Locator l, Topic t, {0} reified
where l.Key = :subjectIdentifierKey
and reified.Key = :reifiedKey
and l in elements(t.SubjectIdentifiers)
and l in elements(reified.SourceLocators)
", typeof(TReified).Name));
			q.SetParameter("subjectIdentifierKey", subjectIdentifier.Key);
			q.SetParameter("reifiedKey", reified.Key);
			Topic[] reifiers = (Topic[]) ActiveRecordMediator<Topic>.ExecuteQuery(q);


			if(reifiers.Length > 0 ) {
				if (reified.Reifier == null) {
					reified.Reifier = reifiers[0];
					selfAsITopicMapRepository.Save(reified);
				}
			}
		}

		void ensureTopicMapReifier(Locator subjectIdentifier, TopicMap topicMap) {
			SimpleQuery<Topic> q =
				new SimpleQuery<Topic>(
@"select t 
from Locator l, 
TopicMap m,
Topic t
where m.Key = :topicMapKey
and t.TopicMap.Key = :topicMapKey
and l in elements(m.SourceLocators)
and l in elements(t.SubjectIdentifiers"
			);
			q.SetParameter("topicMapKey", topicMap.Key);
			Topic[] topics = q.Execute();
			if (topics.Length > 0) {
				if (topics[0] != topicMap.Reifier) {
					topicMap.Reifier = topics[0];
					selfAsITopicMapRepository.Save(topicMap);
				}
			}
		}


		Locator ensureLocator(string notation, string address) {

			Model.Locator locator = selfAsITopicMapRepository.GetLocator(notation, address);
			if (locator == null) {
				locator = new Model.Locator();
				locator.Address = address;
				locator.Notation = notation;
				ActiveRecordMediator<Model.Locator>.Save(locator);
			}
			return locator;
		}

		private void removeTopicMapObjects(TopicMap topicMap) {
			ISessionFactoryHolder sessionHolder = ActiveRecordMediator.GetSessionFactoryHolder();
			using (ISession session = sessionHolder.CreateSession(typeof(TopicTypesRelation))) {
				string[] queries = { "delete from topicmapobjects where topicmap_key = :topicMapKey" 
				                   };
				foreach (string query in queries) {
					ISQLQuery q = session.CreateSQLQuery(query);
					q.SetBinary("topicMapKey", topicMap.Key.Value.ToByteArray());
					q.AddEntity(typeof(TopicTypesRelation));
					try {
						object o = q.List();
					}
					catch {
#if RELEASE
					throw;
#endif
					}
				}
			}
		}

		private string generateObjectId() {
			return Guid.NewGuid().ToString("N");
		}

		private void createDefaultSourceLocator(Model.TopicMapObject topicMapObject) {
			string uri = topicMapObject.Id;
			Model.Locator defaultsourcelocator = ensureLocator("URI", uri);
			topicMapObject.SourceLocators.Add(defaultsourcelocator);
			selfAsITopicMapRepository.Save(topicMapObject);
		}
	}

}