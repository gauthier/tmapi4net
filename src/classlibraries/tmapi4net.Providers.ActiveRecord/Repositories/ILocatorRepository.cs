namespace tmapi4net.Providers.ActiveRecord.Repositories {
	using System.Collections.Generic;

	interface ILocatorRepository {
		IEnumerable<Model.Locator> GetTopicMapBaseLocators();
		Model.Locator CreateLocator(string notation, string address);
		Model.Locator GetLocator(string notation, string address);
	}
}