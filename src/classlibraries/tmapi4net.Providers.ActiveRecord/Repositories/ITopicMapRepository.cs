namespace tmapi4net.Providers.ActiveRecord.Repositories {
	using System.Collections.Generic;
	using tmapi4net.Core;
	using tmapi4net.Providers.ActiveRecord.Model;
	using AssociationRole=tmapi4net.Providers.ActiveRecord.Implementations.AssociationRole;
	using TopicName=tmapi4net.Providers.ActiveRecord.Implementations.TopicName;


	interface ITopicMapRepository : ILocatorRepository {
		Model.TopicMap CreateTopicMap(string baseLocator);
		Model.TopicMap GetTopicMap(Model.Locator baseLocator);
		void RemoveTopicMap(Model.TopicMap topicMap);

		Model.Topic CreateTopic(Model.TopicMap topicMap);

		Model.TopicName CreateTopicName(Model.Topic topic, string topicName, IList<Model.Topic> scope);
		Model.TopicName CreateTopicName(Model.Topic topic, string topicName, Model.Topic type, IList<Model.Topic> scope);

		Model.Association CreateAssociation(Model.TopicMap topicMap);
		Model.AssociationRole CreateAssociationRole(Association association, Topic player, Topic roleSpec);
		void RemoveAssociationRole(Model.AssociationRole associationRole);
		void AddScopingTopic(Model.ScopedObject scopedObject, Model.Topic topic);
		void RemoveScopingTopic(Model.ScopedObject scopedObject, Model.Topic topic);
		void RemoveTopicName(Model.TopicName topicName);

		Occurrence CreateOccurrence(Topic topic, Locator resource, string occurrenceData, Topic occurrenceType, TopicSet occurrenceScope); 
		void RemoveOccurrence(Occurrence occurrence);
		Variant CreateVariant(Model.TopicName topicName, Model.Locator resource, string resourceData, TopicSet scope);
		void RemoveVariant(Variant variant);
		IEnumerable<Association> GetAllAssociations(TopicMap topicMap);
		void Save(TopicMapObject topic);
		void RemoveAssociation(Model.Association association);
		TopicMapObject GetObjectById(Model.TopicMap topicMap, string objectId);
		void RemoveTopic(Model.Topic topic);
		IEnumerable<Topic> GetAllTopics(TopicMap topicMap);
		IEnumerable<TopicMapObject> GetTopicMapObjectsReifiedBy(Topic topic);
		Topic GetReifier<TReified>(TReified reified) where TReified : ReifiedObject;
		void AddTypeToTopic(Topic topic, Topic type);
		void RemoveTypeToTopic(Topic topic, Topic type);
		void AddSubjectIdentifier(Topic topic, Locator locator);
		void AddSourceLocator(TopicMapObject topicMapObject, Locator locator);
	}
}