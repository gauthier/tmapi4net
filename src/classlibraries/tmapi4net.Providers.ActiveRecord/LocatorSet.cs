namespace tmapi4net.Providers.ActiveRecord.Implementations {
	using System.Collections.Generic;
	using Iesi.Collections.Generic;
	using tmapi4net.Core;


	class ReadOnlySet<T> : SortedSet<T>, tmapi4net.Core.Collections.IReadOnlySet<T> {

		IDictionary<int, T> itemsByIndex = new Dictionary<int, T>();
		


		#region ISet<T> Members

		T tmapi4net.Core.Collections.IReadOnlySet<T>.this[int index] {
			get {
				return itemsByIndex[index];
			}
		}

		public override bool Add(T o) {
			itemsByIndex.Add(itemsByIndex.Count, o);
			return base.Add(o);
		}

		public override bool AddAll(ICollection<T> c) {
			foreach (T t in c) {
				itemsByIndex.Add(itemsByIndex.Count, t);
			}
			return base.AddAll(c);
		}

		#endregion
		
	}
	
	class LocatorSet :ReadOnlySet<ILocator> {
		
		public LocatorSet(IEnumerable<Model.Locator> locatorRecords) {
			foreach (Model.Locator locatorrecord in locatorRecords) {
				this.Add(new Locator(locatorrecord));
			}
		}


	
	}
}