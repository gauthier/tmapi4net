namespace tmapi4net.Providers.ActiveRecord {
	using System;
	using System.Collections;
	using Castle.ActiveRecord;
	using Castle.ActiveRecord.Framework;
//	using Castle.Core.Configuration;
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;
	using tmapi4net.Providers.ActiveRecord.Repositories;
	//using LocatorSet=tmapi4net.Providers.ActiveRecord.Implementations.LocatorSet;
	using System.Collections.Generic;

	class TopicMapSystem : TopicMapSystemBase, ITopicMapSystem {
		private ITopicMapRepository topicMapRepository_;
		private static bool arstarted;
		static TopicMapSystem() {
fixedFeatures_ = new Dictionary<string, bool>();
			defaultFeatures_ = new Dictionary<string, bool>();
			defaultFeatures_.Add("http://tmapi.org/features/model/xtm1.1", true);
			defaultFeatures_.Add("http://tmapi.org/features/automerge", true);
			defaultFeatures_.Add("http://tmapi.org/features/readOnly", false);


			//IConfigurationSource config = Castle.ActiveRecord.Framework.Config.ActiveRecordSectionHandler.Instance;
			
			/*if(!arstarted) {
				ActiveRecordStarter.Initialize(config);
				arstarted = true;

				ActiveRecordStarter.RegisterTypes(
				typeof (Model.Locator),
				typeof (Model.TopicMapObject),
				typeof (Model.TopicMap),
				typeof (Model.Topic),
				typeof (Model.TopicName),
				typeof (Model.Association),
				typeof (Model.AssociationRole),
				typeof(Model.Occurrence),
				typeof(Model.Variant), 
				typeof(Model.TopicTypesRelation)
				);
			}*/
		}

		internal static readonly IDictionary<string, bool> defaultFeatures_;

		internal static readonly IDictionary<string, bool> fixedFeatures_;
		public TopicMapSystem() : base(defaultFeatures_) {
			
			
			//locatorRepository_ = new LocatorRepository();

			topicMapRepository_ = new TopicMapRepository();

		}
		#region ITopicMapSystem Members

		ITopicMap ITopicMapSystem.CreateTopicMap(string baseLocatorAddress, string baseLocatorNotation) {
			throw new Exception("The method or operation is not implemented.");
		}

		ITopicMap ITopicMapSystem.CreateTopicMap(string baseLocatorAddress) {
			return new Implementations.TopicMap(this, topicMapRepository_.CreateTopicMap(baseLocatorAddress));
		}

		ITopicMap ITopicMapSystem.GetTopicMap(string baseLocatorAddress, string baseLocatorNotation) {
			Model.Locator locator = topicMapRepository_.GetLocator(baseLocatorNotation, baseLocatorAddress);
			return selfAsITopicMapSystem.GetTopicMap(new Implementations.Locator(locator));
		}

		ITopicMap ITopicMapSystem.GetTopicMap(string baseLocatorAddress) {

			return selfAsITopicMapSystem.GetTopicMap(baseLocatorAddress, "URI");
		}

		ITopicMap ITopicMapSystem.GetTopicMap(ILocator baseLocator) {
			Model.Locator locator = topicMapRepository_.GetLocator(baseLocator.Notation, baseLocator.Reference);
			if(locator == null)
				return null;
			Model.TopicMap map = topicMapRepository_.GetTopicMap(locator);
			if(map == null)
				return null;
			return new  Implementations.TopicMap(this, map );
		}

		IReadOnlySet<ILocator> ITopicMapSystem.BaseLocators {
			get {
				return new Implementations.LocatorSet(topicMapRepository_.GetTopicMapBaseLocators());
			}
		}

		string ITopicMapSystem.GetProperty(string propertyName) {
			throw new Exception("The method or operation is not implemented.");
		}

		void ITopicMapSystem.Close() {
			
		}

		#endregion

		ITopicMapSystem selfAsITopicMapSystem { get { return this; } }
	}
}