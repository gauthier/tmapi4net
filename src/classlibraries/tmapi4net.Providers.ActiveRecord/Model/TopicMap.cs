namespace tmapi4net.Providers.ActiveRecord.Model {
	using System;
	using System.Collections.Generic;
	using Castle.ActiveRecord;

	[ActiveRecord(Table = "topicmaps")]
	public class TopicMap : ReifiedObject, IReified {
		/*
		[BelongsTo("reifier_key")]
		public override Topic Reifier {
			get {
				return base.Reifier;
			}
			set {
				base.Reifier = value;
			}
		}*/

		[JoinedKey(SqlColumnName.key)]
		public override Guid? Key {
			get {
				return base.Key;
			}
			set {
				base.Key = value;
			}
		}


		private Locator baseLocator_;
		[BelongsTo("locator_key")]
		public virtual Locator BaseLocator {
			get { return baseLocator_; }
			set { baseLocator_ = value; }
		}

		public override string Class {
			get { return "TopicMap"; }
		}



	}
}