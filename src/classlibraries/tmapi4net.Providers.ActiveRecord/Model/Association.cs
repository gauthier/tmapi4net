namespace tmapi4net.Providers.ActiveRecord.Model {
	using System;
	using System.Collections.Generic;
	using Castle.ActiveRecord;

	[ActiveRecord("associations")]
	public class Association : ScopedObject {
		
		public Association(){}
		internal Association(TopicMap topicMap) : base(topicMap){}
		/*[BelongsTo("reifier_key")]
		public override Topic Reifier {
			get {
				return base.Reifier;
			}
			set {
				base.Reifier = value;
			}
		}*/


		[JoinedKey(SqlColumnName.key)]
		public override Guid? Key {
			get {
				return base.Key;
			}
			set {
				base.Key = value;
			}
		}

		public override string Class {
			get { return "Association"; }
		}

	
		private Topic type_;
		[BelongsTo("type_key")]
		public virtual Topic Type {
			get { return type_; }
			set { type_ = value; }
		}

		private IList<AssociationRole> roles_ = new List<AssociationRole>();
		[HasAndBelongsToMany(MapType = typeof(AssociationRole),
		   ColumnKey = "association_key", ColumnRef = SqlColumnName.key
			, Table = "associationroles")]
		public virtual IList<AssociationRole> Roles {
			get { return roles_;}
			set{ roles_ = value;}
		}




		[HasAndBelongsToMany(MapType= typeof(Topic), ColumnKey = "association_key", ColumnRef = "topic_key", Table = "association_scopes")] 
		public override IList<Topic> Scope {
			get {
				return base.Scope;
			}
			set {
				base.Scope = value;
			}
		}
	}
}