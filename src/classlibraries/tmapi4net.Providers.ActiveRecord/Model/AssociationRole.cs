namespace tmapi4net.Providers.ActiveRecord.Model {
	using System;
	using Castle.ActiveRecord;

	[ActiveRecord("associationroles")]
	public class AssociationRole : ReifiedObject, IReified {


		public AssociationRole(){}
		internal AssociationRole(TopicMap topicMap) : base(topicMap) { }

		[JoinedKey(SqlColumnName.key)]
		public override Guid? Key {
			get {
				return base.Key;
			}
			set {
				base.Key = value;
			}
		}
		public override string Class {
			get { return "AssociationRole"; }
		}


		private Topic type_;
		[BelongsTo("type_key")]
		public virtual Topic Type {
			get { return type_; }
			set { type_ = value; }
		}

		private Topic player_;
		[BelongsTo("player_key")]
		public virtual Topic Player {
			get { return player_; }
			set { player_ = value; }
		}
		internal Association association_;
		[BelongsTo("association_key")]
		public virtual Association Association {
			get { return association_; }
			set{ association_ = value;}
		}
		/*
		[BelongsTo("reifier_key")]
		public override Topic Reifier {
			get {
				return base.Reifier;
			}
			set {
				base.Reifier = value;
			}
		}*/

	}
}