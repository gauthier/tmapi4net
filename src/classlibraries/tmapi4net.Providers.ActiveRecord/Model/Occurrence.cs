namespace tmapi4net.Providers.ActiveRecord.Model {
	using System;
	using System.Collections.Generic;
	using Castle.ActiveRecord;

	[ActiveRecord("occurrences")]
	public class Occurrence : ScopedObject {
		public Occurrence(){}
		
		public Occurrence(Topic topic) : base(topic.TopicMap) {
			topic_ = topic;
		}
		public override string Class {
			get { return "Occurrence"; }
		}

		[JoinedKey(SqlColumnName.key)]
		public override Guid? Key {
			get {
				return base.Key;
			}
			set {
				base.Key = value;
			}
		}

		
		private Locator locator_;

		private string value_;

		[BelongsTo("locator_key")]
		public virtual Locator Locator {
			get { return locator_; }
			set { locator_ = value; }
		}


		private Topic type_;
		[BelongsTo("type_key")]
		public virtual Topic Type {
			get { return type_; }
			set { type_ = value; }
		}


		[Property("value")]
		public virtual string Value {
			get { return value_; }
			set { value_ = value; }
		}

		[BelongsTo("topic_key")]
		public virtual Topic Topic {
			get { return topic_; }
			set { topic_ = value; }
		}


		private Topic topic_;



		[HasAndBelongsToMany(
			MapType = typeof(Topic),
			Table = "occurrence_scopes",
			ColumnKey = "occurrence_key",
			ColumnRef = "topic_key")]

		public override IList<Topic> Scope {
			get { return base.Scope; }
			set { base.Scope = value; }
		}
		/*
		[BelongsTo("reifier_key")]
		public override Topic Reifier {
			get {
				return base.Reifier;
			}
			set {
				base.Reifier = value;
			}
		}
		*/
	}
}