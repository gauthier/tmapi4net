namespace tmapi4net.Providers.ActiveRecord.Model {
	using System;
	using Castle.ActiveRecord;
	using System.Collections.Generic;

	[ActiveRecord("topicnames")]
	public class TopicName : ScopedObject {
		public TopicName() { }
		public TopicName(Topic topic)
			: base(topic.TopicMap) {
			topic_ = topic;
		}

		[JoinedKey(SqlColumnName.key)]
		public override Guid? Key {
			get {
				return base.Key;
			}
			set {
				base.Key = value;
			}
		}
		public override string Class {
			get { return "TopicName"; }

		}


		private Topic type_;
		[BelongsTo("type_key")]
		public virtual Topic Type {
			get { return type_; }
			set { type_ = value; }
		}

		private string value_;
		[Property("value")]
		public virtual string Value {
			get { return value_; }
			set { value_ = value; }

		}


		private Topic topic_;
		[BelongsTo("topic_key")]
		public virtual Topic Topic {
			get { return topic_; }
			set { topic_ = value; }

		}


		private IList<Topic> scope_;
		[HasAndBelongsToMany(
			MapType = typeof(Topic),
			Table = "topicname_scopes",
			ColumnKey = "topicname_key",
			ColumnRef = "topic_key")]

		public override IList<Topic> Scope {
			get { return base.Scope; }
			set { base.Scope = value; }
		}


		private IList<Variant> variants_ = new List<Variant>();
		[HasAndBelongsToMany(
			MapType = typeof(Variant),
			Table = "variants",
			ColumnKey = "topicname_key",
		   ColumnRef = SqlColumnName.key)]

		public virtual IList<Variant> Variants {
			get { return variants_; }
			set { variants_ = value; }
		}
		/*
		[BelongsTo("reifier_key")]
		public override Topic Reifier {
			get {
				return base.Reifier;
			}
			set {
				base.Reifier = value;
			}
		}*/
	}
}