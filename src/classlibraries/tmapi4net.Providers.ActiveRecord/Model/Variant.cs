namespace tmapi4net.Providers.ActiveRecord.Model {
	using System;
	using System.Collections.Generic;
	using Castle.ActiveRecord;

	[ActiveRecord("variants")]
	public class Variant : ScopedObject {
		public Variant(){}
		public Variant(TopicName topicName) : base(topicName.TopicMap){}


		[JoinedKey(SqlColumnName.key)]
		public override Guid? Key {
			get {
				return base.Key;
			}
			set {
				base.Key = value;
			}
		}

		private Locator locator_;

		[BelongsTo("locator_key")]
		public virtual Locator Locator {
			get { return locator_; }
			set { locator_ = value; }
		}

		private TopicName topicName_;
		[BelongsTo("topicname_key")]
		public virtual TopicName TopicName {
			get { return topicName_; }
			set{ topicName_ = value;}
		}


		private string value_;
		[Property("value")]
		public string Value {
			get { return value_; }
			set{ value_ = value;}
		}

		public override string Class {
			get { return "Variant"; }
		}

		[HasAndBelongsToMany(
			MapType = typeof(Topic),
			Table = "variant_scopes",
			ColumnKey = "variant_key",
			ColumnRef = "topic_key")]

		public override IList<Topic> Scope {
			get { return base.Scope; }
			set { base.Scope = value; }
		}
		/*
		[BelongsTo("reifier_key")]
		public override Topic Reifier {
			get {
				return base.Reifier;
			}
			set {
				base.Reifier = value;
			}
		}
		*/
	}
}