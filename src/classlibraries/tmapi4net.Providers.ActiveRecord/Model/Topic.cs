namespace tmapi4net.Providers.ActiveRecord.Model {
	using System;
	using System.Collections.Generic;
	using Castle.ActiveRecord;
	using Iesi.Collections.Generic;


	class TopicSet : List<Topic> {
		public TopicSet(){}
		public TopicSet(IEnumerable<Model.Topic> topics) {
			
			foreach (Model.Topic topic in topics) {
				this.Add(topic);
			}
			

		}


	}

	public static class SqlColumnName {
		public const string key = "key";
	}


	[ActiveRecord(Table="topics")]
	public class Topic : TopicMapObject {

		public Topic() {}
		internal Topic(TopicMap topicMap):base(topicMap) {
			
		}

		[JoinedKey(SqlColumnName.key)]
		public override Guid? Key {
			get {
				return base.Key;
			}
			set {
				base.Key = value;
			}
		}

		public override string Class {
			get { return "Topic"; }
		}


		private ISet<Topic> types_ = new HashedSet<Topic>();

		[HasAndBelongsToMany(
			typeof(Topic), 
			Table="topic_types",
			ColumnKey = "topic_key",
			ColumnRef ="type_key",
			Cascade = ManyRelationCascadeEnum.AllDeleteOrphan,
			Inverse = true
			)
		]
		public virtual ISet<Topic> Types {
			get { return types_; }
			set { types_ = value;}
		}


		private IList<TopicName> topicNames_=  new List<TopicName>();

		[HasAndBelongsToMany(
			MapType = typeof(TopicName),
			Table = "topicnames",
			ColumnKey="topic_key", 
			ColumnRef= SqlColumnName.key,
		   Cascade = ManyRelationCascadeEnum.Delete,
		   Inverse = true)
		]
		public virtual IList<TopicName> TopicNames {
			get { return topicNames_;}
			set{ topicNames_ = value;}

		}

		private IList<Occurrence> occurrences_ = new List<Occurrence>();
		[HasAndBelongsToMany(
			MapType =  typeof(Occurrence),
			Table = "occurrences",
		   ColumnKey = "topic_key", 
			ColumnRef = SqlColumnName.key,
			Cascade = ManyRelationCascadeEnum.Delete
		   
			)]
		public virtual IList<Occurrence> Occurrences {
			get { return occurrences_; }
			set { occurrences_ = value;}

		}

		[HasAndBelongsToMany(
			MapType = typeof(Locator),
			Table = "subjectidentifiers",
			ColumnKey = "topic_key",
			ColumnRef = "locator_key",
			RelationType = RelationType.Set,
		   Cascade = ManyRelationCascadeEnum.Delete
		   )]
		public virtual Iesi.Collections.Generic.ISet<Locator> SubjectIdentifiers {
			get { return subjectIdentifiers_; }
			set { subjectIdentifiers_ = value; }
		}

		[HasAndBelongsToMany(
			MapType = typeof(Locator),
			Table = "subjectlocators",
			ColumnKey = "topic_key",
			ColumnRef = "locator_key",
			Cascade = ManyRelationCascadeEnum.Delete
		   )]
		public virtual IList<Locator> SubjectLocators {
			get { return subjectLocators_; }
			set { subjectLocators_ = value; }
		}

		[HasAndBelongsToMany(
			MapType = typeof(AssociationRole),
			Table = "associationroles",
			ColumnKey = "player_key",
		   ColumnRef = SqlColumnName.key,
		   Cascade = ManyRelationCascadeEnum.Delete
			,Sort="type_key"
		   )]
		public virtual IList<AssociationRole> RolesPlayed {
			get { return rolesPlayed_; }
			set { rolesPlayed_ = value; }
		}

		private IList<AssociationRole> rolesPlayed_ = new List<AssociationRole>();


		private IList<Locator> subjectLocators_ = new List<Locator>();

		private Iesi.Collections.Generic.ISet<Locator> subjectIdentifiers_ = new Iesi.Collections.Generic.HashedSet<Locator>();
	}

}