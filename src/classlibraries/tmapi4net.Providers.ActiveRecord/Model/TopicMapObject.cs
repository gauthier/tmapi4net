namespace tmapi4net.Providers.ActiveRecord.Model {
	using System;
	using System.Collections.Generic;
	using Castle.ActiveRecord;
	using tmapi4net.Core;

	interface IReified {
		Topic Reifier { get; set; }
	}

	abstract public class ActiveRecordObject {
	
	}


	[ActiveRecord(Table = "topicmapobjects"),
		JoinedBase()
		]
	abstract public class TopicMapObject : ActiveRecordObject {

		public TopicMapObject(){}

		protected TopicMapObject(TopicMap topicMap) {
			topicMap_ = topicMap;
		}

		private TopicMap topicMap_;
		private string id_;


		[Property("id")]
		public virtual string Id {
			get { return id_; }
			set { id_ = value; }
		}
		private Guid? key_;

		[PrimaryKey(Column = SqlColumnName.key, Generator = PrimaryKeyType.GuidComb), JoinedKey(SqlColumnName.key)]
		public virtual Guid? Key {
			get { return key_; }
			set {
				key_ = value;
			}
		}

		[BelongsTo("topicmap_key")]

		public virtual TopicMap TopicMap {
			get { return topicMap_; }
			set { topicMap_ = value; }
		}

		[Property(Column = "class")]
		public virtual string Class { get { throw new NotImplementedException(); } set{} }

		private Iesi.Collections.Generic.ISet<Locator> sourceLocators_ = new Iesi.Collections.Generic.HashedSet<Locator>();

		[HasAndBelongsToMany(
			MapType = typeof(Locator),
			ColumnKey = "topicmapobject_key",
			ColumnRef = "locator_key",
			Table = "topicmapobject_locators",
			RelationType = RelationType.Set
			)]
		public virtual Iesi.Collections.Generic.ISet<Locator> SourceLocators {
			get { return sourceLocators_; }
			set{ sourceLocators_ = value;}
		}
	}
}