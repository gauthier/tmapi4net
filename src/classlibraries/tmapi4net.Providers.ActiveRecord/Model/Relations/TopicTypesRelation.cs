namespace tmapi4net.Providers.ActiveRecord.Model {
	using System;
	using Castle.ActiveRecord;


	[Serializable]
	public class TopicSubjectIdentifiersKey {
		public TopicSubjectIdentifiersKey() { }
		public TopicSubjectIdentifiersKey(Guid? topicKey, Guid? locatorKey) {
			topicKey_ = topicKey;
			locatorKey_ = locatorKey;
		}
		private Guid? topicKey_;
		private Guid? locatorKey_;

		[KeyProperty(Column = "type_key")]
		public virtual Guid? TypeKey {
			get { return locatorKey_; }
			set { locatorKey_ = value; }
		}

		[KeyProperty(Column = "topic_key")]
		public virtual Guid? TopicKey {
			get { return topicKey_; }
			set { topicKey_ = value; }
		}

		public override int GetHashCode() {
			return topicKey_.GetHashCode() + locatorKey_.GetHashCode();
		}
		public override bool Equals(object obj) {
			return obj.GetHashCode() == GetHashCode();
		}
	}

	[Serializable]
	public class TopicTypesKey {
		public TopicTypesKey() {}
		public TopicTypesKey(Guid? topicKey, Guid? typeKey) {
			topicKey_	= topicKey;
			typeKey_	= typeKey;
		}
		private Guid? topicKey_;
		private Guid? typeKey_;

		[KeyProperty(Column="type_key")]
		public virtual Guid? TypeKey {
			get { return typeKey_; }
			set { typeKey_ = value; }
		}

		[KeyProperty(Column = "topic_key")]
		public virtual Guid? TopicKey {
			get { return topicKey_; }
			set { topicKey_ = value; }
		}

		public override int GetHashCode() {
			return topicKey_.GetHashCode() + typeKey_.GetHashCode();
		}
		public override bool Equals(object obj) {
			return obj.GetHashCode() == GetHashCode();
		}
	}

	/*

	[Serializable]
	public class TopicNameScopingTopicKey {
		public ScopingTopicKey() { }
		public ScopingTopicKey(Guid? scopedObjectKey, Guid? scopingTopicKey) {
			scopedObjectKey_ = scopedObjectKey;
			scopingTopicKey_ = scopingTopicKey;
		}
		private Guid? scopedObjectKey_;
		private Guid? scopingTopicKey_;

		[KeyProperty(Column = "scopedobject_key")]
		public Guid? ScopedObjectKey {
			get { return scopedObjectKey_; }
			set { scopedObjectKey_ = value; }
		}

		[KeyProperty(Column = "topic_key")]
		public Guid? ScopingTopicKey {
			get { return scopingTopicKey_; }
			set { scopingTopicKey_ = value; }
		}

		public override int GetHashCode() {
			return "ScopingTopicKey".GetHashCode() - scopingTopicKey_.GetHashCode() + scopedObjectKey_.GetHashCode();
		}
		public override bool Equals(object obj) {
			return obj.GetHashCode() == GetHashCode();
		}
	}





	[Castle.ActiveRecord.ActiveRecord("topicname_scopes")]
	public class TopicNameScopesRelation {
		private TopicNameScopingTopicKey key_;

		[CompositeKey]
		public TopicNameScopingTopicKey Key {
			get { return key_; }
			set { key_ = value; }
		}
		private Topic topic_;
		private Topic type_;


		[BelongsTo(Column = "topicname_key")]
		public Topic Topic {
			get { return topic_; }
			set { topic_ = value; }
		}

		[BelongsTo(Column = "topic_key")]
		public Topic Type {
			get { return type_; }
			set { type_ = value; }
		}

	}
	*/
	
	
	
	
	
	[Castle.ActiveRecord.ActiveRecord("topic_types")]
	public class TopicTypesRelation {
		private TopicTypesKey key_;
		
		[CompositeKey]
		public TopicTypesKey Key {
			get { return key_; }
			set { key_ = value; }
		}
		

	}

	
	
	[Castle.ActiveRecord.ActiveRecord("subjectidentifiers")]
	public class TopicSubjectIdentifiersRelation {
		private TopicSubjectIdentifiersKey key_;
		
		[CompositeKey]
		public TopicSubjectIdentifiersKey Key {
			get { return key_; }
			set { key_ = value; }
		}
		

	}

}