namespace tmapi4net.Providers.ActiveRecord.Model {
	using System;
	using System.Globalization;
	using Castle.ActiveRecord;

	[ActiveRecord(Table = "locators")]
	public partial class Locator : ActiveRecordObject, IComparable<Locator>, IComparable {
		private Guid? key_;

		[PrimaryKey(Column = SqlColumnName.key, Generator = PrimaryKeyType.GuidComb)]
		public virtual Guid? Key {
			get { return key_; }
			set { key_ = value; }
		}

		[Property("notation")]
		public virtual string Notation {
			get { return notation_; }
			set { notation_ = value; }
		}

		[Property("address")]
		public virtual string Address {
			get { return address_; }
			set { address_ = value; }
		}

		private string address_;
		private string notation_;







		int IComparable<Locator>.CompareTo(Locator other) {
			if (other == null)
				return 1;
			int notationdif = CultureInfo.CurrentCulture.CompareInfo.Compare(Notation, other.Notation);
			if (notationdif != 0) return notationdif;
			return CultureInfo.CurrentCulture.CompareInfo.Compare(Address, other.Address);
		}

	
		int IComparable.CompareTo(object obj) {
			if (obj is Locator) return ((IComparable<Locator>)this).CompareTo((Locator)obj);
			return 1;
		}

	}
}