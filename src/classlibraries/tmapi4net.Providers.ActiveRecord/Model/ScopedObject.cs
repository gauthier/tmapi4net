namespace tmapi4net.Providers.ActiveRecord.Model {
	using System.Collections.Generic;
	using Castle.ActiveRecord;

/*	[ActiveRecord(),
	JoinedBase()
	]*/
	public abstract class ReifiedObject : TopicMapObject, IReified {
		protected ReifiedObject() : base(){}
		protected ReifiedObject (TopicMap topicMap) : base(topicMap) {
			
		}

		private Topic reifier_;
		[BelongsTo("reifier_key")]
		public virtual Topic Reifier {
			get { return reifier_; }
			set { reifier_ = value; }
		}
	}

	public abstract class ScopedObject : ReifiedObject { 
		internal ScopedObject(){}
		protected ScopedObject(TopicMap topicMap)  : base(topicMap){}
		private IList<Topic> scope_= new List<Topic>();

		public virtual IList<Topic> Scope {

			get { return scope_; }
			set{ scope_ = value;}
		}

		
	}
}