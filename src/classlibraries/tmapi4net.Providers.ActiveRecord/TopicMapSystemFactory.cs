using System;
using System.Collections.Generic;
using System.Text;

namespace tmapi4net.Providers.ActiveRecord.Repositories {
}

namespace tmapi4net.Providers.ActiveRecord.Implementations {
}

namespace tmapi4net.Providers.ActiveRecord {
	using Castle.ActiveRecord;
	using NHibernate.Criterion;
	using tmapi4net.Core;
	using System.Collections;
	using System.Collections.Specialized;
	using tmapi4net.Core.Exceptions;
	using tmapi4net.Providers.ActiveRecord.Repositories;


	public class TopicMapSystemFactory : Core.TopicMapSystemFactory, ITopicMapSystemFactory {

		internal readonly IDictionary<string, bool> defaultFeatures_;

		#region ITopicMapSystemFactory Members

		ITopicMapSystem ITopicMapSystemFactory.NewTopicMapSystem() {
			return new TopicMapSystem();
		}

		bool ITopicMapSystemFactory.IsFeatureSupported(string featureName) {
			throw new Exception("The method or operation is not implemented.");
		}

		bool ITopicMapSystemFactory.GetFeature(string featureName) {
			throw new Exception("The method or operation is not implemented.");
		}

		void ITopicMapSystemFactory.SetFeature(string featureName, bool value) {
			if (TopicMapSystem.defaultFeatures_.ContainsKey(featureName)) {
				if (!TopicMapSystem.fixedFeatures_.ContainsKey(featureName)) {
					TopicMapSystem.defaultFeatures_[featureName] = value;
					return;
				}
				else if (TopicMapSystem.fixedFeatures_.ContainsKey(featureName) && TopicMapSystem.fixedFeatures_[featureName] == value)
					return;
				else
					throw FeatureNotSupportedException.CreateWithFeatureNameAndFixedValue(featureName, TopicMapSystem.fixedFeatures_[featureName]);

			}
		}

		string ITopicMapSystemFactory.GetProperty(string propertyName) {
			throw new Exception("The method or operation is not implemented.");
		}

		void ITopicMapSystemFactory.SetProperty(string propertyName, string value) {
			throw new Exception("The method or operation is not implemented.");
		}

		#endregion
	}
}
