namespace tmapi4net.Index.Attributes {
	using System;


	public class MandatoryClassAttribute : Attribute {
		public MandatoryClassAttribute()
		{
			
		}
		private string description_;

		public string Description {
			get { return description_; }
			set { description_ = value; }
		}

		
	}





	public class MandatoryMethodAttribute : Attribute {
		public MandatoryMethodAttribute()
		{
			
		}
		private string description_;

		public string Description {
			get { return description_; }
			set { description_ = value; }
		}
	}
}

namespace tmapi4net.Index {
	using System;
	using tmapi4net.Index.Attributes;


	/// <summary>
	/// Abstract representation of a topic map index.
	/// </summary>
	/// <remarks>
	/// This is the base interface that must be implemented by all indexes.
	/// </remarks>
	public interface IIndex {

		/// <summary>
		/// Open the index.
		/// </summary>
		/// <remarks>
		/// This method must be invoked before using any other method 
		/// exported by this interface or derived interfaces.
		/// </remarks>
		/// <exception cref="TMAPIIndexException"></exception>
		void Open();

		/// <summary>
		/// Close the index.
		/// </summary>
		/// <exception cref="TMAPIIndexException"/>
		void Close();

		/// <summary>
		/// Return the state of this Index.
		/// </summary>
		/// <value>
		/// <c>true</c> if the Index is currently open, <c>false</c> 
		/// otherwise.
		/// </value>
		/// <exception cref="TMAPIIndexException"/>
		bool IsOpen { get; }

		/// <summary>
		/// Resynchronize this Index with the data in the TopicMap
		/// </summary>
		/// <exception cref="TMAPIIndexException"/>
		void Reindex();

		/// <summary>
		/// Retrieve the IndexFlag instance that represents the static meta 
		/// data for this Index.
		/// </summary>
		/// <value>
		/// the IndexFlag instance that represents the static meta data for 
		/// this IIndex.
		/// </value>
		/// <exception cref="TMAPIIndexException"/>
		IIndexFlags Flags { get;}



		/// <summary>
		/// wether this index support a method with given name
		/// </summary>
		/// <remarks>
		/// call with method name flagged with 
		/// <see cref="MandatoryMethodAttribute"/> attribute
		/// should always return true
		/// </remarks>
		/// <param name="MethodName">given method name</param>
		/// <returns></returns>
		bool SupportMethod(string MethodName);

		/// <summary>
		/// wether this index support a method with given name and parameter 
		/// types
		/// </summary>
		/// <remarks>
		/// call with method name flagged with 
		/// <see cref="MandatoryMethodAttribute"/> attribute
		/// and matching parameters should always return true
		/// </remarks>
		/// <param name="MethodName">given method name</param>
		/// <param name="parameterTypes">given parameter types</param>
		/// <returns></returns>
		bool SupportMethod(string MethodName, params Type[] parameterTypes);
	}
}
