namespace tmapi4net.Index {
	public interface IIndexFlags {

		/// <summary>
		/// whether the index is auto updated or not.
		/// </summary>
		bool IsAutoUpdated { get; }
	}
}