namespace tmapi4net.Index.Core {
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;
	using tmapi4net.Index.Attributes;

	/// <summary>
	/// An index of the AssociationRole in the indexed TopicMap.
	/// <remarks>
	/// This index provides methods to retrieve AssociationRole instances
	/// by their type and to retrieve the collection of all Topics
	/// which define the type of one or more AssociationRole instances in the
	/// indexed TopicMap.
	/// </remarks>
	/// </summary>
	[MandatoryClass(Description = "since tmapi 1.0")]
	public interface IAssociationRolesIndex : IIndex {
		/// <summary>
		/// Returns all Topic instances used to type AssociationRole instances in
		/// the indexed TopicMap
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Collection but must never be <c>null</c>.
		/// </remarks>
		/// <returns>
		/// a Collection of Topic instances.
		/// </returns>
		[MandatoryMethod(Description = "since tmapi 1.0")]
		ISet<ITopic> GetAssociationRoleTypes();

		/// <summary>
		/// Returns all AssociationRole instances typed by the Topic
		/// <c>type</c> in the indexed Topic Map
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Collection but must never
		/// be <c>null</c>.
		/// </remarks>
		/// <param name="type">
		/// The type of the AssociationRole instances to be returned.
		/// <remarks>
		/// If type is <c>null</c> a collection containing all untyped 
		/// AssociationRoles will be returned
		/// </remarks>
		/// </param>
		/// <returns>a Collection of AssociationRole instances.</returns>
		[MandatoryMethod(Description = "since tmapi 1.0")]
		ISet<IAssociationRole> GetAssociationRolesByType(ITopic type);
	}
}