namespace tmapi4net.Index.Core {
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;
	using tmapi4net.Index.Attributes;

	/// <summary>
	/// An index of Association instances in a TopicMap.
	/// </summary>
	/// <remarks>
	/// This index provides an index of Associations by their
	/// type and a method for retrieving all Topics which
	/// are used to specify the type of one or more Associations.
	/// </remarks>
	/// 
	[MandatoryClass(Description = "since tmapi 1.0")]
	public interface IAssociationsIndex : IIndex {

		/// <summary>
		/// Retrieve the Topics which are used to type Associations in this TopicMap.
		/// </summary>
		/// <remarks>
		/// The return value may be an empty
		/// Collection but must never be <c>null</c>.
		/// </remarks>
		/// <returns>
		/// a Collection of Topic instances.
		/// </returns>
		[MandatoryMethod(Description = "since tmapi 1.0")]
		ISet<ITopic> GetAssociationTypes();

		/// <summary>
		/// Retrieve the Associations which are typed by the Topic <c>type</c>.
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Collection but must never be <c>null</c>.
		/// </remarks>
		/// <param name="type">
		/// The type of Associations to be returned
		/// <remarks>
		/// If type is <c>null</c> a collection containing all untyped 
		/// Association will be returned
		/// </remarks>
		/// </param>
		/// <returns>
		/// a Collection of Association instances.
		/// </returns>
		[MandatoryMethod(Description = "since tmapi 1.0")]
		ISet<IAssociation> GetAssociationsByType(ITopic type);
	}
}