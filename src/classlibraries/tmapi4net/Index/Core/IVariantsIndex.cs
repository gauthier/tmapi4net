namespace tmapi4net.Index.Core {
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;
	using tmapi4net.Index.Attributes;

	/// <summary>
	/// An index of all Variant instances in the TopicMap by their resource 
	/// reference or resource data.
	/// </summary>
	[MandatoryClass(Description = "since tmapi 1.0")]
	public interface IVariantsIndex : IIndex {

		/// <summary>
		/// Returns the Variants in the topic map whose 
		/// resource locator matches <code>loc</code>.
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Collection but must never 
		/// be <c>null</c>.	 	 
		/// </remarks>
		/// <param name="loc">
		/// the Locator used to query the index (must not be <c>null</c>)
		/// </param>
		/// <returns>a Collection of Variant instances.</returns>
		[MandatoryMethod(Description = "since tmapi 1.0")]
		ISet<IVariant> GetVariantsByResource(ILocator loc);

		/// <summary>
		/// Returns the Variants in the topic map whose
		/// value property matches <c>value</c>
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Collection but must 
		/// never be <c>null</c>.	 	 
		/// </remarks>
		/// <param name="value">
		/// value the data string used to query the index 
		/// (must not be <c>null</c>)
		/// </param>
		/// <returns>
		/// a Collection of Variant instances.
		/// </returns>
		[MandatoryMethod(Description = "since tmapi 1.0")]
		ISet<IVariant> GetVariantsByValue(string value);
	}
}