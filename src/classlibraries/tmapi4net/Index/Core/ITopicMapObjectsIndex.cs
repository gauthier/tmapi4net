namespace tmapi4net.Index.Core {
	using tmapi4net.Core;
	using tmapi4net.Index.Attributes;

	/// <summary>
	/// An index of all TopicMapObject instances in the 
	/// TopicMap by the value(s) of their sourceLocator
	/// property.
	/// </summary>
	[MandatoryClass(Description = "since tmapi 1.0")]
	public interface ITopicMapObjectsIndex : IIndex {

		/// <summary>
		/// Returns the ITopicMapObject with the specified locator as one of 
		/// its source locators.
		/// </summary>
		/// <param name="sourceLocator">
		/// the source locator of the object to be returned 
		/// (must not be <c>null</c>)
		/// </param>
		/// <returns>
		/// the TopicMapObject with the specified source locator or <c>null</c> if 
		/// no such object exists in the indexed TopicMap.
		/// </returns>
		[MandatoryMethod(Description = "since tmapi 1.0")]
		ITopicMapObject GetTopicMapObjectBySourceLocator(ILocator sourceLocator);
	}
}