namespace tmapi4net.Index.Core {
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;
	using tmapi4net.Index.Attributes;

	/// <summary>
	/// An index of all Occurrence instances in the TopicMap by their type 
	/// and resource reference or resource data.
	/// </summary>
	[MandatoryClass(Description = "since tmapi 1.0")]
	public interface IOccurrencesIndex : IIndex {

		/// <summary>
		/// Returns the Topics which are used to type Occurrences
		/// in the indexed TopicMap.
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Collection but must never 
		/// be <c>null</c>.	 	 
		/// </remarks>
		/// <returns>
		/// a Collection of Topic instances
		/// </returns>
		[MandatoryMethod(Description = "since tmapi 1.0")]
		ISet<ITopic> GetOccurrenceTypes();

		/// <summary>
		/// Returns the Occurrences typed by the topic <c>type</c>.
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Collection but must never 
		/// be <c>null</c>.
		/// </remarks>
		/// <param name="type">
		/// the type of Occurrences to be returned
		/// <remarks>
		/// If type is <c>null</c> a collection containing all untyped 
		/// Occurrences will be returned.
		/// </remarks>
		/// </param>
		/// <returns>a Collection of Occurrence instances.</returns>
		[MandatoryMethod(Description = "since tmapi 1.0")]
		ISet<IOccurrence> GetOccurrencesByType(ITopic type);

		/// <summary>
		/// Returns the Occurrences in the topic map whose 
		/// resource locator matches <c>loc</c>.
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Collection but must never be <c>null</c>.	 	 
		/// </remarks>
		/// <param name="loc">
		/// the Locator used to query the index (must not be <c>null</c>)
		/// </param>
		/// <returns>
		/// a Collection of Occurrence instances.
		/// </returns>
		[MandatoryMethod(Description = "since tmapi 1.0")]
		ISet<IOccurrence> GetOccurrencesByResource(ILocator loc);

		/// <summary>
		/// Returns the Occurrences in the topic map whose
		/// value property matches <c>value</c>
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Collection but must never
		/// be <c>null</c>.	 	
		/// </remarks>
		/// <param name="value">
		/// value the data string used to query the index 
		/// (must not be <c>null</c>)
		/// </param>
		/// <returns>
		/// a Collection of Occurrence instances.
		/// </returns>
		[MandatoryMethod(Description = "since tmapi 1.0")]
		ISet<IOccurrence> GetOccurrencesByValue(string value);
	}
}