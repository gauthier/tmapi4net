namespace tmapi4net.Index.Core {
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;
	using tmapi4net.Index.Attributes;

	/// <summary>
	/// An index of the Topics in the TopicMap.
	/// <remarks>
	/// <para>
	/// This index provides methods to retrieve all of the
	/// Topics in the TopicMap which are used to define the
	/// type of one or more Topics in the TopicMap.
	/// </para>
	/// <para>
	/// The index
	/// also provides methods to retrieve all Topics with one
	/// or more types and to retrieve Topics by their reified
	/// subject address or subject indicators.
	/// </para>
	/// </remarks>
	/// </summary>
	[MandatoryClass(Description = "since tmapi 1.0")]
	public interface ITopicsIndex : IIndex {

		/// <summary>
		/// Retrieve the Topics that are used as topic types
		/// in the indexed TopicMap.
		/// </summary>
		/// <remarks>The return value may be an empty Collection but
		/// must never be <c>null</c>.</remarks>
		/// <returns>a Collection of Topic instances.</returns>
		[MandatoryMethod(Description = "since tmapi 1.0")]
		IReadOnlySet<ITopic> GetTopicTypes();

		/// <summary>
		/// Retrieve the Topics in the TopicMap that include the
		/// Topic <c>type</c> as one of their types.
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Collection but 
		/// must never be <c>null</c>.
		/// </remarks>
		/// <param name="type">
		/// the type of the Topics to be returned
		/// <remarks>
		/// If type is <c>null</c> a collection containing all untyped 
		/// Topics will be returned.
		/// </remarks>
		/// </param>
		/// <returns>
		/// a Collection of Topic instances.
		/// </returns>
		[MandatoryMethod(Description = "since tmapi 1.0")]
		IReadOnlySet<ITopic> GetTopicsByType(ITopic type);

		/// <summary>
		/// Retrieve the Topics in the TopicMap that include all of
		/// the specified topics amongst their types.
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Collection but must
		/// never be <c>null</c>.
		/// </remarks>
		/// <param name="types">
		/// the types of the Topics to be returned (must not be <c>null</c>)
		/// </param>
		/// <param name="matchAll">
		/// if <c>true</c>, then only those Topic instances
		/// where all of the Topics in <c>types</c> are present 
		/// as type property will be returned (this includes the superset
		/// of the given Topics in the <c>types</c> argument), 
		/// otherwise all Topics where one ore more of the Topics specified in
		/// <c>types</c> are present as type property will be returned.
		/// </param>
		/// <returns>a Collection of Topic instances.</returns>
		[MandatoryMethod(Description = "since tmapi 1.0")]
		IReadOnlySet<ITopic> GetTopicsByTypes(ITopic[] types, bool matchAll);

		/// <summary>
		/// Retrieve the Topic in the TopicMap that has the specified
		/// Locator as its subject Locator.
		/// </summary>
		/// <param name="subjectLocator">
		/// the subject locator of the Topic to retrieve 
		/// (must not be <c>null</c>).
		/// </param>
		/// <returns>
		/// a Topic instance or <c>null</c> if no Topic has the specified
		/// subject locator.
		/// </returns>
		[MandatoryMethod(Description = "since tmapi 1.0")]
		ITopic GetTopicBySubjectLocator(ILocator subjectLocator);

		/// <summary>
		/// Retrieve the Topic in the TopicMap that has the specified
		/// Locator as its subject indicator.
		/// </summary>
		/// <param name="subjectIdentifier">
		/// the subject identifier of the Topic to retrieve 
		/// (must not be <c>null</c>). 
		/// </param>
		/// <returns>
		/// a Topic instance or <c>null</c> if no Topic has the specified 
		/// subject identifier.
		/// </returns>
		[MandatoryMethod(Description = "since tmapi 1.0")]
		ITopic GetTopicBySubjectIdentifier(ILocator subjectIdentifier);


		/// <summary>
		/// List all topics except typing topics
		/// </summary>
		/// <param name="topicMap">
		/// Topic map to search for 
		/// </param>
		/// <returns>all non typing topics</returns>
		IReadOnlySet<ITopic> GetNonTypingTopics(ITopicMap topicMap);

		/// <summary>
		/// List all topics that don't have TopicName
		/// </summary>
		/// <param name="topicMap">
		/// Topic map to search for
		/// </param>
		/// <returns>all unnamed topics</returns>
		IReadOnlySet<ITopic> GetUnnamedTopics(ITopicMap topicMap);
	}
}