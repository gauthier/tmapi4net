namespace tmapi4net.Index.Core {
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;
	using tmapi4net.Index.Attributes;

	/// <summary>
	/// An index of all ScopedObject (Association, TopicName, Occurrence
	/// and Variant) instances in the indexed TopicMap. 
	/// <remarks>
	/// This index provides
	/// methods to retrieve all of the ScopedObject instances in the TopicMap
	/// with a particular Topic or set of Topics in their scope, and to 
	/// retrieve all of the Topics used in the scope of one or more 
	/// ScopedObjects in the TopicMap.
	/// </remarks>
	/// </summary>
	[MandatoryClass(Description = "since tmapi 1.0")]
	public interface IScopedObjectsIndex : IIndex {

		/// <summary>
		/// Returns all Topic instances which are present in the scope of
		/// one or more ScopedObject instances in the indexed TopicMap.
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Collection but must never
		/// be <c>null</c>.
		/// </remarks>
		/// <returns>
		/// an unmodifiable Collection of Topic instances.
		/// </returns>
		[MandatoryMethod(Description = "since tmapi 1.0")]
		IReadOnlySet<ITopic> GetScopingTopics();

		/// <summary>
		/// Returns all ScopedObject instances where the specified Topic
		/// is one of the themes in the scope of the ScopedObject.
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Collection but must never
		///  be <c>null</c>.
		/// </remarks>
		/// <param name="scopingTopic">
		/// the Topic that must be in the scope of each returned ScopedObject.
		/// <remarks>
		/// If scopingTopic is <c>null</c> a collection containing 
		/// all ScopedObjects that have no scope will be returned.
		/// </remarks>
		/// </param>
		/// <returns>
		/// an unmodifiable Collection of ScopedObject instances.
		/// </returns>
		[MandatoryMethod(Description = "since tmapi 1.0")]
		IReadOnlySet<IScopedObject> GetScopedObjectsByScopingTopic(ITopic scopingTopic);

		/// <summary>
		/// Returns all ScopedObject instances where some or all of the
		/// specified Topics are in the scope of the ScopedObject.
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Collection but must never
		/// be <c>null</c>.
		/// </remarks>
		/// <param name="scopingTopics">
		/// scopingTopics the Topics which must be in the scope of each
		/// returned ScopedObject instance (must not be <c>null</c>).
		/// </param>
		/// <param name="matchAll">
		/// if <c>true</c>, then only those ScopedObjects
		/// where all of the Topics in <c>scopingTopics</c>
		/// are present in the scope will be returned, otherwise
		/// all ScopedObjects where one or more of the Topics
		/// specified in <c>scopingTopics</c> are present in 
		/// the scope will be returned.
		/// </param>
		[MandatoryMethod(Description = "since tmapi 1.0")]
		ISet<ITopic> GetScopedObjectsByScopingTopics(ITopic[] scopingTopics, bool matchAll);
	}
}