namespace tmapi4net.Index.Core {
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;
	using tmapi4net.Index.Attributes;

	/// <summary>
	/// This Index gives direct access to all TopicNames.
	/// </summary>
	/// <remarks>
	/// you can get all TopicNames, or a TopicName by value
	/// </remarks>
	[MandatoryClass(Description = "since tmapi 1.0")]
	public interface ITopicNamesIndex : IIndex {

		/// <summary>
		/// Retrieves the TopicNames in the TopicMap which
		/// have a value equal to <c>value</c>
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Collection but 
		/// must never be <c>null</c>.
		/// </remarks>
		/// <param name="value">
		/// value of the TopicNames to be returned (must not be <c>null</c>)
		/// </param>
		/// <returns>
		/// a Collection of TopicName instances.
		/// </returns>
		[MandatoryMethod(Description = "since tmapi 1.0")]
		ISet<ITopicName> GetTopicNamesByValue(string value);

		/// <summary>
		/// Retrieves the topics in the TopicMap which define the
		/// type of one or more TopicNames.
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Collection but must never
		/// be <c>null</c>.
		/// </remarks>
		/// <returns>
		/// a Collection of Topic instances.
		/// <remarks>
		/// If the processor
		/// does not support the XTM 1.1 model, this method must return
		/// an empty Collection.
		/// </remarks>
		/// </returns>
		[MandatoryMethod(Description = "since tmapi 1.0")]
		ISet<ITopic> GetTopicNameTypes();

		/// <summary>
		/// Retrieves the TopicNames in the indexed TopicMap whose
		/// type is defined by the specified Topic.
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Collection but must 
		/// never be <c>null</c>.
		/// </remarks>
		/// <param name="type">
		/// the type of the TopicNames to be retrieved.
		/// <remarks>
		/// If type is <c>null</c> a collection containing all untyped 
		/// TopicNames will be returned
		/// </remarks>
		/// </param>
		/// <returns>
		/// a Collection of TopicNames instances. 
		/// <remarks>
		/// If the processor does not support the XTM 1.1 model, this method
		/// must return an empty Collection.
		/// </remarks>
		/// </returns>
		[MandatoryMethod(Description = "since tmapi 1.0")]
		ISet<ITopicName> GetTopicNamesByType(ITopic type);
	}
}