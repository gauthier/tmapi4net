namespace tmapi4net.Core {
	using System;

	/// <summary>
	/// An abstract representation of any addressing scheme.
	/// </summary>
	/// <remarks>
	/// In this representation, an address (reference) is defined using 
	/// a string and the reference string is specified in a particular notation.
	/// </remarks>
	/// <example>
	/// For example, the majority of network addresses are specified
	/// using the "URI" notation, and so the reference string must
	/// conform to this notation specification.
	/// </example>
	public interface ILocator : IComparable<ILocator>, IComparable  {
		
		/// <summary>
		/// Returns the reference string portion of the locator. 
		/// </summary>
		string Reference { get; }

		/// <summary>
		/// Returns the notation string of the locator.
		/// </summary>
		string Notation { get; }

		/// <summary>
		/// Produces a new <see cref="ILocator"/> object by resolving a
		/// reference string relative to the reference of this 
		/// <see cref="ILocator"/>.
		/// </summary>
		/// <param name="relative">
		/// The relative reference to be resolved.
		/// </param>
		/// <returns>
		/// A <see cref="ILocator"/> object representing the absolute
		/// reference created by resolving <code>relative</code>
		/// relative to the reference of this <see cref="ILocator"/>.
		/// </returns>
		ILocator ResolveRelative(string relative);

		/// <summary>
		/// Returns a string representation of this locator suitable
		/// for output or passing to APIs which will parse the locator
		/// anew.
		/// </summary>
		/// <returns></returns>
		/// <remarks>
		/// Any special character will be escaped using the escaping
		/// conventions of the locator notation.
		/// </remarks>
		string ToExternalForm();

		/// <summary>
		/// Returns <c>true</c> if the other object is equal to this one.
		/// </summary>
		/// <remarks>
		/// Equality must be the result of comparing the references 
		/// and notations of the two locator objects.
		/// If <c>o</c> is not an instance of 
		/// <see cref="ILocator"/> the return value is <c>false</c>.
		/// </remarks>
		/// <param name="o">the object to compare this instance with</param>
		/// <returns>
		/// <code>
		/// (
		/// o is ILocator 
		/// &amp;&amp; 
		/// Reference.Equals(((ILocator)o).Reference) 
		/// &amp;&amp; 
		/// Notation.Equals(((ILocator)o).Notation)
		/// )
		/// </code>
		/// </returns>
		bool Equals(object o);

		/// <summary>
		/// The implementation must satisfy .net requirements that
		/// equal objects should return equal hashcode values.
		/// </summary>
		/// <returns>
		/// Hashcode for this object
		/// </returns>
		int GetHashCode();
	}
}