namespace tmapi4net.Core.Collections {
	using System.Collections.Generic;

	public interface ISet<T> : IReadOnlySet<T> {
		new T this[int index] { get;set; }
	}

	public interface IReadOnlySet<T> : ICollection<T> {
		T this[int index] { get; }
	}

}