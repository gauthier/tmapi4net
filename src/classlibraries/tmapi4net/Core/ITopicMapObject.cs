namespace tmapi4net.Core {
	using System;
	using tmapi4net.Core.Collections;
	using tmapi4net.Core.Exceptions;

	/// <summary>
	/// The base class for all objects in the TMAPI system which 
	/// represent constructs in a topic map.
	/// </summary>
	/// <remarks>
	/// Every <see cref="ITopicMapObject"/> may be created from any number 
	/// of input sources (typically markup in some topic map interchange format),
	/// each of which may have a separate address. In addition, the TMAPI 
	/// <see cref="ITopicMapObject"/> may "wrap" one or more objects in an underlying
	/// topic map processing system.
	/// </remarks>
	public interface ITopicMapObject : System.IEquatable<ITopicMapObject>, IComparable<ITopicMapObject>, IComparable {
		/// <summary>
		/// Returns the topic map to which this object belongs.
		/// </summary>
		/// <remarks>
		/// If the object is an instance of the <see cref="ITopicMap"/> 
		/// interface, then it should return itself.
		/// </remarks>
		/// <value>
		/// The <see cref="ITopicMap"/> object to which this object belongs.
		/// </value>
		ITopicMap TopicMap { get; }

		/// <summary>
		/// Returns the set of source locators for this object.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Source locators are identifiers for the object and can be
		/// freely assigned in order to be used to refer to the object.
		/// Note that on XTM import source locators MUST be assigned that
		/// refer to the XTM elements that gave rise to the object, 
		/// providing the XTM element had an id attribute.
		/// </para>
		/// <para>
		/// The return value may be an empty Set, but is never <c>null</c>.
		/// </para>
		/// </remarks>
		/// <value>An unmodifiable Set of ILocator objects</value>
		IReadOnlySet<ILocator> SourceLocators { get; }

		/// <summary>
		/// Adds a <see cref="ILocator"/> to the set of source locators 
		/// for this object.
		/// </summary>
		/// <remarks>
		/// <para>
		/// It is not allowed to have two <see cref="ITopicMapObject"/>s 
		/// in the same <see cref="ITopicMap"/> with the same source locator.
		/// </para>
		/// <para>
		/// If the two objects are <see cref="ITopic"/> objects, 
		/// then they must be merged.
		/// </para>
		/// <para>
		/// If at least one
		/// of the two objects is not a <see cref="ITopic"/> object, 
		/// a <see cref="DuplicateSourceLocatorException"/>
		/// must be reported.
		/// </para>
		/// </remarks>
		/// <see cref="SourceLocators"/>
		/// <param name="sourceLocator">
		/// sourceLocator the ILocator to be added
		/// </param>
		/// <exception cref="DuplicateSourceLocatorException">
		/// if there is already a <see cref="ITopicMapObject"/> in the same 
		/// <see cref="ITopicMap"/> with this source locator.
		/// </exception>
		void AddSourceLocator(ILocator sourceLocator);

		/// <summary>
		/// Removes a <see cref="ILocator"/> from the set of source locators
		/// for this object.
		/// </summary>
		/// <param name="sourceLocator">
		/// the <see cref="ILocator"/> to be removed.
		/// </param>
		void RemoveSourceLocator(ILocator sourceLocator);

		/// <summary>
		/// Removes the object from its container.
		/// </summary>
		/// <exception cref="TMAPIRuntimeException">
		/// <para>
		/// if the object cannot be removed from the container. Derived 
		/// interfaces may define specific circumstances 
		/// under which a subclass of <c ref="TMAPIException"/>
		/// must be raised.
		/// </para>
		/// <para>
		/// Implementations must not use a <c ref="TMAPIException"/> to
		/// wrap a failure raised in the underlying engine 
		/// - such errors must always be wrapped in a
		/// <c ref="TMAPIRuntimeException"/>.
		/// </para>
		/// </exception>
		void Remove();

		/// <summary>
		/// returns the system-specific identifier as a string
		/// </summary>
		/// <remarks>
		/// Returns a topic map-specific identifier for this object. The
		/// identifier returned must be guaranteed to be unique across
		/// all TopicMapObject instances from the same 
		/// <c ref="ITopicMap"/> instance.
		/// </remarks>
		string ObjectId { get; }

		/// <summary>
		/// Returns true if the other object is equal to this one.
		/// </summary>
		/// <remarks>
		/// Equality must be the result of comparing the object ids of the 
		/// two topic map objects. 
		/// If <c>o</c> is not an instance of
		/// <see cref="ITopicMapObject"/> the return value is false.
		/// </remarks>
		/// <param name="o"></param>
		/// <returns>
		/// (
		/// o is ITopicMapObject 
		/// &amp;&amp;
		/// this.ObjectId.Equals(((ITopicMapObject)o).ObjectId)
		/// )
		/// </returns>
		bool Equals(object o);

		/// <summary>
		/// Returns the hashcode of the topic map object's object id
		/// </summary>
		/// <returns>this.ObjectId.GetHashCode()</returns>
		int GetHashCode();
	}
}