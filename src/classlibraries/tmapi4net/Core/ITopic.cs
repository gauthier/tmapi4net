namespace tmapi4net.Core {
	
	using System.Collections.Generic;
	using tmapi4net.Core.Collections;
	using tmapi4net.Core.Exceptions;

	/// <summary>
	/// Represents the topic map topic construct.
	/// </summary>
	public interface ITopic : ITopicMapObject {

		/// <summary>
		/// Returns the TopicNames defined for this topic.
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Set, but is never <c>null</c>.
		/// </remarks>
		/// <value>
		/// An unmodifiable Set of TopicName objects.
		/// </value>
		IReadOnlySet<ITopicName> TopicNames { get; }

		/// <summary>
		/// Creates a new TopicName for this topic.
		/// </summary>
		/// <param name="topicName">
		/// The string value of the TopicName.
		/// <remarks>
		/// Pass <c>null</c> to create a TopicName with no name value.
		/// </remarks>
		/// </param>
		/// <param name="scope">
		/// A Collection of ITopic objects.
		/// <remarks>
		/// If <c>null</c>, the TopicName will be in the unconstrained scope.
		/// </remarks>
		/// </param>
		/// <returns>
		/// the newly created TopicName instance
		/// </returns>
		/// <exception cref="MergeException">
		/// if the processor detects that a merge is required
		///  and either automerge is disabled or automerge is enabled but the merge
		/// cannot be completed for some other reason. <strong>NOTE:</strong>
		/// This method will not raise this exception if the XTM 1.1 model is 
		/// enabled, as untyped names do not cause merging under XTM 1.1.
		/// </exception>
		ITopicName CreateTopicName(string topicName, IEnumerable<ITopic> scope);

		/// <summary>
		/// OPTIONAL:
		/// Creates a new typed TopicName for this topic.
		/// </summary>
		/// <remarks>
		/// This method reflects the XTM 1.1 model. A processor
		/// which does not implement support for XTM 1.1 (or if the 
		/// feature "http://tmapi.org/features/model/xtm.1.1" is disabled)
		/// MUST throw an <see cref="UnsupportedOperationException"/>.
		/// </remarks>
		/// <param name="topicName">
		/// The string value of the TopicName.
		/// <remarks>
		/// Pass <c>null</c> to create a TopicName with no name value.
		/// </remarks>
		/// </param>
		/// <param name="type">
		/// A ITopic instance or <c>null</c>
		/// </param>
		/// <param name="scope">
		/// A Collection of ITopic objects. 
		/// <remarks>
		/// If <c>null</c>, the TopicName will be in the unconstrained scope.
		/// </remarks>
		/// </param>
		/// <returns>
		/// the newly created TopicName instance
		/// </returns>
		/// <exception cref="UnsupportedOperationException">
		/// if the processor does not support the XTM 1.1 model.
		/// </exception>
		/// <exception cref="MergeException">
		/// if the processor detects that a merge is required 
		/// and either automerge is disabled or automerge is enabled but 
		/// the merge cannot be completed for some other reason.
		/// </exception>
		ITopicName CreateTopicName(string topicName, ITopic type, IEnumerable<ITopic> scope);

		/// <summary>
		/// Creates a new occurrence as a contained object in this topic.
		/// </summary>
		/// <param name="occurrenceData">
		/// The occurrence data.
		/// </param>
		/// <param name="type">
		/// The occurrence type. 
		/// <remarks>
		/// If <c>null</c>, then the occurrence will be untyped.     
		/// </remarks>
		/// </param>
		/// <param name="scope">
		/// A bag of ITopic objects.
		/// <remarks>
		/// If <c>null</c>, the occurrence will be in the unconstrained scope.
		/// </remarks>
		/// </param>
		/// <returns>
		/// the newly created <see cref="IOccurrence"/> instance.
		/// </returns>
		IOccurrence CreateOccurrence(string occurrenceData, ITopic type, IEnumerable<ITopic> scope);

		/// <summary>
		/// Creates a new occurrence as a contained object in this topic.
		/// </summary>
		/// <param name="resource">
		/// The occurrence resource locator.
		/// </param>
		/// <param name="type">
		/// The occurrence type. If <c>null</c>, then the occurrence will be untyped.  
		/// </param>
		/// <param name="scope">
		/// A bag of <see cref="ITopic"/> objects.
		/// <remarks>
		/// If <c>null</c>, the occurrence will be in the unconstrained scope.
		/// </remarks>
		/// </param>
		/// <returns>the newly created IOccurrence instance.</returns>
		IOccurrence CreateOccurrence(ILocator resource, ITopic type, IEnumerable<ITopic> scope);

		/// <summary>
		/// Returns the occurrences defined for this topic.
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Set, but is never <c>null</c>.
		/// </remarks>
		/// <value>
		/// An unmodifiable Set of IOccurrence objects.
		/// </value>
		IReadOnlySet<IOccurrence> Occurrences { get; }

		/// <summary>
		/// Returns ILocator instances which reference the subject-constituting
		/// resources of this topic.
		/// <remarks>
		/// The return value may be an empty Set, but is never <c>null</c>.
		/// </remarks>
		/// </summary>
		/// <value>
		/// An unmodifiable Set of ILocator objects.
		/// </value>
		IReadOnlySet<ILocator> SubjectLocators { get; }

		/// <summary>
		/// Adds the specified locator to the set of locators that reference 
		/// the subject-constituting resources of this topic.
		/// </summary>
		/// <param name="subjectLocator">
		/// The locator of the new subject constituting resource 
		/// for this topic.
		/// </param>
		/// <exception cref="MergeException">
		/// if the processor detects that a merge is required 
		/// and either automerge is disabled or automerge is enabled but 
		/// the merge cannot be completed for some other reason.
		/// </exception>
		/// <exception cref="ModelConstraintException">
		/// if the XTM 1.0 processor model is enabled 
		/// and this <see cref="ITopic"/> already 
		/// has one <see cref="ILocator"/> in the subjectLocator set.
		/// </exception>
		void AddSubjectLocator(ILocator subjectLocator);

		/// <summary>
		/// Removes the specified locator from the set of locators that reference 
		/// the subject-constituting resources of this topic.
		/// </summary>
		/// <param name="subjectLocator">
		/// subjectLocator the ILocator to be removed from the set of
		/// subject-constituting resources for this topic.
		/// </param>
		void RemoveSubjectLocator(ILocator subjectLocator);

		/// <summary>
		/// Returns the Locators which reference the subject-indicating
		/// resources of this topic.
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Set, but is never <c>null</c>.
		/// </remarks>
		/// <value>
		/// An unmodifiable Set of <see cref="ILocator"/> objects.
		/// </value>
		IReadOnlySet<ILocator> SubjectIdentifiers { get; }

		/// <summary>
		/// Adds the specified locator to the set of locators
		/// that reference the subject-indicating resources
		/// of this topic. 
		/// </summary>
		/// <param name="subjectIdentifier">
		/// The locator to be added.
		/// </param>
		/// <exception cref="MergeException">
		/// if the processor detects that a merge is required 
		/// and either automerge is disabled or automerge is enabled 
		/// but the merge cannot be completed for some other reason.
		/// </exception>
		void AddSubjectIdentifier(ILocator subjectIdentifier);

		/// <summary>
		/// Removes the specified locator from the set of locators
		/// that reference the subject-indicating resources of this topic.
		/// </summary>
		/// <param name="subjectIdentifier">
		/// The locator to be removed.
		/// </param>
		void RemoveSubjectIdentifier(ILocator subjectIdentifier);

		/// <summary>
		/// Returns the topics which define the types of this topic.
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Set, but is never <c>null</c>.
		/// </remarks>
		/// <value>An unmodifiable Set of ITopic objects.</value>
		IReadOnlySet<ITopic> Types { get; }

		/// <summary>
		/// Adds a type to this topic.
		/// </summary>
		/// <param name="type">The type-defining topic to be added.</param>
		void AddType(ITopic type);

		/// <summary>
		/// Removes a type from this topic.
		/// </summary>
		/// <param name="type">
		/// The type-defining topic to be removed.
		/// </param>
		void RemoveType(ITopic type);

		/// <summary>
		/// Returns the association roles in which this topic is a player.
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Set, but is never <c>null</c>.
		/// </remarks>
		/// <value>
		/// An unmodifiable Set of AssociationRole objects.
		/// </value>
		IReadOnlySet<IAssociationRole> RolesPlayed { get; }

		/// <summary>
		/// Removes this object from the collection of Topics managed by 
		/// the parent <see cref="ITopicMap"/>. 
		/// </summary>
		/// <remarks>
		/// This method must also remove all contained <see cref="ITopicName"/>
		/// and <see cref="IOccurrence"/> objects.
		/// </remarks>
		/// <exception cref="TopicInUseException">
		/// If this <see cref="ITopic"/> is currently used as a type of 
		/// <list type="">
		/// <item>another topic</item>
		/// <item>occurrence</item>
		/// <item>association</item>
		/// </list>
		/// or if this topic is as association role type
		/// <list type="">
		/// <item>
		/// a member of a scope
		/// </item>
		/// <item>
		/// or a role player in an association.
		/// </item>
		/// </list>
		/// </exception>
		new void Remove();

		/// <summary>
		/// Merges another <see cref="ITopic"/> into this <see cref="ITopic"/>.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Merging a <see cref="ITopic"/> into this <see cref="ITopic"/> 
		/// causes this <see cref="ITopic"/> to gain all of the characteristics 
		/// of the other <see cref="ITopic"/> and to replace the other 
		/// <see cref="ITopic"/> wherever it is used as a typing or scoping 
		/// topic. 
		/// </para>
		/// <para>
		/// After this method completes, <c>other</c> will have 
		/// been removed from the <see cref="ITopicMap"/>.
		/// </para>
		/// <para>
		/// note: 
		/// The <c>other</c> <see cref="ITopic"/> must belong to the same 
		/// <see cref="ITopic"/>Map instance as this <see cref="ITopic"/>.
		/// </para>
		/// </remarks>
		/// <param name="other">
		/// the other <see cref="ITopic"/> to be merged into this 
		/// <see cref="ITopic"/>
		/// </param>
		/// <exception cref="SubjectLocatorClashException">
		/// if this topic and <c>other</c> have different, non-null 
		/// values for the <see cref="ITopic.SubjectLocators"/> property and 
		/// the XTM 1.0 processing model is enabled.
		/// </exception>
		void MergeIn(ITopic other);

		/// <summary>
		/// Returns a Set of TopicMapObject instances that are subject of this 
		/// ITopic.
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Set, but is never <c>null</c>.
		/// </remarks>
		/// <value>
		/// An unmodifiable set of <see cref="ITopicMapObject"/> instances.
		/// </value>
		IReadOnlySet<ITopicMapObject> Reified { get; }

		/// <summary>
		/// Adds a <see cref="ILocator"/> to the set of source locators for 
		/// this <see cref="ITopic"/>.
		/// </summary>
		/// <remarks>
		/// It is not allowed to have two <see cref="ITopicMapObject"/>s in 
		/// the same <see cref="ITopicMap"/> with the same source locator. 
		/// If the two objects are <see cref="ITopic"/> objects, then they 
		/// must be merged. 
		/// If at least one of the two objects is not a <see cref="ITopic"/>
		/// object, a <see cref="DuplicateSourceLocatorException"/>
		/// must be reported.
		/// </remarks>
		/// <param name="sourceLocator">
		/// sourceLocator the ILocator to be added
		/// </param>
		/// <seealso cref="ITopicMapObject.SourceLocators"/>
		/// <exception cref="DuplicateSourceLocatorException">
		/// if there is already a <see cref="ITopicMapObject"/> that is not 
		/// a <see cref="ITopic"/> with this source <see cref="ILocator"/>.
		/// </exception>
		/// <exception cref="MergeException">
		/// if another ITopic with this source locator alreday exists 
		/// in the same ITopicMap, and either the feature 
		/// <a href="http://tmapi.org/features/automerge">
		/// http://tmapi.org/features/automerge 
		/// is disabled or the merge cannot be completed for some reason.
		/// </a>
		/// </exception>
		new void AddSourceLocator(ILocator sourceLocator);
	}
}