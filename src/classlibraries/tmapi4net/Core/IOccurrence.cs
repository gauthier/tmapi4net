namespace tmapi4net.Core {
	
	/// <summary>
	/// Represents the topic map occurrence construct.
	/// </summary>
	/// <remarks>
	/// An occurrence may either reference an external
	/// resource (via the resource property) or
	/// contain inline string data (in the value
	/// property). An occurrence with value will return
	/// <c>null</c> for resource and vice-versa.
	/// </remarks>
	public interface IOccurrence : IScopedObject, ITypable {
		
		/// <summary>
		/// Returns the topic to which this occurrence belongs.
		/// </summary>
		/// <value>
		/// The <see cref="ITopic"/> object which contains this 
		/// <see cref="IOccurrence"/>.
		/// </value>
		ITopic Topic { get; }

		/// <summary>
		/// Returns or sets the topic which defines the type of this 
		/// occurrence.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Getter return the <see cref="ITopic"/> object which defines the
		/// type of this <see cref="IOccurrence"/>, or <c>null</c> if there is
		/// no occurrence type.
		/// </para>
		/// <para>
		/// Setter overwrites any existing type information, set to 
		/// <c>null</c> to remove any existing type.
		/// </para>
		/// </remarks>
		/// <value>
		/// The <see cref="ITopic"/> which defines the type of this
		/// <see cref="IOccurrence"/>.
		/// </value>
		new ITopic Type { get; set; }

		/// <summary>
		/// Returns or sets the data string associated with this occurrence.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Getter return the value data string of this occurrence, or 
		/// <c>null</c> if the occurrence does not have a value data string.
		/// </para>
		/// <para>
		/// Setter overwrite any existing value data string and will set the 
		/// value of the resource property to <c>null</c>.
		/// </para>
		/// </remarks>
		string Value { get; set; }

		/// <summary>
		/// Returns or sets the resource reference locator associated 
		/// with this occurrence.
		/// </summary>
		/// <remarks>
		///	<para>
		/// Getter return the <see cref="ILocator"/> object for the resource 
		/// reference, or <c>null</c> if this occurrence does not have a
		/// resource reference.
		/// </para>
		/// <para>
		/// Setter overwrite any existing resource reference and will 
		/// set the value of the value property to <c>null</c>.
		/// </para>
		/// </remarks>
		/// <value>
		/// The ILocator object which reference the resource
		/// </value>
		ILocator Resource { get; set; }

		/// <summary>
		/// Removes this <see cref="IOccurrence"/> from the collection of
		/// <see cref="IOccurrence"/>s managed by 
		/// the parent <see cref="ITopic"/>.
		/// </summary>
		new void Remove();
	}
}