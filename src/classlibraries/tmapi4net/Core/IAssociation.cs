namespace tmapi4net.Core {
	using tmapi4net.Core.Collections;

	/// <summary>
	/// Represents the topic map association construct.
	/// </summary>
	public interface IAssociation : IScopedObject, ITypable {
		
		/// <summary>
		/// Returns or sets the topic which defines 
		/// the type of the association.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Getter will return the <see cref="ITopic"/> object which 
		/// defines the type of the association, or <c>null</c> 
		/// if the association is untyped.
		/// </para>
		/// <para>
		/// Setter replace any existing type
		/// </para>
		/// </remarks>
		/// <value>
		/// The topic specifying the type of the association,
		/// or <c>null</c> to if the association is untyped.
		/// </value>
		new ITopic Type { get; set; }

		/// <summary>
		/// Creates a new <see cref="IAssociationRole"/> representing a 
		/// role in this association.
		/// </summary>
		/// <param name="player">
		/// The topic playing the role or <c>null</c> if there is no 
		/// player of this role.
		/// </param>
		/// <param name="roleSpec">
		/// The topic defining the role of the new 
		/// <see cref="IAssociationRole"/>, or <c>null</c> if the role
		/// is untyped.
		/// </param>
		/// <returns>
		/// The newly created <see cref="IAssociationRole"/>.
		/// </returns>
		IAssociationRole CreateAssociationRole(ITopic player, ITopic roleSpec);

		/// <summary>
		/// Returns the <see cref="IAssociationRole"/> objects representing
		/// all of the roles in this association.
		/// </summary>
		/// <remarks>
		/// The return value may be an empty enumerable, but is 
		/// never <c>null</c>.
		/// </remarks>
		/// <value>
		/// An unmodifiable set of <see cref="IAssociationRole"/> objects.
		/// </value>
		IReadOnlySet<IAssociationRole> AssociationRoles { get; }

		/// <summary>
		/// Removes this <see cref="IAssociation"/> from the collection
		/// of Associations managed by the parent <see cref="ITopicMap"/>.
		/// </summary>
		/// <remarks>
		/// This method must also remove all contained 
		/// <see cref="IAssociationRole"/> objects.
		/// </remarks>
		new void Remove();
	}
}