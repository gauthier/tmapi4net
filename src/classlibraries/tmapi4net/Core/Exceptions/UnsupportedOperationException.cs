namespace tmapi4net.Core.Exceptions {
	public class UnsupportedOperationException : TMAPIException {
		public UnsupportedOperationException(string message) : base(message) {}
	}
}