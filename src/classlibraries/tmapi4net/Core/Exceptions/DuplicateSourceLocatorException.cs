namespace tmapi4net.Core.Exceptions {
	using System;

	public class DuplicateSourceLocatorException : TMAPIRuntimeException {
		private ITopicMapObject unmodifiedTopicMapObject_;
		private ITopicMapObject modifiedTopicMapObject_;

		public DuplicateSourceLocatorException(ITopicMapObject modified, ITopicMapObject unmodified, ILocator sourceLocator,
		                                       string message) : base(message) {
			this.unmodifiedTopicMapObject_ = unmodified;
			this.modifiedTopicMapObject_ = modified;
			
		}

		public ITopicMapObject UnmodifiedTopicMapObject {
			get { return unmodifiedTopicMapObject_; }
		}

		public ITopicMapObject ModifiedTopicMapObject {
			get { return modifiedTopicMapObject_; }
		}
	}
}