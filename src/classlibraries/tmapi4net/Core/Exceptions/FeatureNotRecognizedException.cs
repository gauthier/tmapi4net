namespace tmapi4net.Core.Exceptions {
	public class FeatureNotRecognizedException : TMAPIException {
		private FeatureNotRecognizedException(string message) : base(message) {
			;
		}

		public static FeatureNotRecognizedException CreateWithFeatureName(string featureName) {
			return new FeatureNotRecognizedException(string.Format("The feature name '{0}' is not recognized.", featureName));
		}
	}
}