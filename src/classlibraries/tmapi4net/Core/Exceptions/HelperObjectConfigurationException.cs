namespace tmapi4net.Core.Exceptions {
	using System;
	using System.Runtime.Serialization;

	[global::System.Serializable]
	public class HelperObjectConfigurationException : Exception {
	

		public HelperObjectConfigurationException() { }
		public HelperObjectConfigurationException(string message) : base(message) { }
		public HelperObjectConfigurationException(string message, Exception inner) : base(message, inner) { }
		protected HelperObjectConfigurationException(
			SerializationInfo info,
			StreamingContext context)
			: base(info, context) { }
	}
}