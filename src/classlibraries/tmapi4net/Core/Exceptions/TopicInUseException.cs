namespace tmapi4net.Core.Exceptions {
	using System;

	public class TopicInUseException : TMAPIException {
		public TopicInUseException(Exception innerException) : base(innerException) { }


	}
}