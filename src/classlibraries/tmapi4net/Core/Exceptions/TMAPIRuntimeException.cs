namespace tmapi4net.Core.Exceptions {
	using System;

	public class TMAPIRuntimeException : TMAPIException {
		public TMAPIRuntimeException(string message) : base(message){}
		public TMAPIRuntimeException(Exception innerException) : base(innerException) {}
	}
}