namespace tmapi4net.Core.Exceptions {
	using System;
	using System.Runtime.Serialization;

	[global::System.Serializable]
	public class HelperObjectInstantiationException : TMAPIException {	


		public HelperObjectInstantiationException() { }
		public HelperObjectInstantiationException(string message) : base(message) { }
		public HelperObjectInstantiationException(string message, Exception inner) : base(message, inner) { }
		protected HelperObjectInstantiationException(
			SerializationInfo info,
			StreamingContext context)
			: base(info, context) { }
	}
}