namespace tmapi4net.Core.Exceptions {
	using tmapi4net.Core.Collections;

	///<summary>
	///</summary>
	public class SubjectLocatorClashException : TMAPIException {
		private ISet<ITopic> topics_;

		public SubjectLocatorClashException(ISet<ITopic> topics_)
		{
			this.topics_ = topics_;
		}

		///<summary>
		///</summary>
		public ISet<ITopic> Topics {
			get { return topics_; }
		}
	}
}