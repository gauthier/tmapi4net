namespace tmapi4net.Core.Exceptions {
	using System;

	public class TopicsMustMergeException : MergeException {
		
		private ITopic unmodifiedTopic_;
		private ITopic modifiedTopic_;

		public TopicsMustMergeException(ITopic modified, ITopic unmodified, string message) {
			this.unmodifiedTopic_ = unmodified;
			this.modifiedTopic_ = modified;
		}

		public ITopic UnmodifiedTopic {
			get { return unmodifiedTopic_; }
		}

		public ITopic ModifiedTopic {
			get { return modifiedTopic_; }
		}
	}
}