namespace tmapi4net.Core.Exceptions {
	using System;
	using System.Runtime.Serialization;

	[Serializable]
	public class FactoryConfigurationException : TMAPIException {
		public FactoryConfigurationException() {}
		public FactoryConfigurationException(string message) : base(message) {}
		public FactoryConfigurationException(string message, Exception inner) : base(message, inner) {}

		protected FactoryConfigurationException(
			SerializationInfo info,
			StreamingContext context)
			: base(info, context) {}
	}


	[Serializable]
	public class TopicMapExistsException : TMAPIException {
		//public TopicMapExistsException() { }
		private TopicMapExistsException(string message) : base(message) {}
		/*public TopicMapExistsException(string message, Exception inner) : base(message, inner) { }*/

		public static TopicMapExistsException CreateByLocatorAddress(string baseLocatorAddress) {
			string message =
				String.Format("TopicMap with baseLocatorReference {0} already exists in TopicMapSystem", baseLocatorAddress);
			return new TopicMapExistsException(message);
		}

		protected TopicMapExistsException(
			SerializationInfo info,
			StreamingContext context)
			: base(info, context) {}
	}
}