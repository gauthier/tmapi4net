namespace tmapi4net.Core.Exceptions {
	public class FeatureNotSupportedException : TMAPIException {
		public FeatureNotSupportedException(string message) : base(message) {}

		public static FeatureNotSupportedException CreateWithFeatureNameAndFixedValue(string featureName, bool defaultValue) {
			return
				new FeatureNotSupportedException(
					string.Format("The feature {0} has a fixed value of {1} which cannot be altered.", featureName, defaultValue));
		}
	}
}