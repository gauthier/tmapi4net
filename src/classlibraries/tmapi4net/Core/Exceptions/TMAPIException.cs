namespace tmapi4net.Core.Exceptions {
	using System;
	using System.Runtime.Serialization;


	/// <summary>
	/// The base class for all standard (non run-time) exceptions
	/// thrown by a TMAPI system. 
	/// </summary>
	[Serializable]
	public class TMAPIException : Exception {
		public TMAPIException(Exception innerException) : base("", innerException) { }
		public TMAPIException() { }
		public TMAPIException(string message) : base(message) { }
		public TMAPIException(string message, Exception inner) : base(message, inner) { }

		protected TMAPIException(
			SerializationInfo info,
			StreamingContext context)
			: base(info, context) { }

		public static TMAPIException CreateUnsupportedHelperException(Type helperInterface) {
			return new TMAPIException(
				string.Format("interface {0}is not a supported helper", helperInterface.Name)
				);
		}
	}
}