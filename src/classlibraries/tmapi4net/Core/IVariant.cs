namespace tmapi4net.Core {
	using System.Collections.Generic;

	/// <summary>
	/// Represents the topic map variant name construct.
	/// </summary>
	/// <remarks>
	/// <para>
	/// A variant name is qualified by zero or more
	/// parameters, each of which is a topic. This
	/// class uses the <see cref="IScopedObject"/>
	/// to manage the parameters.
	/// </para>
	/// <para>
	/// A variant name may either reference an external
	/// resource (via the resource property) or
	/// contain inline string data (in the value
	/// property). A IVariant with value will return
	/// <c>null</c> for resource and vice-versa.
	/// </para>
	/// </remarks>
	public interface IVariant : IScopedObject {

		/// <summary>
		/// Returns the <see cref="ITopicName"/> to which this variant belongs.
		/// </summary>
		/// <value>
		/// A <see cref="ITopicName"/> object.
		/// </value>
		ITopicName TopicName { get; }

		/// <summary>
		/// Returns or sets the respirce data string associated with 
		/// this variant.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Getter return the value string of this variant.
		/// </para>
		/// <para>
		/// Setter sets the resource data string for this variant;
		/// will overwrite any existing value data string 
		/// and will set the value of the resource property to <c>null</c>..
		/// </para>
		/// </remarks>
		/// <value>
		/// the value string of this variant, 
		/// or <c>null</c> if the variant does not have a value data string.
		/// </value>
		string Value { get; set; }

		/// <summary>
		/// Returns or sets the resource reference locator associated 
		/// with this variant.
		/// </summary>
		/// <remarks>
		///	<para>
		/// Setter sets the resource reference for this variant.
		/// will overwrite any existing resource reference and will 
		/// set the value property to <c>null</c>.
		/// </para>
		/// </remarks>
		/// <value>
		/// The ILocator object for the resource reference, or 
		/// <c>null</c> if this variant does not have a resource reference.
		/// </value>
		ILocator Resource { get; set; }

		/// <summary>
		/// Removes this IVariant from the collection of 
		/// <see cref="IVariant"/>s managed by the parent 
		/// <see cref="ITopicName"/> object.
		/// </summary>
		new void Remove();

		/// <summary>
		/// Returns the topics which define the parameters of
		/// this IVariant.
		/// </summary>
		/// <remarks>
		/// <para>
		/// This method must return only the topics which are the
		/// parameters of the IVariant and not the topics which are 
		/// in the scope of the parent <see cref="ITopicName"/> instance.
		/// </para>
		/// <para>
		/// The return value may be an empty Set, but is never <c>null</c>.
		/// </para>
		/// </remarks>
		/// <value>
		/// An unmodifiable Set of ITopic objects.
		/// </value>
		new IEnumerable<ITopic> Scope { get; }
	}
}