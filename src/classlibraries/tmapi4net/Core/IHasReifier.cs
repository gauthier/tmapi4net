namespace tmapi4net.Core {



	/// <summary>
	/// Convinience interface for single typed objects.
	/// </summary>
	public interface ITypable {

		/// <summary>
		/// Returns or sets the <see cref="ITopic"/> that type this instance.
		/// </summary>
		ITopic Type { get; set; }
	}





	/// <summary>
	/// Convinience interface for reified objects.
	/// </summary>
	public interface IReifiable {
		
		/// <summary>
		/// Returns the <see cref="ITopic"/> that reifies this instance.
		/// </summary>
		/// <value>
		/// A <see cref="ITopic"/> instance or <c>null</c> if the 
		/// topic name is not reified.
		/// </value>
		ITopic Reifier { get; }
	}
}