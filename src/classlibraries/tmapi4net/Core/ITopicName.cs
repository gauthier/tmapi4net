namespace tmapi4net.Core {
	using System.Collections.Generic;
	using tmapi4net.Core.Collections;
	using tmapi4net.Core.Exceptions;

	/// <summary>
	/// Represents the topic map topic name construct.
	/// </summary>
	/// <remarks>
	/// In XTM, this construct is represented by
	/// the <c>&lt;baseName&gt;</c> element.
	/// </remarks>
	public interface ITopicName : IScopedObject, ITypable {
		
		///<summary>
		/// Returns the ITopic which contains this TopicName.
		/// </summary>
		ITopic Topic { get; }

		/// <summary>
		/// Returns or sets the name string of the TopicName
		/// </summary>
		/// <remarks>
		/// <para>
		/// Setter defines the name string for this <see cref="ITopicName"/>.
		/// A new value will replace any existing value.
		/// A <c>null</c> value indicates that there is no name string for this
		/// <see cref="ITopicName"/>.
		/// </para>
		/// </remarks>
		/// <value>
		/// The name string or <c>null</c> if this TopicName has no value.
		/// </value>
		/// <exception cref="MergeException">
		/// if the processor detects that a merge is required 
		/// and either automerge is disabled or automerge is enabled 
		/// but the merge cannot be completed for some other reason.
		/// </exception>
		string Value { get; set; }

		/// <summary>
		/// Returns the variants defined for this <see cref="ITopicName"/>.
		/// The return value may be an empty Set, but is never <c>null</c>.
		/// </summary>
		/// <value>
		/// An unmodifiable Set of <see cref="IVariant"/> objects.
		/// </value>
		IReadOnlySet<IVariant> Variants { get; }

		/// <summary>
		/// Creates a variant of this topic name with the specified inline
		/// data string and scopes.
		/// </summary>
		/// <param name="resourceData">
		/// The inline data string for the variant. Pass
		/// <c>null</c> for no string.
		/// </param>
		/// <param name="scope">
		/// The scope for the <c ref="IVariant"/> to be created. Must
		/// be a collection of <c ref="ITopic"/> objects.
		/// </param>
		/// <returns>the newly created <c ref="IVariant"/> object.</returns>
		IVariant CreateVariant(string resourceData, IEnumerable<ITopic> scope);

		/// <summary>
		/// Creates a variant of this topic name with the specified external
		/// resource locator and scopes.
		/// </summary>
		/// <param name="resource">
		/// The locator of the external resource for the
		/// variant. Pass <c>null</c> for no reference.
		/// </param>
		/// <param name="scope">
		/// The scope for the IVariant to be created. Must
		/// be a collection of ITopic objects.
		/// </param>
		/// <returns>the newly created IVariant object.</returns>
		IVariant CreateVariant(ILocator resource, IEnumerable<ITopic> scope);

		/// <summary>
		///Returns or sets the ITopic that defines the type of this topic name.
		/// </summary>
		/// <remarks>
		/// <para>
		/// OPTIONAL: This method reflects the XTM 1.1 model. 
		/// A processor which does not implement support for XTM 1.1 (or if the 
		/// feature "http://tmapi.org/features/model/xtm1.1" is disabled)
		/// </para>
		/// <para>
		/// Getter MUST return <c>null</c> if a processor which does not 
		/// implement support for XTM 1.1 (or if the feature 
		/// "http://tmapi.org/features/model/xtm1.1" is disabled)
		/// </para>
		/// <para>
		/// Setter overwrites any existing type information.
		/// MUST throw an UnsupportedOperationException when A processor which
		///  does not implement support for XTM 1.1 (or if the feature 
		/// "http://tmapi.org/features/model/xtm1.1" is disabled)
		/// </para>
		/// </remarks>
		/// <value>
		/// A <see cref="ITopic"/> instance or <c>null</c> if the topic name 
		/// does not have a type specified.
		/// </value>
		/// <exception cref="UnsupportedOperationException">
		/// if the processor does not support the XTM 1.1 model.
		/// </exception>
		/// <exception cref="MergeException">
		/// if the processor detects that a merge is required 
		/// and either automerge is disabled or automerge is enabled but the 
		/// merge cannot be completed for some other reason.
		/// </exception>
		new ITopic Type { get; set; }

		/// <summary>
		/// Removes this <see cref="ITopicName"/> from the collection 
		/// of <see cref="ITopicName"/>s managed by the parent 
		/// <see cref="ITopic"/>. 
		/// </summary>
		/// <remarks>
		/// This method removes all contained <see cref="IVariant"/> objects.
		/// </remarks>
		new void Remove();

		/// <summary>
		/// Adds the scoping topic <c>topic</c> to the collection of 
		/// <see cref="ITopic"/>s that define the scope of this name.
		/// </summary>
		/// <param name="topic">
		/// the scoping topic to be added to the scope of this name.
		/// </param>
		/// <exception cref="MergeException">
		/// if the processor detects that a merge is required
		/// and either automerge is disabled or automerge is enabled but the 
		/// merge cannot be completed for some other reason.
		/// </exception>
		new void AddScopingTopic(ITopic topic);

		/// <summary>
		/// Removes a topic from the set which defines the scope/parameters
		/// for this object.
		/// </summary>
		/// <param name="topic">The topic to be remove from the set.</param>
		/// <exception cref="MergeException">
		/// if the processor detects that a merge is required 
		/// and either automerge is disabled or automerge is enabled but
		/// the merge cannot be completed for some other reason.
		/// </exception>
		new void RemoveScopingTopic(ITopic topic);
	}
}