namespace tmapi4net.Core {
	using tmapi4net.Core.Exceptions;

	/// <summary>
	/// This factory class provides access to a topic map system.
	/// </summary>
	/// <remarks>
	/// <para>
	/// A new <see cref="ITopicMapSystem"/> instance is created by 
	/// invoking the <see cref="NewTopicMapSystem"/> method.
	/// </para>
	/// <para>
	/// Configuration properties for the new TopicMapSystem instance
	/// can be set by calling the <see cref="SetFeature"/> or 
	/// <see cref="SetProperty"/>
	/// methods prior to invoking <see cref="NewTopicMapSystem"/>.
	/// </para>
	/// </remarks>
	public interface ITopicMapSystemFactory {

		/// <summary>
		/// Creates a new <see cref="ITopicMapSystem"/> instance using
		/// the currently configured factory parameters.
		/// </summary>
		/// <returns>
		/// a new instance of a <see cref="ITopicMapSystem"/>
		/// </returns>
		/// <exception cref="TMAPIException">
		/// if a <see cref="ITopicMapSystem"/> cannot be created which 
		/// satisfies the requested configuration.
		/// </exception>
		ITopicMapSystem NewTopicMapSystem();

		/// <summary>
		/// Returns if the particular feature is supported by the 
		/// <see cref="ITopicMapSystem"/>.
		/// </summary>
		/// <remarks>
		/// Opposite to <see cref="GetFeature"/>,
		/// this method returns if the requested feature is 
		/// generally available / supported by the 
		/// underlying <see cref="ITopicMapSystem"/> and does not 
		/// return the state (enabled/disabled) of the feature.
		/// </remarks>
		/// <param name="featureName">the name of the feature to check</param>
		/// <returns>true if the requested feature is supported, otherwise false</returns>
		bool IsFeatureSupported(string featureName);

		/// <summary>
		/// Returns the particular feature requested for in the underlying
		/// implementation of <see cref="ITopicMapSystem"/>.
		/// </summary>
		/// <param name="featureName">the name of the feature to check</param>
		/// <returns>
		/// true if the named feature is enabled for 
		/// <see cref="ITopicMapSystem"/> instances created by this factory;
		/// false if the named feature is disabled for
		/// <see cref="ITopicMapSystem"/> instances created by this factory.
		/// </returns>
		/// <exception cref="FeatureNotRecognizedException">
		/// if the underlying implementation does not recongnize
		/// the named feature.
		/// </exception>
		bool GetFeature(string featureName);

		/// <summary>
		/// Sets a particular feature in the underlying implementation of 
		/// <see cref="ITopicMapSystem"/>.
		/// </summary>
		/// <remarks>
		/// A list of the core features
		/// can be found at http://tmapi.org/features/
		/// </remarks>
		/// <param name="featureName">the name of the feature to be set</param>
		/// <param name="value">true to enable the feature, false to disable it</param>
		/// <exception cref="FeatureNotRecognizedException">
		/// if the underlying implementation does not recongnize the named feature.
		/// </exception>
		/// <exception cref="FeatureNotSupportedException">
		/// if the underlying implementation recongnizes the named feature but does not support enabling or 
		/// disabling it (as specified by the <c>value</c> parameter.
		/// </exception>
		void SetFeature(string featureName, bool value);

		/// <summary>
		/// Gets the value of a property in the underlying implementation of <see cref="ITopicMapSystem"/>}.
		/// A list of the core properties defined by TMAPI can be found at 
		/// http://tmapi.org/properties/. An implementation is free to
		/// support properties other than the core ones.
		/// </summary>
		/// <param name="propertyName">the name of the property to retrieve.</param>
		/// <returns>
		/// the value set for this property or <c>null</c> if no value is currently set for the property.
		/// </returns>
		string GetProperty(string propertyName);

		/// <summary>
		/// Sets a property in the underlying implementation of {@link TopicMapSystem}.
		/// </summary>
		/// <remarks>
		/// <para>
		/// A list of the core
		/// properties defined by TMAPI can be found at <a href="http://tmapi.org/properties/">
		/// http://tmapi.org/properties/</a>.
		/// </para>
		/// <para>
		/// An implementation is free to
		/// support properties other than the core ones.
		/// </para>
		/// </remarks>
		/// <param name="propertyName">the name of the property to be set</param>
		/// <param name="value">
		/// the value to be set of this property or <c>null</c> to remove the
		/// property from the current factory configuration.
		/// </param>
		void SetProperty(string propertyName, string value);
	}
}