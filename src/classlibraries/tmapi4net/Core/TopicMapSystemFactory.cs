namespace tmapi4net.Core {
	using System;
	using System.Reflection;
	using tmapi4net.Core.Exceptions;

	/// <summary>
	/// This factory class provides access to a topic map system.
	/// A new <see cref="ITopicMapSystem"/> instance is created by invoking the
	/// <see ref="NewTopicMapSystem"/> method.
	/// Configuration properties for the new <see cref="ITopicMapSystem"/>
	/// instance can be set by calling the
	/// <see cref="ITopicMapSystemFactory.SetFeature"/> and/or
	/// <see cref="ITopicMapSystemFactory.SetProperty"/>
	/// methods prior to invoking
	/// <see cref="ITopicMapSystemFactory.NewTopicMapSystem"/>.
	/// </summary>
	public abstract class TopicMapSystemFactory : ITopicMapSystemFactory {


	#region ITopicMapSystemFactory Members

		ITopicMapSystem ITopicMapSystemFactory.NewTopicMapSystem() {
			throw new Exception("The method or operation is not implemented.");
		}

		bool ITopicMapSystemFactory.IsFeatureSupported(string featureName) {
			throw new Exception("The method or operation is not implemented.");
		}

		bool ITopicMapSystemFactory.GetFeature(string featureName) {
			throw new Exception("The method or operation is not implemented.");
		}

		void ITopicMapSystemFactory.SetFeature(string featureName, bool value) {
			throw new Exception("The method or operation is not implemented.");
		}

		string ITopicMapSystemFactory.GetProperty(string propertyName) {
			throw new Exception("The method or operation is not implemented.");
		}

		void ITopicMapSystemFactory.SetProperty(string propertyName, string value) {
			throw new Exception("The method or operation is not implemented.");
		}

		#endregion

		/// <summary>
		/// Obtain a new instance of a tmapi4net.Core.TopicMapSystemFactory.
		/// </summary>
		/// <remarks>
		/// <para>
		/// This static method creates a new factory instance.
		/// This method uses the following ordered lookup procedure to determine the
		/// <see cref="ITopicMapSystemFactory"/> implementation class to load:
		/// <list>
		///		<item>
		///			<para>
		///				Use the
		///				<c>tmapi4net.Core.TopicMapSystemFactory</c>
		///				 property.
		///			</para>
		///			<para>
		///				The value of this property is the fully qualified
		///				name of the implementation class to use.
		///			</para>
		/// 	 </item>
		///   <!--<item>
		///      Use the properties file <c>lib/tmapi.properties</c> in the JRE directory.
		///      This configuration file is in standard java.util.Properties format
		///      and contains the fully qualified name of the implementation class
		///      with the key being the system property defined above.
		///   </item>
		///   <item>
		///      Use the Services API (as detailed in the JAR specification), if
		///      available, to determine the classname. The Services API will look for
		///      a classname in the file <c>META-INF/services/org.tmapi.tmapi4net.Core.TopicMapSystemFactory</c>
		///      in jars available to the runtime.
		///   </item> -->
		/// </list>
		/// </para>
		/// <para>
		/// Once an application has obtained a reference to a
		/// tmapi4net.Core.TopicMapSystemFactory it can use the factory to
		/// configure and obtain TopicMapSystem instances.
		/// </para>
		/// </remarks>
		/// <returns>
		/// a new instance of tmapi4net.Core.TopicMapSystemFactory
		/// </returns>
		/// <exception cref="FactoryConfigurationException">
		///
		/// </exception>
		public static ITopicMapSystemFactory NewInstance() {
			Type implementationType = getImplementationType();
			if (typeof (ITopicMapSystemFactory).IsAssignableFrom(implementationType)) {
				ConstructorInfo ci = implementationType.GetConstructor(new Type[] {});
				if (ci != null)
					return (ITopicMapSystemFactory) ci.Invoke(new object[] {});
				throw new FactoryConfigurationException();
			}
			else
				throw new FactoryConfigurationException();
		}


		/**
     * Implements the class name lookup logic for the
     * newInstance() method. When a class name is found,
     * this method attempts to load the specified class.
     * @return the loaded implementation class or null
     *   if no implementation class name was found.
     * @throws FactoryConfigurationException if the specified
     *   implementation class could not be loaded.
     */


		private static Type getImplementationType() {
			string implementationTypeName = TopicMapSystemFactoryTypeName_;
			if (implementationTypeName == null)
				implementationTypeName = getImplementationTypeNameFromProperties();
			if (implementationTypeName == null)
				implementationTypeName = getClassNameFromResource();
			if (implementationTypeName == null) {
				throw new FactoryConfigurationException("No TopicMapSystemFactory implementation class specified.");
			}
			else {
				try {
					return Type.GetType(implementationTypeName);
				}
				catch (Exception ex) {
					throw new FactoryConfigurationException(
						"Error loading TopicMapSystemFactory implementation class " + implementationTypeName + ".", ex);
				}
			}
		}

		private static string getClassNameFromResource() {
			throw new Exception("The method or operation is not implemented.");
		}

		private static string getImplementationTypeNameFromProperties() {
#if DEBUG
			return "TestTopicMapSystemFactory";
#else
 	throw new Exception("The method or operation is not implemented.");
#endif
		}

		public static void SetTopicMapSystemFactoryTypeName(string typeName) {
			TopicMapSystemFactoryTypeName_ = typeName;
		}

		private static string TopicMapSystemFactoryTypeName_;
	}
}