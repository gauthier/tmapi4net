namespace tmapi4net.Core {
	using tmapi4net.Core.Collections;

	/// <summary>
	/// The base class for all objects which take a collection of
	/// topic objects to define a scope or parameter property.
	/// </summary>
	public interface IScopedObject : ITopicMapObject, IReifiable {
		
		/// <summary>
		/// Returns the topics which define the scope/parameters of
		/// this <see cref="IScopedObject"/>.
		/// </summary>
		/// <remarks>
		/// The value may be an empty Set, but is never <c>null</c>.
		/// </remarks>
		/// <value>
		/// An unmodifiable set of <see cref="ITopic"/> objects.
		/// </value>
		IReadOnlySet<ITopic> Scope { get; }

		/// <summary>
		/// Adds a topic to the set which define the scope/parameters for this
		/// object.
		/// </summary>
		/// <param name="topic">The topic to be added to the set.</param>
		void AddScopingTopic(ITopic topic);

		/// <summary>
		/// Removes a topic from the set which defines the scope/parameters
		/// for this object.
		/// </summary>
		/// <param name="topic">The topic to be remove from the set.</param>
		void RemoveScopingTopic(ITopic topic);
	}
}