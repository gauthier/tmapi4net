namespace tmapi4net.Core {
	using System.Collections.Generic;
	using tmapi4net.Core.Exceptions;


	/// <summary>
	/// convinience base class for implementing ITopicMapSystem interface
	/// </summary>
	public abstract class TopicMapSystemBase {
		private IDictionary<string, bool> featuresValues_;


		/// <summary>
		/// Constructor that initialize features values
		/// </summary>
		/// <param name="features"></param>
		protected TopicMapSystemBase(IDictionary<string, bool> features) {

			featuresValues_ = new Dictionary<string, bool>();
			
			foreach (string featurename in features.Keys) {
				featuresValues_.Add(featurename, features[featurename]);
			}

		}

		/// <summary>
		/// Implement of the GetFeature method
		/// </summary>
		/// <param name="featureName">the feature to return value</param>
		/// <returns>the value of the feature</returns>
		/// <exception cref="FeatureNotRecognizedException"></exception>
		public bool GetFeature(string featureName) {
			
			// if features is recognized
			if (featuresValues_.ContainsKey(featureName))
				return featuresValues_[featureName];

			// else throw
			throw FeatureNotRecognizedException.CreateWithFeatureName(featureName);
		}

	}
}