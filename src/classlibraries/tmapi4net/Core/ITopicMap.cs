namespace tmapi4net.Core {
	using tmapi4net.Core.Collections;
	using tmapi4net.Core.Exceptions;

	/// <summary>
	/// Represents a topic map construct.
	/// </summary>
	public interface ITopicMap : ITopicMapObject, IReifiable {
		
		/// <summary>
		/// Returns the <see cref="ITopicMapSystem"/> to which this topic map belongs	/// </summary>
		/// <value>
		/// The <see cref="ITopicMapSystem"/> object which manages this ITopicMap.
		/// </value>
		ITopicMapSystem TopicMapSystem { get; }

		/// <summary>
		/// Returns the base locator of this topic map.
		/// </summary>
		/// <remarks>
		/// This locator is
		/// used as the unique identifier for the ITopicMap within the
		/// ITopicMap system. It is also used for resolving references
		/// to resources external to the ITopicMap during parsing.
		/// </remarks>
		ILocator BaseLocator { get; }

		/// <summary>
		/// Returns all of the associations in the topic map.
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Set, but is never <c>null</c>.
		/// </remarks>
		/// <value>an unmodifiable set of IAssociation objects.</value>
		IReadOnlySet<IAssociation> Associations { get; }

		/// <summary>
		/// Creates a new association in the topic map.
		/// </summary>
		/// <returns>
		/// The newly created IAssociation.
		/// </returns>
		/// <remarks>
		/// The newly created association has no type, no roles and
		/// is in the unconstrained scope.
		/// </remarks>
		IAssociation CreateAssociation();

		/// <summary>
		/// Returns all of the topics in the topic map.
		/// </summary>
		/// <remarks>
		/// The return value may be an empty Set, but is never <c>null</c>.
		/// </remarks>
		/// <value>
		/// an unmodifiable set of ITopic objects.
		/// </value>
		IReadOnlySet<ITopic> Topics { get; }

		/// <summary>
		/// Creates a new topic in the topic map.
		/// </summary>
		/// <remarks>
		/// The newly created topic has no types, names or occurrences.
		/// </remarks>
		/// <returns>The newly created <see cref="ITopic"/>.</returns>
		ITopic CreateTopic();

		/// <summary>
		/// Creates a locator with the specified notation and reference.
		/// </summary>
		/// <param name="address">
		/// The address string of the new locator.
		/// </param>
		/// <param name="notation">
		/// The notation of the new locator.
		/// </param>
		/// <returns>The newly created <see cref="ILocator"/>.</returns>
		ILocator CreateLocator(string address, string notation);

		/// <summary>
		/// Creates and returns a new <see cref="ILocator"/> object.
		/// </summary>
		/// <remarks>
		/// The notation of the created object will be the same
		/// as that of the baseLocator.
		/// </remarks>
		/// <param name="address">
		/// The address string of the new locator.
		/// </param>
		/// <returns>
		/// The newly created <see cref="ILocator"/>.
		/// </returns>
		ILocator CreateLocator(string address);

		/// <summary>
		/// Removes this ITopicMap from the parent 
		/// <see cref="ITopicMapSystem"/>.
		/// </summary>
		/// <remarks>
		/// This method also removes all contained 
		/// <see cref="ITopic"/> and <see cref="IAssociation"/> objects.
		/// </remarks>
		new void Remove();

		/// <summary>
		/// Returns the TopicMapObject managed by this 
		/// <see cref="ITopicMap"/> with the specified object ID.
		/// </summary>
		/// <param name="objectId">
		/// the object ID of the <see cref="ITopicMapObject"/> to be returned
		/// </param>
		/// <returns>
		/// the <see cref="ITopicMapObject"/> with the specified ID or 
		/// <c>null</c> if no <see cref="ITopicMapObject"/> with that ID
		/// is managed by this <see cref="ITopicMap"/>
		/// </returns>
		ITopicMapObject GetObjectById(string objectId);

		/// <summary>
		/// Merges the <see cref="ITopicMap"/> <c>other</c> into this <see cref="ITopicMap"/>.
		/// </summary>
		/// <remarks>
		/// All Topics and Associations and all of their contents in
		/// <c>other</c> will be added to this ITopicMap. Topics
		/// in <c>other</c> will be merged with topics in this
		/// <see cref="ITopicMap"/> as defined by XTM merging rules. The merge
		/// process will not modify <c>other</c> in any way.
		/// </remarks>
		/// <param name="other">the <see cref="ITopicMap"/> to be merged with this <see cref="ITopicMap"/></param>
		/// <exception cref="SubjectLocatorClashException">
		/// if merging of topics 
		///    of this ITopicMap instance and the <c>other</c>
		///    cannot be completed because of subjectLocator property 
		///    conflicts and the XTM 1.0 processing model is enabled.
		/// </exception>
		void MergeIn(ITopicMap other);

		/// <summary>
		/// Returns a concrete implementation of a specified helper
		/// object interface.
		/// </summary>
		/// <remarks>
		/// TMAPI implementations are required to
		/// determine the concrete implementation class as follows where
		/// <typeparamref name="TInterface}"/> is replaced by the fully qualified class
		/// of the helper object interface specified in the 
		/// <typeparamref name="TInterface}"/> generic parameter.
		/// <para>
		/// The steps must be applied in the order specified above,
		/// if a step does not result in a fully qualified class
		/// name being located, then the next step should be applied.
		/// If all steps are applied without finding an implementation
		/// class name, then an <see cref="UnsupportedHelperObjectException"/>
		/// exception must be thrown.
		/// </para>
		/// <para>
		/// If a step results in a value being returned, that value shall
		/// be interpreted as a fully qualified class.
		/// Implementations are free to return either a new instance of
		/// the specified class or a cached instance. If instantiation
		/// of the implementation class results in an exception, this
		/// should be propagated as a
		/// <see cref="HelperObjectInstantiationException"/>.
		/// </para>
		/// <para>If the implementation class implements the interface
		/// <see cref="IConfigurableHelperObject"/>, then before returning
		/// the implementation instance, 
		/// the method <see cref="IConfigurableHelperObject.Configure"/> 
		/// must be invoked on the instance. 
		/// If the method throws a
		/// <see cref="HelperObjectConfigurationException"/>, the
		/// exception must be propagated from this method.
		/// </para>
		/// </remarks>
		/// <typeparam name="TInterface">
		/// a type that specifies an interface that the returned 
		/// implementation object must implement.
		/// </typeparam>
		/// <returns>an implementation of <typeparamref name="TInterface}"/>.</returns>
		TInterface GetHelperObject<TInterface>();

		/// <summary>
		/// Closes use of this <see cref="ITopicMap"/>.
		/// </summary>
		/// <remarks>
		/// This method should be invoked by the
		/// application once it is finished using the 
		/// <see cref="ITopicMap"/> instance. 
		/// Implementation may release any resources required for the 
		/// <see cref="ITopicMap"/> instance or any ofons the 
		/// <see cref="ITopicMapObject"/> instances contained by 
		/// the <see cref="ITopicMap"/> instance.
		/// </remarks>
		/// <exception cref="UnsupportedHelperObjectException">
		///  if no implementation of the specified helper interface can be found.
		/// </exception>
		/// <exception cref="HelperObjectInstantiationException">
		/// if the specified implementation class could not be loaded o
		/// r if a new instance of the helper class could not be instantiated.
		/// </exception>
		/// <exception cref="HelperObjectConfigurationException">
		/// if the <see cref="IConfigurableHelperObject.Configure"/>
		/// method of the helper object implementation raises
		/// an exception.
		/// </exception>
		void Close();
	}
}