namespace tmapi4net.Core {
	using tmapi4net.Core.Exceptions;

	/// <summary>
	/// An interface for a helper object implementation 
	/// which requires notification of the <see cref="ITopicMap"/>
	/// instance it acts upon.
	/// </summary>
	public interface IConfigurableHelperObject {

		/// <summary>
		/// Configuration method of the helper object.
		/// </summary>
		/// <remarks>
		/// The object
		/// instance must be guaranteed that this method will 
		/// always be invoked prior to calls to any other
		/// class method other than constructors.
		/// </remarks>
		/// <param name="topicMap">
		/// the ITopicMap instance that this helper object acts upon.
		/// </param>
		/// <exception cref="HelperObjectConfigurationException">
		/// if the configuration could not be completed for some reason.
		/// </exception>
		void Configure(ITopicMap topicMap);
	}
}