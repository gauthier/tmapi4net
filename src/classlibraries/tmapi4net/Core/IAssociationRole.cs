namespace tmapi4net.Core {
	/// <summary>
	/// Represents the topic map member construct (known as
	/// association role in HyTM).
	/// </summary>
	/// <remarks>
	/// This class represents a normalised association member.
	/// In normalising the members of an association, one member
	/// must be created for each of the role players of the association.
	/// For example the following (XTM) association would result
	/// in three Member objects.
	/// <code>
	///	&lt;association>
	///		&lt;member>
	///			&lt;instanceOf>&lt;topicRef xlink:href="#parent"/>&lt;/instanceOf>
	///			&lt;topicRef xlink:href="#homer"/>
	///		&lt;/member>
	///		&lt;member>
	///			&lt;instanceOf>&lt;topicRef xlink:href="#child"/>&lt;/instanceOf>
	///			&lt;topicRef xlink:href="#bart"/>
	///			&lt;topicRef xlink:href="#lisa"/>
	///		&lt;/member>
	///	&lt;/association>
	/// </code>
	/// </remarks>
	public interface IAssociationRole : ITopicMapObject, IReifiable, ITypable {
		
		/// <summary>
		/// Returns the association to which this 
		/// <see cref="IAssociationRole"/> belongs.
		/// </summary>
		IAssociation Association { get; }

		/// <summary>
		/// Returns or sets the player of this <see cref="IAssociationRole"/>.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Getter return the <see cref="ITopic"/> which plays this role 
		/// in the association, or <c>null</c> if there is no role player.
		/// </para>
		/// <para>
		/// Setter overwrites any previous value.
		/// </para>
		/// </remarks>
		/// <value>
		/// The topic which plays this role in the association,
		/// or <c>null</c> if no topic plays this role.
		/// </value>
		ITopic Player { get; set; }

		/// <summary>
		/// Returns or sets the topic which defines the 
		/// type of this <see cref="IAssociationRole"/>.
		/// </summary>
		/// <remarks>
		///	<para>
		/// Getter will return The <see cref="ITopic"/> object which defines the 
		/// type of this association role, or <c>null</c> if there 
		/// is no role-defining topic.
		/// </para>
		/// <para>
		/// Setter will Overwrites any existing role type. 
		/// <c>null</c> to remove any existing role-defining topic.
		/// </para>
		/// </remarks>
		/// <value>
		/// The <see cref="ITopic"/> object which defines the type 
		/// of this <see cref="IAssociationRole"/>, or <c>null</c> 
		/// if there is no role-defining topic.
		/// </value>
		new ITopic Type { get; set; }

		/// <summary>
		/// Returns the ITopic that reifies this association role.
		/// </summary>
		/// <value>
		/// A <see cref="ITopic"/> instance or <c>null</c> if the 
		/// association role is not reified.
		/// </value>
		new ITopic Reifier { get; }

		/// <summary>
		/// Removes this <see cref="IAssociationRole"/> from the collection 
		/// of <see cref="IAssociationRole"/>s
		/// managed by the parent <see cref="IAssociation"/>. 
		/// </summary>
		///<remarks>
		/// The Topics reference by the <see cref="Player"/> and 
		/// <see cref="Type"/> properties are not affected by this method.
		/// </remarks>
		new void Remove();
	}
}