namespace tmapi4net.Core {
	using tmapi4net.Core.Collections;
	using tmapi4net.Core.Exceptions;

	/// <summary>
	/// A generic interface to a TMAPI system.
	/// </summary>
	/// <remarks>
	/// <para>
	/// Any TMAPI system must be capable of providing access to one or more 
	/// ITopicMap objects.
	/// </para>
	/// <para>
	/// A TMAPI system <b>may</b> be capable of allowing a client to 
	/// create a new ITopicMap object.
	/// </para>
	/// <para>
	/// Every ITopicMap managed by a TopicMapSystem must be uniquely
	/// identifiable by a locator. This locator must be specified when
	/// creating a new ITopicMap and is the identifier used to retrieve
	/// a ITopicMap from the TopicMapSystem.
	/// </para>
	/// </remarks>
	public interface ITopicMapSystem {

		/// <summary>
		/// Creates a new topic map with a specified base locator.
		/// </summary>
		/// <param name="baseLocatorAddress">
		/// The data part of the base locator reference.
		/// </param>
		/// <param name="baseLocatorNotation">
		/// The notation of the base locator reference.
		/// </param>
		/// <returns>
		/// the newly created ITopicMap object.
		/// </returns>
		/// <exception cref="TopicMapExistsException">
		/// if the TopicMapSystem already manages a ITopicMap with the 
		/// specified base locator.
		/// </exception>
		ITopicMap CreateTopicMap(string baseLocatorAddress, string baseLocatorNotation);

		/// <summary>
		/// Creates a new topic map with a specified base locator.
		/// </summary>
		/// <remarks>
		/// The locator reference string is assumed to be in URI notation.
		/// </remarks>
		/// <param name="baseLocatorAddress">
		/// The data part of the base locator reference. 
		/// <remarks>
		/// The value must be a valid URI.
		/// </remarks>
		/// </param>
		/// <returns>
		/// the newly created ITopicMap object.
		/// </returns>
		/// <exception cref="TopicMapExistsException">
		/// if the TopicMapSystem already manages a ITopicMap with the 
		/// specified base locator.
		/// </exception>
		ITopicMap CreateTopicMap(string baseLocatorAddress);

		/// <summary>
		/// Retrieves the topic map managed by the system with the specified
		/// base locator.
		/// </summary>
		/// <param name="baseLocatorAddress">
		/// The data part of the base locator reference.
		/// </param>
		/// <param name="baseLocatorNotation">
		/// The notation of the base locator reference.
		/// </param>
		/// <returns>
		/// The ITopicMap object managed by the TopicMapSystem which 
		/// has the specified base locator, or <c>null</c> if no such
		/// ITopicMap is found.
		/// </returns>
		ITopicMap GetTopicMap(string baseLocatorAddress, string baseLocatorNotation);

		/// <summary>
		/// Retrieves the topic map managed by the system with the specified 
		/// base locator reference.
		/// </summary>
		/// <remarks>
		/// The locator reference is assumed to be in URI notation.
		/// </remarks>
		/// <param name="baseLocatorAddress">
		/// The data part of the base locator reference.
		/// <remarks>
		/// The value must be a valid URI.
		/// </remarks>
		/// </param>
		/// <returns>
		/// The ITopicMap object managed by the TopicMapSystem which 
		/// has the specified base locator, or <c>null</c> if no such
		/// ITopicMap is found.
		/// </returns>
		ITopicMap GetTopicMap(string baseLocatorAddress);

		/// <summary>
		/// Retrieves the topic map managed by the system with the specified 
		/// base locator.
		/// </summary>
		/// <remarks>
		/// The only way to get a base locator
		/// without creating or having a topicmap instance
		/// is the method getBaseLocators() which returns a set of base locators
		/// </remarks>
		/// <param name="baseLocator">
		/// The base locator object.
		/// </param>
		/// <returns>
		/// The ITopicMap object managed by the TopicMapSystem which 
		/// has the specified base locator, or <c>null</c> if no such
		/// ITopicMap is found.
		/// </returns>
		ITopicMap GetTopicMap(ILocator baseLocator);

		/// <summary>
		/// Returns the base locators of the topic maps managed by the system.
		/// </summary>
		/// <remarks>
		/// Implementations MUST return the base locator of every ITopicMap 
		/// that would be accessible by a <c>GetTopicMap</c> call. 
		/// The return value may be an empty Set if no TopicMaps are currently 
		/// managed by the TopicMapSystem, but must never be <c>null</c>.
		/// </remarks>
		/// <value>
		/// An unmodifiable Set of base locator objects
		/// </value>
		IReadOnlySet<ILocator> BaseLocators { get; }

		/// <summary>
		/// Gets a property in the underlying implementation of {@link TopicMapSystem}.
		/// </summary>
		/// <remarks>
		/// <para>
		/// A list of the core properties defined by TMAPI can be found at 
		/// <a href="http://tmapi.org/properties/">http://tmapi.org/properties/</a>.
		/// </para>
		/// <para>
		/// An implementation is free to support properties other than the 
		/// core ones.
		/// </para>
		/// <para>
		/// The properties supported by the TopicMapSystem
		/// and the value for each property is set when the TopicMapSystem is 
		/// created by a call to 
		/// <see cref="ITopicMapSystemFactory.NewTopicMapSystem"/> and cannot 
		/// be modified subsequently.
		/// </para>
		/// </remarks>
		/// <param name="propertyName">
		/// the name of the property to retrieve.
		/// </param>
		/// <returns>
		/// the value set for this property or <c>null</c> if no value is set 
		/// for the property.
		/// </returns>
		string GetProperty(string propertyName);

		/// <summary>
		/// Returns the value of the feature specified by <c>featureName</c>
		/// for this TopicMapSystem instance.
		/// </summary>
		/// <remarks>
		/// The features supported by the TopicMapSystem and the value for
		/// each feature is set when the TopicMapSystem is created by a call to
		/// <see cref="ITopicMapSystemFactory.NewTopicMapSystem"/> and cannot 
		/// be modified subsequently.
		/// </remarks>
		/// <param name="featureName">
		/// the name of the feature to check
		/// </param>
		/// <returns>
		/// <para>
		/// true if the named feature is enabled for TopicMapSystem instances 
		/// created by this factory;
		/// </para>
		/// <para>
		/// false if the named feature is disabled for TopicMapSystem instances 
		/// created by this factory.
		/// </para>
		/// <para>
		/// the bool value for the feature as it was set when this 
		/// TopicMapSystem instance was created.
		/// </para>
		/// </returns>
		/// <exception cref="FeatureNotRecognizedException">
		/// if the underlying implementation does not recongnize the named feature.
		/// </exception>
		bool GetFeature(string featureName);

		/// <summary>
		/// Applications should call this method when the 
		/// <see cref="ITopicMapSystem"/> instance is no longer required.
		/// </summary>
		/// <remarks>
		/// <para>
		/// Once the TopicMapSystem instance is closed, the TopicMapSystem 
		/// and any object retrieved from or created in this TopicMapSystem 
		/// must not be used by the application.
		/// </para>
		/// <para>
		/// An implementation of the TopicMapSystem interface may use this 
		/// method to clean up any resources used by the implementation.
		/// </para>
		/// </remarks>
		void Close();
	}
}