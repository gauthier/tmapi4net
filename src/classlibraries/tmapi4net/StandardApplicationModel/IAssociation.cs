namespace tmapi4net.StandardApplicationModel {
	
	
	public interface IAssociation : IReifiable,IScopable {
		ITopic Type { get; }

		ISet<IAssociationRole> Roles { get; }
		ITopicMap Parent { get;}
	
	
	}
}