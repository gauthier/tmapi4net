namespace tmapi4net.StandardApplicationModel {
	public interface IScopable : ITopicMapConstruct {
		ISet<ITopic> Scope { get; }
	}
}