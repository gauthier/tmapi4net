namespace tmapi4net.StandardApplicationModel {
	public interface IVariant : IReifiable, IScopable {

		string Value { get; }
		ILocator Reference { get; }
		ITopicName Parent { get; }
	}
}