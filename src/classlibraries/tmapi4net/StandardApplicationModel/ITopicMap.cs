namespace tmapi4net.StandardApplicationModel {
	public interface ITopicMap : IReifiable {

		ISet<ITopic> Topics { get; }
		ISet<IAssociation> Associations { get;}
		
	
	}
}