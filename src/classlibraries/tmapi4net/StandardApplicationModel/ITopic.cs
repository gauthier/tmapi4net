namespace tmapi4net.StandardApplicationModel {
	public interface ITopic : ITopicMapConstruct {

		/// <summary>
		/// The locators referring to the information resource that is the subject of this topic.
		/// </summary>
		ISet<ILocator> SubjectLocators { get;}

		/// <summary>
		/// The locators referring to the subject indicators of this topic.
		/// </summary>
		ISet<ILocator> SubjectIdentifiers { get;}

		/// <summary>
		/// The topic map containing the topic.
		/// </summary>
		ITopicMap Parent { get; }
		
		/// <summary>
		/// A set of topic name items. 
		/// </summary>
		/// <remarks>
		/// This is the set of topic names assigned to this topic.
		/// </remarks>
		ISet<ITopicName> TopicNames { get; }


		/// <summary>
		/// A set of occurrence items. 
		/// </summary>
		/// <remarks>
		/// This is the set of occurrences assigned to this topic.
		/// </remarks>
		ISet<IOccurrence> Occurrences { get; }

		/// <summary>
		/// A set of association role items.
		/// </summary>
		/// <remarks>
		/// This is the set of association roles played by this topic.
		/// <para>
		/// Computed value: the set of all association role items whose [player] property value is this topic item.
		/// </para>
		/// </remarks>
		ISet<IAssociationRole> RolesPlayed { get; }


		/// <summary>
		/// An information item, or null. 
		/// </summary>
		/// <remarks>
		/// If given, the topic map construct that is reified by this topic.
		/// <para>
		/// Computed value: the information item whose [reifier] property contains this topic item.
		/// </para>
		/// </remarks>
		ITopicMapConstruct Reified { get; }
	}
}