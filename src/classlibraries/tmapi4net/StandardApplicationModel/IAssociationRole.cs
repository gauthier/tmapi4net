namespace tmapi4net.StandardApplicationModel {
	public interface IAssociationRole : IReifiable {
		ITopic Player { get;}
		ITopic Type { get;}
		IAssociation Parent { get; }
	
	}
}