namespace tmapi4net.StandardApplicationModel {
	public interface IOccurrence : IReifiable, IScopable {
		string Value { get; }
		ILocator Reference { get; }
		ITopic Parent { get; }
		ITopic Type { get;}
	}
}