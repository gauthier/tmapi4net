namespace tmapi4net.StandardApplicationModel {
	public interface ITopicName : IReifiable, IScopable {


		string Value { get;}
		ITopic Type { get; }

		ISet<IVariant> Variants { get;}
		ITopic Parent { get; }
	
	
	}
}