using System.Collections.Generic;

namespace tmapi4net.StandardApplicationModel {

	public interface ISet<T> : ICollection<T> { 

	}

	public interface ILocator { 
	
	}


	public interface ITopicMapConstruct {

		/// <summary>
		/// A set of locators. 
		/// </summary>
		/// <remakrs>
		/// The item identifiers of the topicmap construct.
		/// </remakrs>
		ISet<ILocator> ItemIdentifiers { get;}
	}
}
