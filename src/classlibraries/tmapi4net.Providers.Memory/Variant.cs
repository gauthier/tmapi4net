namespace tmapi4net.Providers.Memory {
	using System;
	using System.Collections.Generic;
	using tmapi4net.Core;

	internal class Variant : ScopedObject, IVariant {
		internal Variant(TopicName baseName, ILocator resourceRef)
			: base((TopicMap) ((ITopicName) baseName).TopicMap) {
			resourceRef_ = resourceRef;
			baseName_ = baseName;
		}


		internal Variant(ITopicName baseName, string resourceData, IEnumerable<ITopic> scope)
			: base((TopicMap) baseName.TopicMap, scope) {
			resourceData_ = resourceData;
			baseName_ = baseName;
		}

		private ITopicName baseName_;
		private string resourceData_;
		private ILocator resourceRef_;
		private IEnumerable<ITopic> scope_;

		#region IVariant Members

		ITopicName IVariant.TopicName {
			get { throw new Exception("The method or operation is not implemented."); }
		}

		string IVariant.Value {
			get { return resourceData_; }
			set { resourceData_ = value; }
		}

		ILocator IVariant.Resource {
			get { return resourceRef_; }
			set { resourceRef_ = value; }
		}


		void IVariant.Remove() {
			remove();
		}

		IEnumerable<ITopic> IVariant.Scope {
			get { throw new Exception("The method or operation is not implemented."); }
		}

		#endregion

		protected override void remove() {
			baseName_.Variants.Remove(this);
			clean();
		}

		private void clean() {
			topicMap_.remove(this);
			baseName_ = null;
			resourceData_ = null;
			resourceRef_ = null;
			scope_ = null;
			base.remove();
		}
	}
}