namespace tmapi4net.Providers.Memory {
	using System;
	using System.Collections.Generic;
	using tmapi4net.Core;
	using tmapi4net.Core.Exceptions;

	public class TopicMapSystemFactory : Core.TopicMapSystemFactory, ITopicMapSystemFactory {
		private static readonly IDictionary<string, FeatureSupport> supportedFeatures_;


		private IDictionary<string, bool> featuresValues_;

		static TopicMapSystemFactory() {
			supportedFeatures_ = new Dictionary<string, FeatureSupport>();

			FeatureSupport[] features = new FeatureSupport[]
				{
					new FeatureSupport("http://tmapi.org/features/notation/URI", true, true),
					new FeatureSupport("http://tmapi.org/features/model/xtm1.0", false, true),
					new FeatureSupport("http://tmapi.org/features/model/xtm1.1", false, false),
					new FeatureSupport("http://tmapi.org/features/automerge", false, false),
					new FeatureSupport("http://tmapi.org/features/merge/byTopicName", false, false),
					new FeatureSupport("http://tmapi.org/features/readOnly", true, false)
				};

			foreach (FeatureSupport f in features) {
				supportedFeatures_.Add(f.FeatureName, f);
			}
		}


		public TopicMapSystemFactory() {
			featuresValues_ = new Dictionary<string, bool>();
			foreach (FeatureSupport fs in supportedFeatures_.Values) {
				featuresValues_.Add(fs.FeatureName, fs.DefaultValue);
			}
		}

		#region ITopicMapSystemFactory Members

		ITopicMapSystem ITopicMapSystemFactory.NewTopicMapSystem() {
			return new TopicMapSystem(featuresValues_);
		}

		bool ITopicMapSystemFactory.IsFeatureSupported(string featureName) {
			return supportedFeatures_.ContainsKey(featureName);
		}

		bool ITopicMapSystemFactory.GetFeature(string featureName) {
			return supportedFeatures_.ContainsKey(featureName);
		}

		void ITopicMapSystemFactory.SetFeature(string featureName, bool value) {
			FeatureSupport fs = getFeatureSupport(featureName);
			if (fs.FixedValue) {
				if (fs.DefaultValue != value) {
					throw FeatureNotSupportedException.CreateWithFeatureNameAndFixedValue(featureName, fs.DefaultValue);
				}
			}
			else {
				if (fs.FeatureName != null) {
					featuresValues_[featureName] = value;
				}
			}
		}

		private FeatureSupport getFeatureSupport(string featureName) {
			if (supportedFeatures_.ContainsKey(featureName))
				return supportedFeatures_[featureName];
			throw FeatureNotRecognizedException.CreateWithFeatureName(featureName);
		}


		string ITopicMapSystemFactory.GetProperty(string propertyName) {
			throw new Exception("The method or operation is not implemented.");
		}

		void ITopicMapSystemFactory.SetProperty(string propertyName, string value) {
			throw new Exception("The method or operation is not implemented.");
		}

		#endregion
	}
}