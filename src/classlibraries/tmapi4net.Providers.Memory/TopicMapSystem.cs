namespace tmapi4net.Core {
	using System.Collections.Generic;
}

namespace tmapi4net.Providers.Memory {
	using System;
	using System.Collections.Generic;
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;
	using tmapi4net.Core.Exceptions;

	internal class TopicMapSystem : TopicMapSystemBase, ITopicMapSystem {
		private IDictionary<ILocator, ITopicMap> topicMaps_;


		internal TopicMapSystem(IDictionary<string, bool> features) : base(features) {
			topicMaps_ = new Dictionary<ILocator, ITopicMap>();

		}

		internal void unsubscribe(ITopicMap topicMap) {
			topicMaps_.Remove(topicMap.BaseLocator);
		}

		#region ITopicMapSystem Members

		ITopicMap ITopicMapSystem.CreateTopicMap(string baseLocatorReference, string baseLocatorNotation) {
			throw new Exception("The method or operation is not implemented.");
		}

		ITopicMap ITopicMapSystem.CreateTopicMap(string baseLocatorAddress) {
			ILocator baselocator = new Locator(baseLocatorAddress);
			if (topicMaps_.ContainsKey(baselocator))
				throw TopicMapExistsException.CreateByLocatorAddress(baseLocatorAddress);
			ITopicMap topicmap = new TopicMap(this, baselocator);
			topicMaps_.Add(baselocator, topicmap);
			return topicmap;
		}

		ITopicMap ITopicMapSystem.GetTopicMap(string baseLocatorReference, string baseLocatorNotation) {
			throw new Exception("The method or operation is not implemented.");
		}

		ITopicMap ITopicMapSystem.GetTopicMap(string baseLocatorReference) {
			throw new Exception("The method or operation is not implemented.");
		}

		ITopicMap ITopicMapSystem.GetTopicMap(ILocator baseLocator) {
			return topicMaps_[baseLocator];
		}

		IReadOnlySet<ILocator> ITopicMapSystem.BaseLocators {
			get {
				ISet<ILocator> locators = new LocatorSet();
				foreach (ILocator locator in topicMaps_.Keys)
					locators.Add(locator);
				return locators;
			}
		}

		string ITopicMapSystem.GetProperty(string propertyName) {
			throw new Exception("The method or operation is not implemented.");
		}

		void ITopicMapSystem.Close() {}

		#endregion
	}
}