namespace tmapi4net.Providers.Memory {
	using System.Collections.Generic;
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;
	using System;


	internal abstract class ReifiedObject : TopicMapObject, IReifiable {
		internal ReifiedObject(string id) : base(id) { }
		internal ReifiedObject(TopicMap topicMap, string id) : base(topicMap, id) { }
		internal ReifiedObject(TopicMap topicMap) : base(topicMap, null){}
		#region IHasReifier Members

		ITopic IReifiable.Reifier {
			get {
				foreach (ILocator locator in sourceLocators_) {
					ITopic topic = getTopicBySubjectIdentifier(locator);
					if (topic != null)
						return topic;
				}
				return null;
				;
			}
		}

		internal virtual ITopic getTopicBySubjectIdentifier(ILocator subjectIdentifier) {
			return topicMap_.getTopicBySubjectIdentifier(subjectIdentifier);
		}

		#endregion
	}
	internal class TopicMapObject : ITopicMapObject {
		#region constructors

		internal TopicMapObject(string id) {
			//only for topicmap constructor
			objectId_ = id;
			sourceLocators_ = new LocatorSet();
		}

		internal TopicMapObject(TopicMap topicMap, string id) : this(id) {
			if (topicMap != null) {
				topicMap_ = topicMap;
				objectId_ = id == null ? ((TopicMap)topicMap_).generateUUID() : id;
				((TopicMap)topicMap_).subscribe(this);
			}
		}

		#endregion

		#region attributes

		protected ISet<ILocator> sourceLocators_;
		private string objectId_;

		protected IDictionary<ILocator, ITopicMapObject> objectsBySourceLocators_ =
			new Dictionary<ILocator, ITopicMapObject>();

		protected TopicMap topicMap_;

		#endregion

		#region ITopicMapObject Members

		ITopicMap ITopicMapObject.TopicMap {
			get { return topicMap_; }
		}

		IReadOnlySet<ILocator> ITopicMapObject.SourceLocators {
			get { return createReadOnlyLocatorSet(sourceLocators_); }
		}

		void ITopicMapObject.AddSourceLocator(ILocator sourceLocator) {
			topicMap_.addSourceLocator(this, sourceLocator, true);
			sourceLocators_.Add(sourceLocator);
		}

		void ITopicMapObject.RemoveSourceLocator(ILocator sourceLocator) {
			sourceLocators_.Remove(sourceLocator);
		}

		void ITopicMapObject.Remove() {
			remove();
		}

		string ITopicMapObject.ObjectId {
			get { return objectId_; }
		}

		bool ITopicMapObject.Equals(object o) {
			if (o is ITopicMapObject) {
				ITopicMapObject topicMapObject = (ITopicMapObject) o;
				return selfAsITopicMapObject.ObjectId == topicMapObject.ObjectId;
			}
			return false;
		}

		int ITopicMapObject.GetHashCode() {
			return selfAsITopicMapObject.ObjectId.GetHashCode();
		}

		#endregion

		#region internal members

		protected ITopicMapObject selfAsITopicMapObject {
			get { return this; }
		}

		protected virtual void remove() {
			((TopicMap) topicMap_).unsubscribe(this);
			objectsBySourceLocators_ = null;
			objectId_ = null;
			topicMap_ = null;
		}


		protected IReadOnlySet<ILocator> createReadOnlyLocatorSet(IEnumerable<ILocator> locators) {
			if (locators == null)
				return new LocatorSet();
			return new LocatorSet(locators);
		}

		protected IReadOnlySet<ITopic> createReadOnlyTopicSet(ISet<ITopic> topics) {
			return topics;
		}

		protected IReadOnlySet<ITopicName> createReadOnlyTopicNameSet(ISet<ITopicName> topicNames) {
			return topicNames;
		}

		protected IReadOnlySet<IAssociation> createReadOnlyAssociationSet(ISet<IAssociation> associations) {
			return associations;
		}

		protected IReadOnlySet<IAssociationRole> createReadOnlyAssociationRoleSet(ISet<IAssociationRole> associationRoles) {
			return associationRoles;
		}


		protected IReadOnlySet<IOccurrence> createReadOnlyOccurrenceSet(ISet<IOccurrence> occurrences) {
			return occurrences;
		}

		protected IReadOnlySet<IVariant> createReadOnlyVariantSet(ISet<IVariant> variants) {
			if (variants == null)
				return new VariantSet();
			return variants;
		}

		#endregion


		#region IComparable<ITopicMapObject> Members

		int IComparable<ITopicMapObject>.CompareTo(ITopicMapObject other) {
			return this.objectId_.CompareTo(other.ObjectId);
		}

		#endregion

		#region IComparable Members

		int IComparable.CompareTo(object obj) {
			if (obj == null) return 1;
			else if (obj is ITopicMapObject) return objectId_.CompareTo(((ITopicMapObject)obj).ObjectId);
			else return 1;
		}

		#endregion
		#region IEquatable<ITopicMapObject> Members

		bool System.IEquatable<ITopicMapObject>.Equals(ITopicMapObject other) {
			return selfAsITopicMapObject.Equals((object) other);
		}

		#endregion
		protected ITopic Reifier {
			get {
				foreach (ILocator locator in sourceLocators_) {
					ITopic topic = topicMap_.getTopicBySubjectIdentifier(locator);
					if (topic != null)
						return topic;
				}
				return null;
			}
		}

	}
}