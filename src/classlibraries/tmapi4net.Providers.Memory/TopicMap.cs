namespace tmapi4net.Providers.Memory {
	using System;
	using System.Collections.Generic;
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;
	using tmapi4net.Core.Exceptions;

/*
	enum MergeMode { 
		None,
		Automerge,

	}*/


	internal class TopicMap : ReifiedObject, ITopicMap {
		#region attributes

		private long uuid_;
		private int mergemode_;
		private IDictionary<ILocator, ITopic> subjectLocators_;
		private IDictionary<ILocator, ITopic> subjectIdentifiers_;
		private bool xtm11_;
		private ILocator baseLocator_;
		private ISet<IAssociation> associations_;
		private ITopicMapSystem topicMapSystem_;
		private ISet<ITopic> topics_;
		private IDictionary<string, ITopicMapObject> objectsById_;
		private IDictionary<Type, Type> helperObjectTypes_;
		#endregion

		internal bool xtm11 {
			get { return xtm11_; }
		}

		#region constructors

		internal TopicMap(ITopicMapSystem system, ILocator baseLocator)
			: base(baseLocator.Reference) {
			base.topicMap_ = this;
			topicMapSystem_ = system;
			baseLocator_ = baseLocator;
			objectsById_ = new Dictionary<string, ITopicMapObject>();
			topics_ = new TopicSet();

			subjectIdentifiers_ = new Dictionary<ILocator, ITopic>();
			subjectLocators_ = new Dictionary<ILocator, ITopic>();
			associations_ = new AssociationSet();
#if DEBUG
			//set merge mode
			try {
				if (topicMapSystem_.GetFeature("http://tmapi.org/features/automerge"))
					mergemode_ = 3;
				else
					mergemode_ = 2;

				/*this.TopicNameMerge= this.tmSystem.GetFeature("http://tmapi.org/features/merge/byTopicName");*/
				xtm11_ = topicMapSystem_.GetFeature("http://tmapi.org/features/model/xtm1.1");


				/*System.out.println("TopicMap with mergemode: "+this.mergemode+" and merging by TopicName : "+this.TopicNameMerge +" and xtm 1.1: "+this.xtm11);*/
			}
			catch (FeatureNotRecognizedException e) {
				throw new TMAPIRuntimeException(e);
			}
#else
			throw new NotImplementedException();
#endif
		}

		#endregion

		#region ITopicMap Members

		ITopicMapSystem ITopicMap.TopicMapSystem {
			get { return topicMapSystem_; }
		}

		ILocator ITopicMap.BaseLocator {
			get { return baseLocator_; }
		}

		IReadOnlySet<IAssociation> ITopicMap.Associations {
			get { return createReadOnlyAssociationSet(associations_); }
		}

		IAssociation ITopicMap.CreateAssociation() {
			IAssociation association = new Association(this);
			associations_.Add(association);
			return association;
		}

		IReadOnlySet<ITopic> ITopicMap.Topics {
			get { return createReadOnlyTopicSet(topics_); }
		}

		ITopic ITopicMap.CreateTopic() {
			ITopic topic = new Topic(this);
			topics_.Add(topic);
			return topic;
		}

		ILocator ITopicMap.CreateLocator(string address, string notation) {
			return new Locator(address, notation);
		}

		ILocator ITopicMap.CreateLocator(string address) {
			return new Locator(address);
		}
		/*
		ITopic ITopicMap.Reifier {
			get {
				foreach (ILocator locator in sourceLocators_) {
					ITopic topic = getTopicBySubjectIdentifier(locator);
					if (topic != null)
						return topic;
				}
				return null;
			}
		}*/

		void ITopicMap.Remove() {
			((TopicMapSystem) topicMapSystem_).unsubscribe(this);
		}

		ITopicMapObject ITopicMap.GetObjectById(string objectId) {
			if (objectsById_.ContainsKey(objectId))
				return objectsById_[objectId];
			return null;
		}

		void ITopicMap.MergeIn(ITopicMap other) {
			bool merge = false;
/*
			foreach (ITopic topic in other.Topics) {
				merge = false;

				foreach (ITopic topic2 in selfAsITopicMap.Topics) {
					if (topic.SubjectLocators.Count > 0 && topic2.SubjectLocators.Count > 0) {
						if (topic.SubjectLocators[0].Equals(topic2.SubjectLocators[0])) {
							merge = true;
						}
					}
				}
			}*/
			throw new Exception("The method or operation is not implemented.");
		}

		TInterface ITopicMap.GetHelperObject<TInterface>() {
			throw new Exception("The method or operation is not implemented.");
		}

		void ITopicMap.Close() {
			throw new Exception("The method or operation is not implemented.");
		}

		#endregion

		#region private members

		public void removeSourceLocator(ITopicMapObject topicMapObject, ILocator sourceLocator) {
			sourceLocators_.Remove(sourceLocator);
		}

		internal void unsubscribe(ITopicMapObject topicMapObject) {
			objectsById_.Remove(topicMapObject.ObjectId);
		}

		internal void hasChanged(IAssociationRole associationRole) {}


		internal void hasChanged(IOccurrence occurrence) {}


		internal void hasChanged(ITopicName topicName) {}

		internal void remove(IAssociation association) {
			associations_.Remove(association);
		}

		internal void remove(IAssociationRole role) {}
		internal void remove(IOccurrence occurrence) {}

		internal void remove(ITopicName topicName) {}

		internal void remove(IVariant variant) {}

		internal void remove(ITopic topic) {
			topics_.Remove(topic);
			removeSubjectLocators(topic, topic.SubjectLocators);
			removeSubjectIdentifiers(topic, topic.SubjectIdentifiers);
		}

		internal string generateUUID() {
			++uuid_;
			return ("id" + uuid_ + ToString());
		}

		internal void subscribe(ITopicMapObject topicMapObject) {
			objectsById_[topicMapObject.ObjectId] = topicMapObject;
		}


		internal bool addSourceLocator(Topic topic, ILocator sourceLocator, bool merging) {
			if (!merging || testSourceLocator(topic, sourceLocator)) {
				objectsBySourceLocators_.Add(sourceLocator, topic);
				return true;
			}
			else
				return false;
		}
		internal bool addSourceLocator(TopicMapObject topicMapObject, ILocator sourceLocator, bool merging) {
			if (!merging || testSourceLocator(topicMapObject, sourceLocator)) {
				objectsBySourceLocators_.Add(sourceLocator, topicMapObject);
				return true;
			}
			else
				return false;
		}


		internal bool testSourceLocator(TopicMapObject topicMapObject, ILocator sourceLocator) {
			ITopicMapObject tmobject = getTopicMapObjectBySourceLocator(sourceLocator);
			if (tmobject != null) {
				throw new DuplicateSourceLocatorException(topicMapObject, tmobject, sourceLocator,
				                                          "Topics cant have the same source locator");
			}
			return true;
		}


		internal bool testSourceLocator(Topic topic, ILocator sourceLocator) {
			ITopicMapObject tmobject = getTopicMapObjectBySourceLocator(sourceLocator);
			if(tmobject != null) {
				if (tmobject is Topic) {
					if (this.mergemode_ == 3) {
						try {
							(tmobject as ITopic).MergeIn(topic);
							return false;
						} catch(MergeException e) {
							throw new TMAPIRuntimeException(e);
						}
					}
					else {
						throw new TopicsMustMergeException(topic, (Topic) tmobject,
						                                   String.Format("Same source locator was found: {0}", sourceLocator.Reference));
					}
				} else {
					throw new DuplicateSourceLocatorException(tmobject, topic, sourceLocator, "same sourcelocator");
				}
			}
			return true;

		}
		internal ITopicMapObject getTopicMapObjectBySourceLocator(ILocator sourceLocator) {
			if (objectsBySourceLocators_.ContainsKey(sourceLocator))
				return objectsBySourceLocators_[sourceLocator];
			else
				return null;
		}


		public bool addSubjectLocator(Topic topic, ILocator locator, bool merging) {
			if (!merging || testSubjectLocator(topic, locator)) {
				subjectLocators_.Add(locator, topic);
				return true;
			}
			else
				return false;
		}


		private bool testSubjectLocator(ITopic topic, ILocator locator) {
			ITopic t1 = getTopicBySubjectLocator(locator);
			if ((t1 != null) && t1 != topic) {
				if (mergemode_ == 3) {
					t1.MergeIn(topic);
					return false;
				}
				else
					throw new TopicsMustMergeException(topic, t1,
					                                   string.Format(
					                                   	"Same subject locator \"{0}\" Topic {1} and Topic {2} must be merged",
					                                   	locator.Reference, topic.ObjectId, t1.ObjectId));
			}
			return true;
		}

		private ITopic getTopicBySubjectLocator(ILocator locator) {
			if (!subjectLocators_.ContainsKey(locator))
				return null;
			return subjectLocators_[locator];
		}

		public bool AddSubjectIdentifier(Topic topic, ILocator subjectIdentifier, bool b) {
			if (!b || testSubjectIdentifier(topic, subjectIdentifier)) {
				subjectIdentifiers_.Add(subjectIdentifier, topic);
				return true;
			}
			return false;
		}

		private bool testSubjectIdentifier(Topic topic, ILocator subjectIdentifier) {
			ITopic t1 = getTopicBySubjectIdentifier(subjectIdentifier);
			if (t1 != null) {
				if (mergemode_ == 3) {
					t1.MergeIn(topic);
					return false;
				}
				else
					throw new TopicsMustMergeException(topic, t1,
					                                   string.Format("Same subject identifier \"{0}\"", subjectIdentifier.Reference));
			}
			return true;
		}

		internal override ITopic getTopicBySubjectIdentifier(ILocator subjectIdentifier) {
			if (subjectIdentifiers_.ContainsKey(subjectIdentifier))
				return subjectIdentifiers_[subjectIdentifier];
			return null;
		}


		private void removeSubjectIdentifiers(ITopic topic, IEnumerable<ILocator> locators) {
			foreach (ILocator locator in locators) {
				removeSubjectIdentifier(topic, locator);
			}
		}

		private void removeSubjectLocators(ITopic topic, IEnumerable<ILocator> locators) {
			foreach (ILocator locator in locators) {
				removeSubjectLocator(topic, locator);
			}
		}

		private void removeSubjectLocator(ITopic topic, ILocator locator) {
			subjectLocators_.Remove(locator);
		}

		private void removeSubjectIdentifier(ITopic topic, ILocator locator) {
			subjectIdentifiers_.Remove(locator);
		}

		#endregion
	}
}