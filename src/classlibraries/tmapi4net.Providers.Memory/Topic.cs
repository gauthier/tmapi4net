namespace tmapi4net.Providers.Memory {
	using System;
	using System.Collections.Generic;
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;
	using tmapi4net.Core.Exceptions;

	internal class Topic : TopicMapObject, ITopic {
		#region constructors

		internal Topic(TopicMap topicMap)
			: this(topicMap, null) {}

		internal Topic(TopicMap topicMap, string id)
			: base(topicMap, id) {
			subjectAddresses_ = new LocatorSet();
			types_ = new TopicSet();
			rolesPlayed_ = new AssociationRoleSet();
			subjectIdentifiers_ = new LocatorSet();
		}

		#endregion

		#region attributes

		private ISet<ILocator> subjectIdentifiers_;
		private ISet<IAssociationRole> rolesPlayed_;
		private ISet<ITopic> types_;
		private ISet<ILocator> subjectAddresses_;
		private bool merging_ = false;
		private ISet<IOccurrence> occurences_ = new OccurrenceSet();
		private ISet<ITopicName> topicNames_ = new TopicNameSet();

		#endregion

		#region ITopic Members

		IReadOnlySet<ITopicName> ITopic.TopicNames {
			get {
				return createReadOnlyTopicNameSet(topicNames_);
				throw new Exception("The method or operation is not implemented.");
			}
		}

		ITopicName ITopic.CreateTopicName(string topicName, IEnumerable<ITopic> scope) {
			ITopicName basename = new TopicName(this, topicName, scope);
			topicNames_.Add(basename);
			return basename;
		}

		ITopicName ITopic.CreateTopicName(string topicName, ITopic type, IEnumerable<ITopic> scope) {
			throw new Exception("The method or operation is not implemented.");
		}

		IOccurrence ITopic.CreateOccurrence(string occurrenceData, ITopic type, IEnumerable<ITopic> scope) {
			IOccurrence occurrence = new Occurrence(this, occurrenceData, scope);
			if (type != null)
				occurrence.Type = type;
			occurences_.Add(occurrence);
			return occurrence;
		}

		IOccurrence ITopic.CreateOccurrence(ILocator resource, ITopic type, IEnumerable<ITopic> scope) {
			IOccurrence occurrence = new Occurrence(this, resource, scope);
			if (type != null)
				occurrence.Type = type;
			occurences_.Add(occurrence);
			return occurrence;
		}

		IReadOnlySet<IOccurrence> ITopic.Occurrences {
			get {
				return createReadOnlyOccurrenceSet(occurences_);
			}
		}

		IReadOnlySet<ILocator> ITopic.SubjectLocators {
			get {
				return createReadOnlyLocatorSet(subjectAddresses_);
			}
		}

		void ITopic.AddSubjectLocator(ILocator subjectLocator) {
			if (topicMap_.addSubjectLocator(this, subjectLocator, !merging_))
				subjectAddresses_.Add(subjectLocator);
		}

		void ITopic.RemoveSubjectLocator(ILocator subjectLocator) {
			throw new Exception("The method or operation is not implemented.");
		}

		IReadOnlySet<ILocator> ITopic.SubjectIdentifiers {
			get { return subjectIdentifiers_; }
		}

		void ITopic.AddSubjectIdentifier(ILocator subjectIdentifier) {
			if (topicMap_.AddSubjectIdentifier(this, subjectIdentifier, !merging_))
				subjectIdentifiers_.Add(subjectIdentifier);
		}

		void ITopic.RemoveSubjectIdentifier(ILocator subjectIdentifier) {
			throw new Exception("The method or operation is not implemented.");
		}

		IReadOnlySet<ITopic> ITopic.Types {
			get { return createReadOnlyTopicSet(types_); }
		}

		void ITopic.AddType(ITopic type) {
			types_.Add(type);
		}

		void ITopic.RemoveType(ITopic type) {
			types_.Remove(type);
		}

		IReadOnlySet<IAssociationRole> ITopic.RolesPlayed {
			get { return createReadOnlyAssociationRoleSet(rolesPlayed_); }
		}

		void ITopic.Remove() {
			remove();
		}

		void ITopic.MergeIn(ITopic other) {
		
			this.merging_ = true;

			
			{
				// compare subject locators equality
				if (selfAsITopic.SubjectLocators.Count > 0) {
					if (other.SubjectLocators.Count > 0) {


					}
				}
				//throw new Exception("The method or operation is not implemented.");

			}


			// merge association roles
			{
				foreach(IAssociationRole role in other.RolesPlayed) {
					if(!this.rolesPlayed_.Contains(role)) {
						if(role.Player == other) {
							role.Player = this;
						}
					}
				}

			}

			// merge types
			{
				foreach (ITopic type in other.Types) {
					if(!this.types_.Contains(type))
						selfAsITopic.AddType(type);
				}
			}

			// merge subject locators
			{
				foreach (ILocator locator in other.SourceLocators) {
					if(!this.sourceLocators_.Contains(locator))
						selfAsITopic.AddSourceLocator(locator);
				}
			}

			// merge subject indicators
			{
				foreach (ILocator locator in other.SubjectIdentifiers) {
					if (!this.subjectIdentifiers_.Contains(locator))
						selfAsITopic.AddSubjectIdentifier(locator);
				}
			}

			//merge topic names
			{
				foreach (ITopicName topicName in other.TopicNames) {
					if (!this.topicNames_.Contains(topicName))
						selfAsITopic.CreateTopicName(topicName.Value, topicName.Type, topicName.Scope);
				}
			}
			// merge occurrences
			{
				foreach (IOccurrence occurrence in other.Occurrences) {
					if (!this.occurences_.Contains(occurrence)) {
						if (false == String.IsNullOrEmpty(occurrence.Value))
							selfAsITopic.CreateOccurrence(occurrence.Value, occurrence.Type, occurrence.Scope);
						else if (null != occurrence.Resource)
							selfAsITopic.CreateOccurrence(occurrence.Resource, occurrence.Type, occurrence.Scope);
					}
				}			
			}

			this.merging_ = false;

			
				try {
					other.Remove();
				}
				catch(TopicInUseException e) {
					throw new TMAPIRuntimeException(e);
				}
			//throw new Exception("The method or operation is not implemented.");

		}

		IReadOnlySet<ITopicMapObject> ITopic.Reified {
			get {
				ISet<ITopicMapObject> reified = new TopicMapObjectSet();
				foreach (ILocator locator in subjectIdentifiers_) {
					ITopicMapObject obj = topicMap_.getTopicMapObjectBySourceLocator(locator);
					if (obj != null)
						reified.Add(obj);
				}
				return reified;
			}
		}

		void ITopic.AddSourceLocator(ILocator sourceLocator) {
			if (topicMap_.addSourceLocator(this, sourceLocator, !merging_))
				sourceLocators_.Add(sourceLocator);
		}

		#endregion

		internal void removeRolePlayed(IAssociationRole role) {
			rolesPlayed_.Remove(role);
		}

		internal void addRolePlayed(AssociationRole role) {
			rolesPlayed_.Add(role);
		}

		internal TopicMap TopicMap {
			get { return base.topicMap_; }
		}

		protected override void remove() {
			topicMap_.remove(this);
			try {
				foreach (IAssociationRole role in rolesPlayed_) {
					role.Remove();
				}

				foreach (ITopicName name in topicNames_) {
					name.Remove();
				}

				foreach (IOccurrence occurrence in occurences_) {
					occurrence.Remove();
				}

				topicNames_ = null;
				occurences_ = null;
				rolesPlayed_ = null;
				subjectAddresses_ = null;
				subjectIdentifiers_ = null;
				types_ = null;
				base.remove();
			}
			catch (TMAPIException e) {
				throw new TopicInUseException(e);
			}
		}

		ITopic selfAsITopic{get{ return this;}}
	}
}