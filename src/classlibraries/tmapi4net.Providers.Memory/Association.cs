namespace tmapi4net.Providers.Memory {
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;

	internal class Association : ScopedObject, IAssociation {
		private ITopic type_;
		private ISet<IAssociationRole> members_;


		internal Association(TopicMap topicMap) : base(topicMap, (string) null) {
			members_ = new AssociationRoleSet();
		}

		#region IAssociation Members

		public ITopic Type {
			get { return type_; }
			set { type_ = value; }
		}

		IAssociationRole IAssociation.CreateAssociationRole(ITopic player, ITopic roleSpec) {
			AssociationRole role = new AssociationRole(this, (Topic) player, (Topic) roleSpec);
			members_.Add(role);
			return role;
		}

		IReadOnlySet<IAssociationRole> IAssociation.AssociationRoles {
			get { return createReadOnlyAssociationRoleSet(members_); }
		}


		void IAssociation.Remove() {
			remove();
		}

		#endregion

		protected override void remove() {
			topicMap_.remove(this);
			ISet<IAssociationRole> roles = new AssociationRoleSet(members_);


			foreach (IAssociationRole role in roles) {
				role.Remove();
			}
			base.remove();
		}
	}
}