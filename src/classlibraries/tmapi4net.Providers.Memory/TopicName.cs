namespace tmapi4net.Providers.Memory {
	using System;
	using System.Collections.Generic;
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;
	using tmapi4net.Core.Exceptions;

	internal class TopicName : ScopedObject, ITopicName {
		internal TopicName(Topic topic, string topicName, IEnumerable<ITopic> scope) : base(topic.TopicMap, scope) {
			value_ = topicName;
			topic_ = topic;
			variants_ = new VariantSet();
		}

		private string value_;
		private Topic topic_, type_;
		private ISet<IVariant> variants_;

		#region ITopicName Members

		ITopic ITopicName.Topic {
			get { return topic_; }
		}

		string ITopicName.Value {
			get { return value_; }
			set {
				value_ = value;
				topicMap_.hasChanged(this);
			}
		}

		IReadOnlySet<IVariant> ITopicName.Variants {
			get {
				return createReadOnlyVariantSet(variants_);
				throw new Exception("The method or operation is not implemented.");
			}
		}

		IVariant ITopicName.CreateVariant(string resourceData, IEnumerable<ITopic> scope) {
			IVariant variant = new Variant(this, resourceData, scope);
			variants_.Add(variant);
			topicMap_.hasChanged(this);
			return variant;
		}

		IVariant ITopicName.CreateVariant(ILocator resource, IEnumerable<ITopic> scope) {
			throw new Exception("The method or operation is not implemented.");
		}

		public ITopic Type {
			get { return type_; }
			set {
				if (!topicMap_.xtm11)
					throw new UnsupportedOperationException(
						"The TopicMap engine is operating in xtm 1.0 mode, so no types are allowed for topic names");
				type_ = (Topic) value;
				topicMap_.hasChanged(this);
			}
		}

		void ITopicName.Remove() {
			((ITopic) topic_).TopicNames.Remove(this);
			topicMap_.hasChanged(this);
			clean();
		}

		void ITopicName.AddScopingTopic(ITopic topic) {
			throw new Exception("The method or operation is not implemented.");
		}

		void ITopicName.RemoveScopingTopic(ITopic topic) {
			throw new Exception("The method or operation is not implemented.");
		}

		#endregion

		private void clean() {
			topicMap_.remove(this);
			topic_ = null;
			value_ = null;
			variants_ = null;
			type_ = null;
			base.remove();
		}
	}
}