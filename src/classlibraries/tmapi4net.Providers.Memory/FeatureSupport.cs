namespace tmapi4net.Providers.Memory {
	internal class FeatureSupport {
		internal FeatureSupport(
			string featureName,
			bool fixedFeature,
			bool defaultValue) {
			featureName_ = featureName;
			fixedValue_ = fixedFeature;
			defaultValue_ = defaultValue;
		}

		public FeatureSupport(string featureName, string propertyName, bool defaultValue) {
			featureName_ = featureName;
			fixedValue_ = false;
			defaultValue_ = defaultValue;
			propertyName_ = propertyName;
		}

		public FeatureSupport(string featureName, string propertyName, bool defaultValue,
		                      string trueValue, string falseValue) {
			featureName_ = featureName;
			fixedValue_ = false;
			defaultValue_ = defaultValue;
			propertyName_ = propertyName;
			propertyTrueValue_ = trueValue;
			propertyFalseValue_ = falseValue;
		}

		private string featureName_;
		private string propertyName_;
		private string propertyTrueValue_;
		private string propertyFalseValue_;
		private bool defaultValue_;
		private bool fixedValue_;


		public string PropertyFalseValue {
			get { return propertyFalseValue_; }
		}

		public string PropertyTrueValue {
			get { return propertyTrueValue_; }
		}

		public string FeatureName {
			get { return featureName_; }
		}

		public string PropertyName {
			get { return propertyName_; }
		}

		public bool DefaultValue {
			get { return defaultValue_; }
		}

		public bool FixedValue {
			get { return fixedValue_; }
		}
	}
}