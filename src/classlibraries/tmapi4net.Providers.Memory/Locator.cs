namespace tmapi4net.Providers.Memory {
	using System;
	using tmapi4net.Core;
	using System.Globalization;

	internal class Locator : ILocator {
		internal Locator(string address, string notation) {
			address_ = address;
			notation_ = notation;
		}

		internal Locator(string address) : this(address, "URI") {}


		private string address_, notation_;

		#region ILocator Members

		string ILocator.Reference {
			get { return address_; }
		}

		string ILocator.Notation {
			get { return notation_; }
		}

		ILocator ILocator.ResolveRelative(string relative) {
			throw new Exception("The method or operation is not implemented.");
		}

		string ILocator.ToExternalForm() {
			throw new Exception("The method or operation is not implemented.");
		}

		public override bool Equals(object o) {
			return selfAsILocator.CompareTo(o) == 0;
		}

		public override int GetHashCode() {
			return String.Format("{0} {1}", notation_, address_).GetHashCode();
		}

		#endregion


		#region IComparable<ILocator> Members

		int IComparable<ILocator>.CompareTo(ILocator other) {
			if (other == null)
				return 1;
			int notationdif = CultureInfo.CurrentCulture.CompareInfo.Compare(notation_, other.Notation);
			if (notationdif != 0) return notationdif;
			return CultureInfo.CurrentCulture.CompareInfo.Compare(address_, other.Reference);
		}

		#endregion

		#region IComparable Members

		int IComparable.CompareTo(object obj) {
			if (obj is ILocator) return ((IComparable<ILocator>)this).CompareTo((ILocator)obj);
			return 1;
		}

		#endregion


		ILocator selfAsILocator {get { return this;}}
	}
}