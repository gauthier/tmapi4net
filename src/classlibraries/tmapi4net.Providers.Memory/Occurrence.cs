namespace tmapi4net.Providers.Memory {
	using System.Collections.Generic;
	using tmapi4net.Core;

	internal class Occurrence : ScopedObject, IOccurrence {
		internal Occurrence(Topic topic, string id) : base((TopicMap) ((ITopic) topic).TopicMap, id) {}

		internal Occurrence(Topic topic, string resourceData, IEnumerable<ITopic> scope) : base(topic.TopicMap, scope) {
			resourceData_ = resourceData;
			topic_ = topic;
		}

		internal Occurrence(Topic topic, ILocator resource, IEnumerable<ITopic> scope) : base(topic.TopicMap, scope) {
			resourceLocator_ = resource;
			topic_ = topic;
		}

		private ITopic topic_, type_;
		private string resourceData_;
		private ILocator resourceLocator_;

		#region IOccurrence Members

		ITopic IOccurrence.Topic {
			get { return topic_; }
		}

		public ITopic Type {
			get { return type_; }
			set {
				type_ = value;
				signalChangedToTopicMap();
			}
		}


		string IOccurrence.Value {
			get { return resourceData_; }
			set {
				resourceLocator_ = null;
				resourceData_ = value;
				signalChangedToTopicMap();
			}
		}

		ILocator IOccurrence.Resource {
			get { return resourceLocator_; }
			set {
				resourceLocator_ = value;
				resourceData_ = null;
				signalChangedToTopicMap();
			}
		}

		void IOccurrence.Remove() {
			remove();
		}

		#endregion

		private void signalChangedToTopicMap() {
			topicMap_.hasChanged(this);
		}


		protected override void remove() {
			topic_.Occurrences.Remove(this);
			signalChangedToTopicMap();
			clean();
			base.remove();
		}

		protected void clean() {
			((TopicMap) topicMap_).remove(this);
			topic_ = type_ = null;
			resourceLocator_ = null;
			resourceData_ = null;
		}
	}
}