namespace tmapi4net.Providers.Memory {
	using tmapi4net.Core;

	internal class AssociationRole : ReifiedObject, IAssociationRole {
		private IAssociation association_;
		private Topic player_, roleSpecitifcation_;

		#region constructors

		internal AssociationRole(Association association, Topic player, Topic roleSpec, string id)
			: base((TopicMap) ((IAssociation) association).TopicMap, id) {
			association_ = association;
			player_ = player;
			roleSpecitifcation_ = roleSpec;
			if (player != null)
				player.addRolePlayed(this);
		}

		internal AssociationRole(Association association, Topic player, Topic roleSpec)
			: this(association, player, roleSpec, null) {}

		#endregion

		#region IAssociationRole Members

		IAssociation IAssociationRole.Association {
			get { return association_; }
		}

		ITopic IAssociationRole.Player {
			get { return player_; }
			set {
				if (player_ != null)
					((Topic) player_).removeRolePlayed(this);
			}
		}

		public ITopic Type {
			get { return roleSpecitifcation_; }
			set {
				roleSpecitifcation_ = (Topic) value;
				((TopicMap) topicMap_).hasChanged(this);
			}
		}


		void IAssociationRole.Remove() {
			player_.removeRolePlayed(this);
			clean();
		}

		#endregion

		private void clean() {
			topicMap_.remove(this);
			association_.AssociationRoles.Remove(this);
			topicMap_.hasChanged(this);
			association_ = null;
			player_ = null;
			roleSpecitifcation_ = null;
			base.remove();
		}

		#region IAssociationRole Members


		ITopic IAssociationRole.Reifier {
			get {
				return base.Reifier;
			}
		}

		#endregion

	}
}