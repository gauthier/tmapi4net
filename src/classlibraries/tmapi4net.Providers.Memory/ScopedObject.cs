namespace tmapi4net.Providers.Memory {
	using System.Collections.Generic;
	using tmapi4net.Core;
	using tmapi4net.Core.Collections;

	internal class ScopedObject : ReifiedObject, IScopedObject {
		internal ScopedObject(TopicMap topicMap, string id) : base(topicMap, id) {
			scopingTopics_ = new TopicSet();
		}

		internal ScopedObject(TopicMap topicMap, IEnumerable<ITopic> scope) : this(topicMap, (string) null) {
			if (scope != null)
				scopingTopics_.AddRange(scope);
		}

		internal ScopedObject(TopicMap topicMap) : this(topicMap, (string) null) {}
		private TopicSet scopingTopics_;

		#region IScopedObject Members

		IReadOnlySet<ITopic> IScopedObject.Scope {
			get { return createReadOnlyTopicSet(scopingTopics_); }
		}

		void IScopedObject.AddScopingTopic(ITopic topic) {
			scopingTopics_.Add(topic);
		}

		void IScopedObject.RemoveScopingTopic(ITopic topic) {
			scopingTopics_.Remove(topic);
		}

		#endregion
	}

	internal class TopicSet : List<ITopic>, ISet<ITopic> {}

	internal class LocatorSet : List<ILocator>,ISet<ILocator>{
		internal LocatorSet() { }
		internal LocatorSet(IEnumerable<ILocator> locators) :base(locators){ }
	}

	internal class VariantSet : List<IVariant>, ISet<IVariant> { }
	internal class AssociationSet : List<IAssociation>, ISet<IAssociation> { }
	internal class OccurrenceSet : List<IOccurrence>, ISet<IOccurrence> { }
	internal class AssociationRoleSet : List<IAssociationRole>, ISet<IAssociationRole> { 
	internal AssociationRoleSet() { }
		internal AssociationRoleSet(IEnumerable<IAssociationRole> roles) : base(roles) { }

	}
	internal class TopicNameSet : List<ITopicName>, ISet<ITopicName> { }

	internal class TopicMapObjectSet : List<ITopicMapObject>, ISet<ITopicMapObject> { }
}