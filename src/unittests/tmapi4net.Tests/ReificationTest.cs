namespace tmapi4net.Core.Tests {
	using NUnit.Framework;

	[TestFixture]
	public class ReificationTest : TMAPITest {
		[Test]
		public void TestTopicMap() {
			ITopic t = topicMap_.CreateTopic();

			ILocator loc = topicMap_.CreateLocator("sloc");

			topicMap_.AddSourceLocator(loc);

			t.AddSubjectIdentifier(loc);

			assertEquals("Reified topic of TopicMap is not right", t, topicMap_.Reifier);

			assertEquals("Topic.getReified() does not return the reified TopicMap", true, t.Reified.Contains(topicMap_));
		}

		[Test]
		public void TestTopicNames() {
			ITopic t = topicMap_.CreateTopic();
			ITopic t2 = topicMap_.CreateTopic();
			ILocator loc = topicMap_.CreateLocator("sloc");

			ITopicName bn = t.CreateTopicName("foo", null);
			bn.AddSourceLocator(loc);

			t2.AddSubjectIdentifier(loc);

			assertEquals("Reified topic of ITopicName is not right", t2, bn.Reifier);

			assertEquals("Topic.getReified() does not return the reified TopicName", true, t2.Reified.Contains(bn));
		}

		[Test]
		public void TestOccurrences() {
			ITopic t = topicMap_.CreateTopic();
			ITopic t2 = topicMap_.CreateTopic();
			ILocator loc = topicMap_.CreateLocator("sloc");

			IOccurrence oc = t.CreateOccurrence("foo", null, null);
			oc.AddSourceLocator(loc);

			t2.AddSubjectIdentifier(loc);

			assertEquals("Reified topic of Occurrence is not right", t2, oc.Reifier);

			assertEquals("Topic.getReified() does not return the reified Occurrence", true, t2.Reified.Contains(oc));
		}

		[Test]
		public void TestAssociations() {
			ITopic t = topicMap_.CreateTopic();
			ILocator loc = topicMap_.CreateLocator("sloc");
			t.AddSubjectIdentifier(loc);

			IAssociation a = topicMap_.CreateAssociation();
			a.AddSourceLocator(loc);

			assertEquals("Reified topic of Association is not right", t, a.Reifier);

			assertEquals("Topic.getReified() does not return the reified Association", true, t.Reified.Contains(a));
		}

		[Test]
		public void TestAssociationRoles() {
			ITopic t = topicMap_.CreateTopic();
			ILocator loc = topicMap_.CreateLocator("sloc");
			t.AddSubjectIdentifier(loc);

			IAssociation a = topicMap_.CreateAssociation();
			IAssociationRole ar = a.CreateAssociationRole(null, null);
			ar.AddSourceLocator(loc);

			assertEquals("Reified topic of AssociationRole is not right", t, ar.Reifier);

			assertEquals("Topic.getReified() does not return the reified AssociationRole", true, t.Reified.Contains(ar));
		}

		[Test]
		public void TestReified() {
			ITopic t = topicMap_.CreateTopic();
			ITopic t2 = topicMap_.CreateTopic();
			ILocator loca = topicMap_.CreateLocator("sloca");
			ILocator locar = topicMap_.CreateLocator("slocar");
			ILocator loco = topicMap_.CreateLocator("sloco");
			ILocator locb = topicMap_.CreateLocator("slocb");

			t2.AddSubjectIdentifier(loca);
			t2.AddSubjectIdentifier(locar);
			t2.AddSubjectIdentifier(loco);
			t2.AddSubjectIdentifier(locb);

			IAssociation a = topicMap_.CreateAssociation();
			a.AddSourceLocator(loca);
			IAssociationRole ar = a.CreateAssociationRole(null, null);
			ar.AddSourceLocator(locar);

			ITopicName bn = t.CreateTopicName("foo", null);
			bn.AddSourceLocator(locb);

			IOccurrence oc = t.CreateOccurrence("foo", null, null);
			oc.AddSourceLocator(loco);

			assertEquals("Topic.Reified does not return the right number of reified TopicMapObject", 4, t2.Reified.Count);
		}
	}
}