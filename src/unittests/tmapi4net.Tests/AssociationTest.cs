namespace tmapi4net.Core.Tests {
	using NUnit.Framework;

	[TestFixture]
	public class AssociationTest : ScopedObjectTest {
		[Test]
		public void TestType() {
			IAssociation assoc = topicMap_.CreateAssociation();
			ITopic t = topicMap_.CreateTopic();
			assertNull("Expected null type after creation.", assoc.Type);
			assoc.Type = (t);
			assertNotNull("Expected non-null type after set", assoc.Type);
			assertEquals("Unexpected typing topic!", t, assoc.Type);
		}

		[Test]
		public void TestMembers() {
			IAssociation assoc = topicMap_.CreateAssociation();
			assertEquals("Expected empty AssociationRole set after create.", 0,
			             assoc.AssociationRoles.Count);

			ITopic p1 = topicMap_.CreateTopic();
			ITopic p2 = topicMap_.CreateTopic();
			IAssociationRole m1 = assoc.CreateAssociationRole(p1, null);
			assertEquals("Unexpected AssociationRole count after first add", 1,
			             assoc.AssociationRoles.Count);
			assertTrue("Expected AssociationRole m1 to be found after first add",
			           assoc.AssociationRoles.Contains(m1));

			IAssociationRole m2 = assoc.CreateAssociationRole(p2, null);
			assertEquals("Unexpected AssociationRole count after second add", 2,
			             assoc.AssociationRoles.Count);
			assertTrue("Expected AssociationRole m2 to be found after second add",
			           assoc.AssociationRoles.Contains(m2));

			m2.Remove();
			assertEquals("Unexpected AssociationRole count after first remove", 1,
			             assoc.AssociationRoles.Count);
			assertTrue("Expected AssociationRole m1 to be found after first remove",
			           assoc.AssociationRoles.Contains(m1));

			m1.Remove();
			assertEquals("Unexpected AssociationRole count after second remove", 0,
			             assoc.AssociationRoles.Count);
		}


		protected override IScopedObject getScopedObject() {
			IAssociation a = topicMap_.CreateAssociation();
			return a;
		}
	}
}