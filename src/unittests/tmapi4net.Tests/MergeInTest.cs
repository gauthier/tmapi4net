namespace tmapi4net.Core.Tests {
	using NUnit.Framework;
	using tmapi4net.Core.Exceptions;

	[TestFixture]
	public class MergeInTest : TMAPITest {
		/** Tests if TopicName Merging is provided by this TMAPI Implementation
        by default */

		[Test]
		public void TestBaseNameDefault() {
			bool merge = topicMapSystem_.GetFeature("http://tmapi.org/features/merge/byTopicName");
			ITopic t1 = topicMap_.CreateTopic();
			ITopicName bn1 = t1.CreateTopicName("testbn", null);

			ITopicMap tm2 = createTopicMap("test2");
			ITopic t2 = tm2.CreateTopic();
			ITopicName bn2 = t2.CreateTopicName("testbn", null);
			ITopicName bn22 = t2.CreateTopicName("testbn2", null);

			topicMap_.MergeIn(tm2);

			if (merge) {
				// we merge by Topic name
				assertEquals("Topic with same TopicNames are not merged", 1, topicMap_.Topics.Count);
				assertEquals("TopicNames are not merged (added) or are not the union", 2, t1.TopicNames.Count);
			}
			else {
				assertEquals("Topics must not be merged if merge by name is off", 2,
				             topicMap_.Topics.Count);
			}
			int topicCounter = topicMap_.Topics.Count;
			ITopicMap tm3 = createTopicMap("test3");
			ITopic t3 = tm3.CreateTopic();
			ITopicName bn3 = t3.CreateTopicName("testbn500", null);
			topicMap_.MergeIn(tm3);
			assertEquals("Topic with different TopicNames are not inserted", ++topicCounter, topicMap_.Topics.Count);
		}

		/** Tests if TopicName Merging is provided by this TMAPI Implementation */

		[Test]
		public void TestBaseNameForce() {
			bool merge = true;

			// <create new Topic map with name merging set>
			ITopicMapSystemFactory factory = TopicMapSystemFactory.NewInstance();
			try {
				factory.SetFeature("http://tmapi.org/features/merge/byTopicName", true);
			}
			catch (FeatureNotSupportedException) {
				merge = false;
			}

			// bad hack to copy all System.Properties to factory
			/*Iterator itprops = System.getProperties().keySet().iterator();
        while(itprops.hasNext()){
            String key  =   (String)itprops.next();
            factory.setProperty(key,System.getProperty(key));
        }      */
			topicMapSystem_ = factory.NewTopicMapSystem();

			removeAllMaps();
			topicMap_ = createTopicMap(baseLocatorAddress_);
			baseLocator_ = topicMap_.CreateLocator(baseLocatorAddress_);
			// </create>

			ITopic t1 = topicMap_.CreateTopic();
			ITopicName bn1 = t1.CreateTopicName("testbn", null);

			ITopicMap tm2 = createTopicMap("test2");
			ITopic t2 = tm2.CreateTopic();
			ITopicName bn2 = t2.CreateTopicName("testbn", null);
			ITopicName bn22 = t2.CreateTopicName("testbn2", null);

			topicMap_.MergeIn(tm2);

			if (merge) {
				// we merge by Topic name
				assertEquals("Topic with same TopicNames are not merged", 1, topicMap_.Topics.Count);
				assertEquals("TopicNames are not merged (added) or are not the union", 2, t1.TopicNames.Count);
			}
			else {
				assertEquals("Topics must not be merged if merge by name is off", 2,
				             topicMap_.Topics.Count);
			}
			int ITopicCounter = topicMap_.Topics.Count;
			ITopicMap tm3 = createTopicMap("test3");
			ITopic t3 = tm3.CreateTopic();
			ITopicName bn3 = t3.CreateTopicName("testbn500", null);
			topicMap_.MergeIn(tm3);
			assertEquals("Topic with different TopicNames are not inserted", ++ITopicCounter, topicMap_.Topics.Count);
		}


		/** Test if matching is done when 2 Topics have the same SubjectIndicator */

		[Test]
		public void TestSubjectIndicator() {
			ITopic t1 = topicMap_.CreateTopic();
			t1.AddSubjectIdentifier(topicMap_.CreateLocator("sa"));

			ITopicMap tm = createTopicMap("test2");
			ITopic t2 = tm.CreateTopic();
			t2.AddSubjectIdentifier(tm.CreateLocator("sa4"));

			topicMap_.MergeIn(tm);

			assertEquals("Topics with different SubjectIdentifiers are merged", 2, topicMap_.Topics.Count);

			ITopicMap tm3 = createTopicMap("test3");
			ITopic t3 = tm3.CreateTopic();
			t3.AddSubjectIdentifier(tm3.CreateLocator("sa"));
			t3.AddSubjectIdentifier(tm3.CreateLocator("sa5"));
			topicMap_.MergeIn(tm3);
			assertEquals("Topics with same SubjectIdentifier are not merged", 2, topicMap_.Topics.Count);
			assertEquals("SubjectIdentifiers are not merged (added) or there is not the union of them", 2,
			             t1.SubjectIdentifiers.Count);
		}

		/** Test if matching is done when 2 Topics have the same SubjectLocator */

		[Test]
		public void TestSubjectLocator() {
			ITopic t1 = topicMap_.CreateTopic();
			t1.AddSubjectLocator(topicMap_.CreateLocator("sa"));

			ITopicMap tm = createTopicMap("test2");
			ITopic t2 = tm.CreateTopic();
			t2.AddSubjectLocator(tm.CreateLocator("sa4"));
			topicMap_.MergeIn(tm);

			assertEquals("Topics with different SubjectLocators are merged or not inserted", 2, topicMap_.Topics.Count);

			ITopicMap tm3 = createTopicMap("test3");
			ITopic t3 = tm3.CreateTopic();
			t3.AddSubjectLocator(tm3.CreateLocator("sa"));
			topicMap_.MergeIn(tm3);

			assertEquals("Topics with same SubjectLocator are not merged", 2, topicMap_.Topics.Count);
			assertEquals("Topics with same SubjectLocator are merged but now they have duplicated source locators", 1,
			             t1.SubjectLocators.Count);

			ITopic t4 = topicMap_.CreateTopic();
			t4.CreateTopicName("testbn", null);
			t4.MergeIn(t1);

			assertEquals("Topics has to be merged and subject Locator has to be added", 1, t4.SubjectLocators.Count);
		}

		/** Test if matching is done when 2 Topics have the same SourceLocators */

		[Test]
		public void TestSourceLocator() {
			ITopic t1 = topicMap_.CreateTopic();
			int t1SrcLocCount = t1.SourceLocators.Count;
			t1.AddSourceLocator(topicMap_.CreateLocator("sa"));

			ITopicMap tm = createTopicMap("test2");
			ITopic t2 = tm.CreateTopic();
			t2.AddSourceLocator(tm.CreateLocator("sa4"));

			topicMap_.MergeIn(tm);

			assertEquals("Topics with different SourceLocator are merged", 2, topicMap_.Topics.Count);

			ITopicMap tm3 = createTopicMap("test3");
			ITopic t3 = tm3.CreateTopic();
			int t3SrcLocCount = t3.SourceLocators.Count;
			t3.AddSourceLocator(tm3.CreateLocator("sa"));
			topicMap_.MergeIn(tm3);
			assertEquals("Topics with same SourceLocator are not merged", 2, topicMap_.Topics.Count);
			assertEquals("SourceLocator are not merged (added) or there is not the union of them",
			             t1SrcLocCount + t3SrcLocCount + 1, t1.SourceLocators.Count);
		}

		//an equal locator in the [subject identifiers] property of the one Topic
		//item and the [source locators] property of the other
		/** Tests if one SubjectIdentifier matches to one SourceLocator */

		[Test]
		public void TestSubjectIdentifierSourcelocators() {
			ITopic t1 = topicMap_.CreateTopic();
			int count = topicMap_.Topics.Count;
			t1.AddSourceLocator(topicMap_.CreateLocator("http://www.tmapi.org/mergeInTest#foo"));

			ITopicMap tm = createTopicMap("test4");
			ITopic t2 = tm.CreateTopic();
			ILocator loc = tm.CreateLocator("http://www.tmapi.org/subjectAdress");
			t2.AddSubjectLocator(loc);
			t2.AddSubjectIdentifier(tm.CreateLocator("http://www.tmapi.org/mergeInTest#foo"));

			topicMap_.MergeIn(tm);
			assertEquals("Topics with same SourceLocator<->SubjectIdentifier (subjectIndicator) are not merged 1", count,
			             topicMap_.Topics.Count);
			assertEquals("Topics with same SourceLocator<->SubjectIdentifier (subjectIndicator) are not merged 2", loc,
			             t1.SubjectLocators[0]);
		}

		/*
     *Tests if the associations are merged too
     */

		public void TestAssocRolesMerged() {
			ITopic t1 = topicMap_.CreateTopic();
			ITopic t2 = topicMap_.CreateTopic();
			ITopic t3 = topicMap_.CreateTopic();
			IAssociation a = topicMap_.CreateAssociation();
			a.CreateAssociationRole(t2, null);
			a.CreateAssociationRole(t3, null);

			t1.MergeIn(t2);
			assertEquals("ITopic must now play a role after merging", 1, t1.RolesPlayed.Count);
		}

		/*
     *Tests if the types are merged too
     */

		public void TestTypesMerged() {
			ITopic t1 = topicMap_.CreateTopic();
			ITopic t2 = topicMap_.CreateTopic();
			ITopic t3 = topicMap_.CreateTopic();
			t2.AddType(t3);

			t1.MergeIn(t2);
			assertTrue("ITopic must have a type now", t1.Types.Contains(t3));
		}

		/**
     * Test if a subject locator clash is recognized by the ITopicmap processor.
     *
     * @todo This test should be run with two configurations:
     *       One with XTM 1.1 enabled and one with XTM 1.0 enabled.
     *       Currently this test is using the default model of the TM processor.
     */

		[Test]
		public void TestSubjectLocatorClash() {
			bool xtm_1_1 = false;
			try {
				xtm_1_1 = topicMapSystem_.GetFeature("http://tmapi.org/features/model/xtm1.1");
				ITopic t1 = topicMap_.CreateTopic();
				ITopic t2 = topicMap_.CreateTopic();
				t1.AddSubjectLocator(topicMap_.CreateLocator("subjectA"));
				t2.AddSubjectLocator(topicMap_.CreateLocator("subjectB"));

				try {
					t1.MergeIn(t2);
					if (!xtm_1_1)
						fail("XTM 1.1 not supported, yet SubjectLocatorClashException is not thrown.");
				}
				catch (ModelConstraintException e) {
					if (xtm_1_1)
						throw e; // in this case the exception is wrong, so let it go
				}

				if (xtm_1_1)
					assertEquals("Unexpected subject locator size after merging (XTM 1.1 model)", 2, t1.SubjectLocators.Count);
			}
			catch (SubjectLocatorClashException ex) {
				if (!xtm_1_1) {
					assertEquals("Unexpected ITopic count in SubjectLocatorClashException.Topics", 2, ex.Topics.Count);
				}
				else {
					fail("A ITopic map processor that is using the XTM 1.1 model MUST never throw a SubjectLocatorClashException");
				}
			}
		}
	}
}