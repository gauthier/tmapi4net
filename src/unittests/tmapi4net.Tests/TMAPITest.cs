
namespace tmapi4net.Core.Tests {
	using System;
	using System.Collections.Generic;
	using NUnit.Framework;
	using tmapi4net.Core.Collections;
	using System.Reflection;

	public abstract class TMAPITest {
		protected static ITopicMapSystem topicMapSystem_;
		protected string baseLocatorAddress_ = "test";
		protected ILocator baseLocator_;
		protected ITopicMap topicMap_;
		protected IDictionary<string, ITopicMap> topicMaps_;



		static TMAPITest() {
/*
			try {
				IConfigurationSource config = System.Configuration.ConfigurationManager.GetSection("activerecord") as IConfigurationSource;
				ActiveRecordStarter.Initialize(config);
				ActiveRecordStarter.RegisterAssemblies(
					Assembly.Load("tmapi4net.Providers.ActiveRecord")
					);
			}
			catch(Exception e){
			
			Console.WriteLine(e);
			}
			
				ActiveRecordStarter.RegisterTypes(
				typeof (Model.Locator),
				typeof (Model.TopicMapObject),
				typeof (Model.TopicMap),
				typeof (Model.Topic),
				typeof (Model.TopicName),
				typeof (Model.Association),
				typeof (Model.AssociationRole),
				typeof(Model.Occurrence),
				typeof(Model.Variant), 
				typeof(Model.TopicTypesRelation)
				);*/


		}



		public TMAPITest() {
			topicMaps_ = new Dictionary<string, ITopicMap>();
		}


		static readonly log4net.ILog log = log4net.LogManager.GetLogger((typeof(TMAPITest)));
		[SetUp]
		public virtual void SetUp() {
			log4net.Config.XmlConfigurator.Configure();
			if (log.IsDebugEnabled)
				log.Debug("========tmapi4net.Tests SETUP=========");
			//Core.TopicMapSystemFactory.SetTopicMapSystemFactoryTypeName("tmapi4net.Providers.ActiveRecord.TopicMapSystemFactory,tmapi4net.Providers.ActiveRecord");
			Core.TopicMapSystemFactory.SetTopicMapSystemFactoryTypeName("tmapi4net.Providers.Memory.TopicMapSystemFactory,tmapi4net.Providers.Memory");
			ITopicMapSystemFactory factory = Core.TopicMapSystemFactory.NewInstance();

			topicMapSystem_ = factory.NewTopicMapSystem();
			removeAllMaps();
			topicMap_ = createTopicMap(baseLocatorAddress_);
			baseLocator_ = topicMap_.CreateLocator(baseLocatorAddress_);
			if (log.IsDebugEnabled)
				log.Debug("========tmapi4net.Tests /SETUP=========");

		}

		[TearDown]
		public virtual void TearDown() {
			if (log.IsDebugEnabled)
				log.Debug("========tmapi4net.Tests TEARDOWN=========");

			removeAllMaps();
			topicMapSystem_.Close();

			if (log.IsDebugEnabled)
				log.Debug("========tmapi4net.Tests /TEARDOWN=========");

		}


		protected void removeAllMaps() {
			try {
				IReadOnlySet<ILocator> locators = topicMapSystem_.BaseLocators;
				foreach (ILocator locator in locators) {
					removeTopicMap(topicMapSystem_.GetTopicMap(locator));
				}
				topicMaps_.Clear();
			}
			catch (Exception e) {
				log.Error("while removing topicmaps", e);
				throw;
			}
		}

		protected void removeTopicMap(ITopicMap topicMap) {
			topicMaps_.Remove(topicMap.BaseLocator.Reference);
			topicMap.Remove();
		}

		protected ITopicMap createTopicMap(string reference) {
			ITopicMap topicmap = topicMapSystem_.CreateTopicMap(reference);
			topicMaps_.Add(reference, topicmap);
			return (topicmap);
		}

		#region assert methods

		protected void assertEquals(string message, object o1, object o2) {
			Assert.AreEqual(o1, o2, message);
		}

		protected void assertEquals(object o1, object o2) {
			Assert.AreEqual(o1, o2);
		}

		protected void assertNull(string message, object o) {
			Assert.IsNull(o, message);
		}

		protected void assertTrue(string message, bool expression) {
			Assert.IsTrue(expression, message);
		}

		protected void assertTrue(bool expression) {
			Assert.IsTrue(expression);
		}

		protected void assertFalse(bool expression) {
			Assert.IsFalse(expression);
		}

		protected void assertFalse(string message, bool expression) {
			Assert.IsFalse(expression, message);
		}

		protected void assertNotNull(object notnull) {
			Assert.IsNotNull(notnull);
		}

		protected void assertNotNull(string message, object notnull) {
			Assert.IsNotNull(notnull, message);
		}

		protected void fail(string message) {
			Assert.Fail(message);
		}

		#endregion
	}
	/*
	[TestFixture]
	public class BruteForcePerfTest  :TMAPITest{
		[Test]
		public void TestAddTOpics() { 
			for(int i = 0; i < 10; ++i) {
				topicMap_.CreateTopic();
			}
		}


		[Test]
		public void TestAddTOpicsTransaction() {
			using (TransactionScope scope = new TransactionScope()) {
				for (int i = 0; i < 10; ++i) {
					topicMap_.CreateTopic();
				}
			}
		}
	}*/
}