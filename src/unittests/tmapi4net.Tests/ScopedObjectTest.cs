namespace tmapi4net.Core.Tests {
	using NUnit.Framework;

	public abstract class ScopedObjectTest : TMAPITest {
		[Test]
		public void TestThemes() {
			IScopedObject scopedobject = getScopedObject();
			int themeCount = scopedobject.Scope.Count;

			ITopic topic = topicMap_.CreateTopic();
			scopedobject.AddScopingTopic(topic);
			assertEquals("TopicMap m_topicMap is not the same than topicMap from scopedObject ", topicMap_, scopedobject.TopicMap);
			assertEquals("Unexpected scope size. ", themeCount + 1,
			             scopedobject.Scope.Count);
			assertTrue("Expected theme to be in object's scope.",
			           scopedobject.Scope.Contains(topic));
			scopedobject.RemoveScopingTopic(topic);
			assertEquals("Unexpected scope size after remove.", themeCount,
			             scopedobject.Scope.Count);
		}


		protected abstract IScopedObject getScopedObject();
	}
}