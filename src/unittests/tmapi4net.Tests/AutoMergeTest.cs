namespace tmapi4net.Core.Tests {
	using NUnit.Framework;
	using tmapi4net.Core.Exceptions;

	[TestFixture]
	public class AutoMergeTest : TMAPITest {
		private bool automerge;


		[SetUp]
		public override void SetUp() {
			base.SetUp();
			ITopicMapSystemFactory factory = TopicMapSystemFactory.NewInstance();
			try {
				factory.SetFeature("http://tmapi.org/features/automerge", true);
				automerge = true;
			}
			catch (FeatureNotSupportedException) {
				automerge = false;
			}
			topicMapSystem_ = factory.NewTopicMapSystem();
			removeAllMaps();
			topicMap_ = createTopicMap(baseLocatorAddress_);
			baseLocator_ = topicMap_.CreateLocator(baseLocatorAddress_);
		}

		/** Test if matching is done when 2 Topics have the same SubjectIndicator */

		[Test]
		public void TestSubjectIndicator() {
			ITopic t1 = topicMap_.CreateTopic();
			t1.AddSubjectIdentifier(topicMap_.CreateLocator("sa"));
			ITopic t2 = topicMap_.CreateTopic();
			try {
				t2.AddSubjectIdentifier(topicMap_.CreateLocator("sa"));
				assertEquals("Topics must be merged automatically cause they have the same subject identifier", 1,
				             topicMap_.Topics.Count);
				assertTrue("Auto-merge feature cannot be turned on, yet implementation auto-merges", automerge);
			}
			catch (MergeException) {
				assertFalse("Duplicate subject identifiers reported as error, yet automerge is off", automerge);
			}
		}


		/** Test if matching is done when 2 Topics have the same SubjectLocator */

		[Test]
		public void TestSubjectLocator() {
			ITopic t1 = topicMap_.CreateTopic();
			t1.AddSubjectLocator(topicMap_.CreateLocator("sa"));
			ITopic t2 = topicMap_.CreateTopic();
			try {
				t2.AddSubjectLocator(topicMap_.CreateLocator("sa"));
				assertEquals("Topics must be merged automatically cause they have the same subjectidentity", 1,
				             topicMap_.Topics.Count);
				assertTrue("Auto-merge feature cannot be turned on, yet implementation auto-merges", automerge);
			}
			catch (MergeException) {
				assertFalse("Duplicate subject locators reported as error, yet automerge is off", automerge);
			}
		}

		/** Test if matching is done when 2 Topics have the same SourceLocator */

		[Test]
		public void TestTopicsourceLocator() {
			ITopic t1 = topicMap_.CreateTopic();
			t1.AddSourceLocator(topicMap_.CreateLocator("sa"));
			ITopic t2 = topicMap_.CreateTopic();
			try {
				t2.AddSourceLocator(topicMap_.CreateLocator("sa"));
				assertEquals("Topics must be merged automatically cause they have the same source locator", 1,
				             topicMap_.Topics.Count);
				assertTrue("Auto-merge feature cannot be turned on, yet implementation auto-merges", automerge);
			}
			catch (MergeException) {
				assertFalse("Duplicate source locators reported as error, yet automerge is off", automerge);
			}
		}
	}
}