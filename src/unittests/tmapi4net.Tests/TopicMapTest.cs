namespace tmapi4net.Core.Tests {
	using NUnit.Framework;
	using tmapi4net.Core.Exceptions;

	[TestFixture]
	public class TopicMapTest : TMAPITest {
		[Test]
		public void TestBaseLocator() {
			ILocator l = topicMap_.BaseLocator;
			assertNotNull("Execpted notation string for base address",
			              l.Notation);
			assertTrue("Unexpected base Locator Notation.",
			           l.Notation.Equals("URI"));
			assertTrue("Unexpected base Locator Reference.",
			           l.Reference.Equals(baseLocator_.Reference));
		}

		[Test]
		public void TestObjectByIDTopic() {
			ITopic t1 = topicMap_.CreateTopic();
			string t1id = t1.ObjectId;
			ITopic t2 = (ITopic) topicMap_.GetObjectById(t1id);
			assertEquals("Topics are not the same after getObjectById ", t1, t2);
			t2.Remove();
			assertNull("Topic is still in ObjectID list and can be retrieved over getObjectById()", topicMap_.GetObjectById(t1id));
		}

		[Test]
		public void TestObjectByIDAssociation() {
			IAssociation a1 = topicMap_.CreateAssociation();
			string a1id = a1.ObjectId;
			IAssociation a2 = (IAssociation) topicMap_.GetObjectById(a1id);
			assertEquals("Associations are not the same after getObjectById ", a1, a2);
			a2.Remove();
			assertNull("Association is still in ObjectID list and can be retrieved over getObjectById()",
			           topicMap_.GetObjectById(a1id));
		}

		[Test]
		public void TestObjectByIDBaseName() {
			ITopic t1 = topicMap_.CreateTopic();
			ITopic t2 = (ITopic) topicMap_.GetObjectById(t1.ObjectId);
			ITopicName bn1 = t2.CreateTopicName("hallo", null);
			string bn1id = bn1.ObjectId;
			ITopicName bn2 = (ITopicName) topicMap_.GetObjectById(bn1id);
			assertEquals("TopicNames are not the same after getObjectById ", bn1, bn2);
			bn2.Remove();
			assertNull("TopicName is still in ObjectID list and can be retrieved over getObjectById()",
			           topicMap_.GetObjectById(bn1id));
		}

		[Test]
		public void TestObjectByIDOccurrence() {
			ITopic t1 = topicMap_.CreateTopic();
			ITopic t2 = (ITopic) topicMap_.GetObjectById(t1.ObjectId);
			IOccurrence oc1 = t2.CreateOccurrence("resTest", t1, null);
			string oc1id = oc1.ObjectId;
			IOccurrence oc2 = (IOccurrence) topicMap_.GetObjectById(oc1id);
			assertEquals("Occurrences are not the same after getObjectById ", oc1, oc2);
			oc2.Remove();
			assertNull("Occurrence is still in ObjectID list and can be retrieved over getObjectById()",
			           topicMap_.GetObjectById(oc1id));
		}

		[Test]
		public void TestTopics() {
			try {
				assertEquals("Unexpected topic count before add", 0,
				             topicMap_.Topics.Count);

				ITopic t1 = topicMap_.CreateTopic();
				assertEquals("Unexpected topic count after add", 1,
				             topicMap_.Topics.Count);

				ITopic t2 = (ITopic) topicMap_.Topics[0];
				assertEquals("Expected to find only a topic with t1's handle in topic map",
				             t1.ObjectId, t2.ObjectId);
				t2.Remove();
				assertEquals("Unexpected topic count after remove", 0,
				             topicMap_.Topics.Count);
			}
			catch (TopicInUseException ex) {
				fail("Unexpected exception: " + ex.ToString());
			}
		}

		[Test]
		public void TestAssociations() {
			try {
				assertEquals("Unexpected association count before add", 0,
				             topicMap_.Associations.Count);

				IAssociation a1 = topicMap_.CreateAssociation();
				assertEquals("Unexpected association count after add", 1,
				             topicMap_.Associations.Count);

				IAssociation a2 = (IAssociation) topicMap_.Associations[0];
				assertEquals("Expected to find only an association with a1's handle in topic map",
				             a1.ObjectId, a2.ObjectId);
				a2.Remove();
				assertEquals("Unexpected association count after remove", 0,
				             topicMap_.Associations.Count);
			}
			catch (TMAPIException ex) {
				fail("Unexpected exception: " + ex.ToString());
			}
		}


		[Test]
		public void TestTopicMapSystem() {
			assertNotNull("Unexpected getTopicMapSystem() returned null", topicMap_.TopicMapSystem);
			assertEquals("Unexpected TopicMapSystem, has to be the same then the one this topic map was created with",
			             topicMapSystem_, topicMap_.TopicMapSystem);
		}

		[Test]
		public void TestHelperObject() {
			try {
				topicMap_.GetHelperObject<ITopic>();
				fail("UnsupportedHelperException not thrown");
			}
			catch (TMAPIException) {}
		}
	}
}