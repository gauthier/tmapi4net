namespace tmapi4net.Core.Tests {
	using tmapi4net.Core.Exceptions;
	using Test = NUnit.Framework.TestAttribute;
	using TestFixture = NUnit.Framework.TestFixtureAttribute;
	using SetUp = NUnit.Framework.SetUpAttribute;
	using TearDown = NUnit.Framework.TearDownAttribute;

	[TestFixture]
	public class TopicTest : TMAPITest {

		#region testcases

		[Test]
		public void TestBaseNames() {
			ITopic t = topicMap_.CreateTopic();
			assertEquals("Unexpected topicname count before add", 0,
			             t.TopicNames.Count);

			ITopicName basename = t.CreateTopicName("foo", null);
			assertEquals("Unexpected topicname count after add", 1,
			             t.TopicNames.Count);

			// Test setting of name string
			string namestring = t.TopicNames[0].Value;
			assertNotNull("Expected content for topic name", namestring);
			assertEquals("Unexpected topicname content.", namestring, "foo");

			// Test removal
			basename.Remove();
			assertEquals("Unexpected topicname count after remove", 0,
			             t.TopicNames.Count);
		}

		[Test]
		public void TestOccurrences() {
			ITopic t = topicMap_.CreateTopic();
			assertEquals("Unexpected occurrence count before add", 0,
			             t.Occurrences.Count);

			// Test creation
			IOccurrence occ = t.CreateOccurrence("foo", null, null);
			assertEquals("Unexpected occurrence count after add", 1,
			             t.Occurrences.Count);

			// Test setting of resource data
			string resString = t.Occurrences[0].Value;
			assertNotNull("Expected content for occurrence", resString);
			assertEquals("Unexpected occurrence content.", resString, "foo");

			// Test removal
			try {
				occ.Remove();
				assertEquals("Unexpected occurrence count after remove", 0,
				             t.Occurrences.Count);
			}
			catch (TMAPIException ex) {
				fail("Unexpected exception while removing occurrence. " +
				     ex.ToString());
			}
		}

		[Test]
		public void TestSourceLocators() {
			ITopic t = topicMap_.CreateTopic();
			int baseLocCount = t.SourceLocators.Count;

			ILocator locator = topicMap_.CreateLocator("http://tmapi.org/test");
			t.AddSourceLocator(locator);
			assertEquals("Unexpected source Locator count after add", baseLocCount + 1,
			             t.SourceLocators.Count);

			t.RemoveSourceLocator(locator);
			assertEquals("Unexpected sourcelocator count after remove", baseLocCount,
			             t.SourceLocators.Count);
		}

		[Test]
		public void TestSubjectAddress() {
			ITopic t = topicMap_.CreateTopic();
			assertEquals("Unexpected value for subject locator before set",
			             t.SubjectLocators.Count, 0);

			ILocator loc = topicMap_.CreateLocator("http://www.tm4j.org/test/subject", "URI");
			t.AddSubjectLocator(loc);
			assertEquals("Unexpected size of subjectLocators set after setSubjectLocator", 1, t.SubjectLocators.Count);
			assertTrue("Unexpected value for subject Locator after set",
			           t.SubjectLocators[0].Reference.Equals("http://www.tm4j.org/test/subject"));
		}

		[Test]
		public void TestTypes() {
			ITopic t = topicMap_.CreateTopic();
			ITopic type = topicMap_.CreateTopic();
			assertEquals("Unexpected type count before add", 0, t.Types.Count);
			t.AddType(type);
			assertEquals("Unexpected type count after add", 1, t.Types.Count);

			ITopic retType = t.Types[0];
			assertEquals("Unexpected returned type.", type, retType);
			t.RemoveType(type);
			assertEquals("Unexpected type count after remove", 0,
			             t.Types.Count);
		}

		[Test]
		public void TestRolesPlayed() {
			ITopic t1 = topicMap_.CreateTopic();
			ITopic t2 = topicMap_.CreateTopic();

			IAssociation a = topicMap_.CreateAssociation();
			IAssociationRole m1 = a.CreateAssociationRole(t1, null);
			IAssociationRole m2 = a.CreateAssociationRole(t2, null);

			assertEquals("Expected AssociationRole was not found in topic.RolesPlayed ", 1, t1.RolesPlayed.Count);
			assertEquals("Expected AssociationRole was not found in topic.RolesPlayed ", 1, t2.RolesPlayed.Count);
			m1.Remove();
			assertEquals("AssociationRole was removed, but is still in RolesPlayed ", 0, t1.RolesPlayed.Count);
			assertEquals("This AssociationRole was not also removed, but should not be ", 1, t2.RolesPlayed.Count);
			a.Remove();
			assertEquals("Association was removed, but there is still roleplayed ", 0, t2.RolesPlayed.Count);
		}

		#endregion
	}
}