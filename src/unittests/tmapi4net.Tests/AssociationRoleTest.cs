namespace tmapi4net.Core.Tests {
	using NUnit.Framework;

	[TestFixture]
	public class AssociationRoleTest : TMAPITest {
	
		[Test]
		public void TestType() {

			IAssociation assoc = topicMap_.CreateAssociation();

			IAssociationRole m1 = assoc.CreateAssociationRole(null, null);

			Assert.IsNull(m1.Type, "Expected null type after creation");

			ITopic tt = topicMap_.CreateTopic();

			m1.Type = tt;

			Assert.IsNotNull(m1.Type, "Expected non-null type after set");

			Assert.AreEqual(tt, m1.Type, "Unexpected typing topic");

			m1.Type = (null);

			Assert.IsNull(m1.Type, "Expected null type after remove");
		}
	}
}