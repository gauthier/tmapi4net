namespace tmapi4net.Core.Tests {
	using NUnit.Framework;

	[TestFixture]
	public class OccurrenceTest : ScopedObjectTest {
		[Test]
		public void TestTopic() {
			ITopic t = topicMap_.CreateTopic();
			assertTrue("Expected new topics to be created with no occurrences", t.Occurrences.Count == 0);
			IOccurrence occ = t.CreateOccurrence("foo", null, null);
			assertEquals("Unexpected occurrence parent after creation", t,
			             occ.Topic);
			assertEquals("Expected occurrence set size to increment for topic", 1, t.Occurrences.Count);
			occ.Remove();
			assertNull("Unexpected topicname parent after remove", occ.Topic);
			assertTrue("Expected occurrence set size to decrement for topic.", t.Occurrences.Count == 0);
		}

		[Test]
		public void TestData() {
			ITopic t = topicMap_.CreateTopic();
			IOccurrence occ = t.CreateOccurrence("foo", null, null);
			assertNotNull("Expected Occurrence Value string to be set", occ.Value);
			assertTrue("Unexpected Occurrence Value", occ.Value == ("foo"));
			occ.Value = null;
			assertNull("Unexpected Occurrence Value after set", occ.Value);
		}

		[Test]
		public void TestResource() {
			ITopic t = topicMap_.CreateTopic();
			ILocator l = topicMap_.CreateLocator("http://www.tmapi.org/test");
			IOccurrence occ = t.CreateOccurrence(l, null, null);
			assertNotNull("Expected Occurrence Resource to be set", occ.Resource);
			assertEquals("Unexpected Occurrence Resource", occ.Resource, l );
			occ.Resource = null;
			assertNull("Unexpected Occurrence Resource after set", occ.Resource);
		}

		[Test]
		public void TestChangefromValuetoResource() {
			ITopic t = topicMap_.CreateTopic();
			ILocator l = topicMap_.CreateLocator("http://www.tmapi.org/test");
			IOccurrence occ = t.CreateOccurrence(l, null, null);
			assertNull("Expected Occurrence Value to be null", occ.Value);
			assertNotNull("Expected Occurrence Resource to be set", occ.Resource);

			occ.Value = ("foo");
			assertNotNull("Expected Occurrence Value string to be set", occ.Value);
			assertNull("Expected Occurrence Resource to be null", occ.Resource);

			occ.Resource = l;
			assertNull("Expected Occurrence Value to be null", occ.Value);
			assertNotNull("Expected Occurrence Resource to be set", occ.Resource);
		}

		[Test]
		public void TestType() {
			ITopic t = topicMap_.CreateTopic();
			IOccurrence occ = t.CreateOccurrence("foo", null, null);
			ITopic tt = topicMap_.CreateTopic();
			assertNull("Expected null type after creation.", occ.Type);
			occ.Type = (tt);
			assertNotNull("Expected non-null type after set", occ.Type);
			assertEquals("Unexpected typing topic!", tt, occ.Type);
			occ.Type = (null);
			assertNull("Expected null type after remove", occ.Type);
		}


		protected override IScopedObject getScopedObject() {
			ITopic topic = topicMap_.CreateTopic();
			IOccurrence occurrence = topic.CreateOccurrence("test", null, null);
			return occurrence;
		}
	}
}