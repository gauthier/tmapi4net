namespace tmapi4net.Core.Tests {
	using NUnit.Framework;
	using tmapi4net.Core.Exceptions;

	[TestFixture]
	public class MergeDetectionTest : TMAPITest {
		[SetUp]
		public override void SetUp() {
			base.SetUp();

			ITopicMapSystemFactory factory = TopicMapSystemFactory.NewInstance();

			factory.SetFeature("http://tmapi.org/features/automerge", false);

			topicMapSystem_ = factory.NewTopicMapSystem();
			topicMaps_.Clear();
			removeAllMaps();

			topicMap_ = createTopicMap(baseLocatorAddress_);
			baseLocator_ = topicMap_.CreateLocator(baseLocatorAddress_);
		}


		/** Test if matching is done when 2 Topics have the same SubjectIndicator */

		[Test]
		public void TestSubjectIndicator() {
			ITopic t1 = topicMap_.CreateTopic();
			t1.AddSubjectIdentifier(topicMap_.CreateLocator("sa"));
			ITopic t2 = topicMap_.CreateTopic();
			try {
				t2.AddSubjectIdentifier(topicMap_.CreateLocator("sa"));
				fail("TopicsMustMergeException must be thrown cause ITopic with existing subject Identifier is created");
			}
			catch (TopicsMustMergeException ex) {
				assertEquals("TopicsMustMergeException.UnmodifiedTopic must return t1", t1, ex.UnmodifiedTopic);
				assertEquals("TopicsMustMergeException.ModifiedTopic must return t2", t2, ex.ModifiedTopic);
			}
		}

		/** Test if matching is done when 2 Topics have the same SubjectLocator */

		[Test]
		public void TestSubjectLocator() {
			ITopic t1 = topicMap_.CreateTopic();
			t1.AddSubjectLocator(topicMap_.CreateLocator("sa"));
			ITopic t2 = topicMap_.CreateTopic();
			try {
				t2.AddSubjectLocator(topicMap_.CreateLocator("sa"));
				fail("TopicsMustMergeException must be thrown cause ITopic with existing subject locator is created");
			}
			catch (TopicsMustMergeException ex) {
				assertEquals("TopicsMustMergeException.UnmodifiedTopic must return t1", t1, ex.UnmodifiedTopic);
				assertEquals("TopicsMustMergeException.ModifiedTopic must return t2", t2, ex.ModifiedTopic);
			}
		}

		/** Test if matching is done when 2 Topics have the same SourceLocator */

		[Test]
		public void TestTopicsourceLocator() {
			ITopic t1 = topicMap_.CreateTopic();
			t1.AddSourceLocator(topicMap_.CreateLocator("sa"));
			ITopic t2 = topicMap_.CreateTopic();
			try {
				t2.AddSourceLocator(topicMap_.CreateLocator("sa"));
				fail("TopicsMustMergeException must be thrown cause ITopic with existing source locator is created");
			}
			catch (TopicsMustMergeException ex) {
				assertEquals("TopicsMustMergeException.UnmodifiedTopic must return first Topic", t1, ex.UnmodifiedTopic);
				assertEquals("TopicsMustMergeException.ModifiedTopic must return second Topic", t2, ex.ModifiedTopic);
			}
		}


		/** Test if matching is done when 2 Topics have the same SourceLocator */

		[Test]
		public void TestDuplicateSourceLocator() {
			ITopic t1 = topicMap_.CreateTopic();
			t1.AddSourceLocator(topicMap_.CreateLocator("sa"));
			ITopicName tn = t1.CreateTopicName("test", null);
			try {
				tn.AddSourceLocator(topicMap_.CreateLocator("sa"));
				fail("DuplicateSourceLocatorException must be thrown cause TopicMapObject with existing source locator is created");
			}
			catch (DuplicateSourceLocatorException ex) {
				assertEquals("DuplicateSourceLocatorException.getUnmodifiedTopicMapObject() must return t1", t1,
				             ex.UnmodifiedTopicMapObject);
				assertEquals("DuplicateSourceLocatorException.getModifiedTopicMapObject() must return tn", tn,
				             ex.ModifiedTopicMapObject);
			}
		}
	}
}