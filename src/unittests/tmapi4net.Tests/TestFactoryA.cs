namespace tmapi4net.Core.Tests {
	using System;

	internal class TestFactoryA : ITopicMapSystemFactory {
		#region ITopicMapSystemFactory Members

		ITopicMapSystem ITopicMapSystemFactory.NewTopicMapSystem() {
			throw new Exception("The method or operation is not implemented.");
		}

		bool ITopicMapSystemFactory.IsFeatureSupported(string featureName) {
			throw new Exception("The method or operation is not implemented.");
		}

		bool ITopicMapSystemFactory.GetFeature(string featureName) {
			throw new Exception("The method or operation is not implemented.");
		}

		void ITopicMapSystemFactory.SetFeature(string featureName, bool value) {
			throw new Exception("The method or operation is not implemented.");
		}

		string ITopicMapSystemFactory.GetProperty(string propertyName) {
			throw new Exception("The method or operation is not implemented.");
		}

		void ITopicMapSystemFactory.SetProperty(string propertyName, string value) {
			throw new Exception("The method or operation is not implemented.");
		}

		#endregion
	}
}