namespace tmapi4net.Core.Tests {
	public class TestFactory : ITopicMapSystemFactory {
		#region ITopicMapSystemFactory Members

		ITopicMapSystem ITopicMapSystemFactory.NewTopicMapSystem() {
			return null;
		}

		bool ITopicMapSystemFactory.IsFeatureSupported(string featureName) {
			return false;
		}

		bool ITopicMapSystemFactory.GetFeature(string featureName) {
			return false;
		}

		void ITopicMapSystemFactory.SetFeature(string featureName, bool value) {}

		string ITopicMapSystemFactory.GetProperty(string propertyName) {
			return null;
		}

		void ITopicMapSystemFactory.SetProperty(string propertyName, string value) {}

		#endregion
	}


}