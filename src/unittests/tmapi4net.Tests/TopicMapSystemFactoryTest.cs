namespace tmapi4net.Core.Tests {
	using NUnit.Framework;

	[TestFixture]
	public class TopicMapSystemFactoryTest : TMAPITest {
		[Test]
		public void TestReadFromSystemProperty() {
			TopicMapSystemFactory.SetTopicMapSystemFactoryTypeName(
				typeof (TestFactoryA).AssemblyQualifiedName
				);
			ITopicMapSystemFactory factory = TopicMapSystemFactory.NewInstance();
			assertNotNull(factory);
			assertTrue(factory is TestFactoryA);
		}
	}
}