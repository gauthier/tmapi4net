namespace tmapi4net.Core.Tests {
	using System;
	using NUnit.Framework;
	using tmapi4net.Core.Exceptions;

	[TestFixture]
	public class TopicNameTest : ScopedObjectTest {
		[Test]
		public void TestType() {
			bool xtm_1_1 = topicMapSystem_.GetFeature("http://tmapi.org/features/model/xtm1.1");

			ITopic t = topicMap_.CreateTopic();
			ITopic type = topicMap_.CreateTopic();
			ITopicName tn = t.CreateTopicName("foo", null);
			assertNull("Expected null type after creation.", tn.Type);
			if (!xtm_1_1) {
				try {
					tn.Type = type;
					fail("TM processor MUST throw an UnsupportedOperationException while trying to set topicname type (XTM 1.0)");
				}
				catch (UnsupportedOperationException) {
					// noop.
					// Everything is okay, because the TM proc. has thrown the UnsupportedOperationException
				}
				return;
			}

			// The following tests are for model/xtm1.1 only
			tn.Type = type;
			assertNotNull("Expected non-null type after set", tn.Type);
			assertEquals("Unexpected typing topic!", type, tn.Type);
			ITopicName tn2 = t.CreateTopicName("bar", null, null);
			assertNull("Expected null type after creation of topicname with type == null.", tn2.Type);
			tn2.Type = type;
			assertNotNull("Expected non-null type after set", tn2.Type);
			assertEquals("Unexpected typing topic!", type, tn2.Type);
			ITopicName tn3 = t.CreateTopicName("foobar", type, null);
			assertNotNull("Expected non-null type after creation of topicname with type.", tn3.Type);
			assertEquals("Unexpected typing topic after creation of topicname with type!", type, tn3.Type);
			tn3.Type = null;
			assertNull("Expected null type after set type to null", tn3.Type);
		}


		[Test]
		public void TestTopic() {
			ITopic topic = topicMap_.CreateTopic();
			int numnames = topic.TopicNames.Count;
			ITopicName topicname = topic.CreateTopicName("foo", null);

			assertEquals("Unexpected topicname parent after creation", topic,
			             topicname.Topic);
			assertEquals("Expected topic name count to increment by one after name creation",
			             numnames + 1, topic.TopicNames.Count);
			topicname.Remove();
			if (!topicMapSystem_.GetFeature("http://tmapi.org/features/readOnly"))
				/* The following cannot be guaranteed to work for persistent stores*/
				assertNull("Unexpected topicname parent after remove", topicname.Topic);

			assertEquals("Expected topic name count to decrement by one after name removed.",
			             numnames, topic.TopicNames.Count);
		}

		[Test]
		public void TestData() {
			ITopic t = topicMap_.CreateTopic();
			ITopicName tn = t.CreateTopicName(null, null);
			assertNull("Unexpected topicname value before set", tn.Value);
			tn.Value = "foo";
			assertNotNull("Expected TopicName name string to be set", tn.Value);
			assertTrue("Unexpected TopicName value", tn.Value == "foo");
			tn.Value = null;
			assertNull("Unexpected TopicName value after set", tn.Value);
		}

		[Test]
		public void TestVariants() {
			ITopic t = topicMap_.CreateTopic();
			ITopicName tn = t.CreateTopicName(null, null);
			assertEquals("Unexpected variant count before creating variant", 0,
			             tn.Variants.Count);

			IVariant var = tn.CreateVariant("foo", null);
			assertEquals("Unexpected variant count afer created new variant",
			             1, tn.Variants.Count);

			String varString = tn.Variants[0].Value;
			assertNotNull("Expected variant value after add", varString);
			assertTrue("Unexpected variant value after add",
			           varString == ("foo"));
			var.Remove();
			assertEquals("Unexpected variant count after remove", 0,
			             tn.Variants.Count);
		}


		protected override IScopedObject getScopedObject() {
			ITopic t = topicMap_.CreateTopic();
			ITopicName topicname = t.CreateTopicName("test", null);
			return topicname;
		}
	}
}